import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
import { User } from 'src/app/user.model';
import { map } from 'rxjs/operators';

const apiURL = environment.apiUrlWindServices;

@Injectable({
  providedIn: 'root'
})
export class GestioneScorteService {

   // url per servizio utility
   private UrlConfig = apiURL;

   constructor(
     private http: HttpClient
   ) {

   }

   // Servizi esposti
  getWarehouses( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/warehouses';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

  getCatalog( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/catalog';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

  getOpenRequests( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/open_requests';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getGridColumns( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/grid_columns';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

  
  
  getStock( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/stock';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

  getHeadquartersGls( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/headquartersGls';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getValidateOTR( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/validateOTR';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  
  getRichiesteJwt( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/getRichiesteJwt';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getCheckRipin( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/CheckRipin';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getPresidioMagWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/presidio_mag_wind';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getNumRichAttiveOperatore( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/num_rich_attive';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getGuastoGestRich( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/desc_guasto_gest_rich';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getRifiutoGestRich( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/desc_rifiuto_gest_rich';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getCausaliSospReso( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/causali_sosp_reso';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

  createRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  approvatoRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/APPROVATO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  cambioPrioritaRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/CAMBIO_PRIORITA';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  rifiutatoRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/RIFIUTATO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  matSuMobileRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/MATERIALE_SU_MOBILE';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  annullaAttMatRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/ANNULLAMENTO_ATTESA_MATERIALE';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  annullaPrenotaRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/ANNULLAMENTO_PRENOTAZIONE';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  consAccettataRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/CONSEGNA_ACCETTATA';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  materialeUtilizzatoRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/MATERIALE_UTILIZZATO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  rientroAMagazzinoRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/RIENTRO_A_MAGAZZINO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  rifiutoMaterialeAccettatoRmaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/rmas/actions/RIFIUTO_MATERIALE_ACCETTATO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  createResoRipinWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/resos';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  confermaSospResoWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/resos/actions/CONFERMA_SOSPENSIONE';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  annullaSospResoWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/resos/actions/ANNULLAMENTO_SOSPENSIONE';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  sospesoResoWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/resos/actions/SOSPESO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  getPresidioMagazzini( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/presidio_magazzini';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  aggiornaPresidiMagazzini( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/actions/AGGIORNA_PRESIDIO_MAGAZZINI';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  fornituraAnnullataLogisticaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/logisticas/actions/FORNITURA_ANNULLATA';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  fornituraApprovataLogisticaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/logisticas/actions/FORNITURA_APPROVATA';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  fornituraConsegnaAccettataLogisticaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/logisticas/actions/FORNITURA_CONSEGNA_ACCETTATA';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  fornituraMaterialeRifiutatoLogisticaWind( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/logisticas/actions/FORNITURA_MATERIALE_RIFIUTATO';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  registraJwtGeocall( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/geocall/registraJwt';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  terminaValiditaJwtGeocall( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/geocall/terminaValiditaJwt';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  checkValiditaJwtGeocall( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/geocall/checkValiditaJwt';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  getMinSessGeocall( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/geocall/getMinSessGeocall';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  getRmaResi( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/get_rma_resi';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getRmaAppHQ( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/get_rma_approvazione_hq';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getAzioni( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/get_azioni';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getMatCompatibili( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/get_mat_compatibili';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getStatiGestRich( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/get_stati_gest_richieste';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  } 
  
  
  getAnagCompatibili( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/compatibili_equivalenti/anagrafica_compatibili';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getCompatibiliEquivalenti( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/compatibili_equivalenti/get_compatibili_equivalenti';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  creaCompatibileEquivalente( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/processes/compatibili_equivalenti/actions/GESTIONE_COMPATIBILI';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  


}
