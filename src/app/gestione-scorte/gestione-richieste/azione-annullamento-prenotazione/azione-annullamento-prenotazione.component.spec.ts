import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneAnnullamentoPrenotazioneComponent } from './azione-annullamento-prenotazione.component';

describe('AzioneAnnullamentoPrenotazioneComponent', () => {
  let component: AzioneAnnullamentoPrenotazioneComponent;
  let fixture: ComponentFixture<AzioneAnnullamentoPrenotazioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneAnnullamentoPrenotazioneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneAnnullamentoPrenotazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
