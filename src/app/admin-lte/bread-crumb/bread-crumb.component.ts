import { Component, OnInit,Input } from '@angular/core';
import { IBreadcrumb } from './i-breadcrumb.interface'; /* interfaccia alla base del Breadcrumb*/
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-bread-crumb',
  templateUrl: './bread-crumb.component.html',
  styleUrls: ['./bread-crumb.component.css']
})
export class BreadCrumbComponent implements OnInit {

  @Input() breadCrumbCfg: any;
  public breadcrumbs: Array<IBreadcrumb>;
  esterno         : string;
  
  constructor(private router: Router,
    private activatedRoute: ActivatedRoute
    ) {
    
     this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);

    this.esterno            = '';
    this.activatedRoute.queryParams.subscribe(params => {
      this.esterno = params['esterno'] ? params['esterno'].toUpperCase() : null;       
 
     });
    
    }

  ngOnInit(): void {
    
    console.debug('sono BreadCrumbComponent: ',this.breadCrumbCfg);
    
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.breadcrumbs = this.buildBreadCrumb(this.activatedRoute.root);
    })
    
    this.activatedRoute.queryParams.subscribe(params => {
      this.esterno = params['esterno'] ? params['esterno'].toUpperCase() : null;       
 
     });
    
    
  }
  
  
  
  /**
   * Recursively build breadcrumb according to activated route.
   * @param route
   * @param url
   * @param breadcrumbs
   */
  buildBreadCrumb(route: ActivatedRoute,url: string = '',breadcrumbs: Array<IBreadcrumb> = []): Array<IBreadcrumb> {
      console.debug('buildBreadCrumb: ',route);
      //If no routeConfig is avalailable we are on the root path
      let label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      let path = route.routeConfig && route.routeConfig.data ? (route.routeConfig.data.livello != 'principale' ? route.routeConfig.data.livello : '') : '';
  ​    let icon = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.icon : '';
      let titolo = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.titolo : '';
  
      console.debug('01 - :',label,path);
      // If the route is dynamic route such as ':id', remove it
      const lastRoutePart = path.split('/').pop();
      console.debug('lastRoutePart:',lastRoutePart);
      const isDynamicRoute = lastRoutePart.startsWith(':');
      if(isDynamicRoute && !!route.snapshot) {
        const paramName = lastRoutePart.split(':')[1];
        path  = path.replace(lastRoutePart, route.snapshot.params[paramName]);
        label = route.snapshot.params[paramName];
        //icon  = '';
      }
  ​
      //In the routeConfig the complete path is not available,
      //so we rebuild it each time
      const nextUrl = path ? `${url}/${path}` : url;
  ​
      const breadcrumb: IBreadcrumb = {
          label: titolo ? titolo : label,
          url: nextUrl,
          icon: icon,
          titolo : titolo
      };
      // Only adding route with non-empty label
      const newBreadcrumbs = breadcrumb.label ? [ ...breadcrumbs, breadcrumb ] : [ ...breadcrumbs];
      console.debug('firstChild: ',route.firstChild);
      if (route.firstChild) {
          //If we are not on our current path yet,
          //there will be more children to look after, to build our breadcumb
          return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
      }
      return newBreadcrumbs;
  }  

}
