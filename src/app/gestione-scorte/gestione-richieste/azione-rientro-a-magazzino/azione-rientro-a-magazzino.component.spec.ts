import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneRientroAMagazzinoComponent } from './azione-rientro-a-magazzino.component';

describe('AzioneRientroAMagazzinoComponent', () => {
  let component: AzioneRientroAMagazzinoComponent;
  let fixture: ComponentFixture<AzioneRientroAMagazzinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneRientroAMagazzinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneRientroAMagazzinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
