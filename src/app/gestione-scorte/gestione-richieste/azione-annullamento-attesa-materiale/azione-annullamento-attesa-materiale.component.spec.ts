import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneAnnullamentoAttesaMaterialeComponent } from './azione-annullamento-attesa-materiale.component';

describe('AzioneAnnullamentoAttesaMaterialeComponent', () => {
  let component: AzioneAnnullamentoAttesaMaterialeComponent;
  let fixture: ComponentFixture<AzioneAnnullamentoAttesaMaterialeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneAnnullamentoAttesaMaterialeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneAnnullamentoAttesaMaterialeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
