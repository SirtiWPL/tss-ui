import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404GestioneScorteComponent } from './page404-gestione-scorte.component';

describe('Page404GestioneScorteComponent', () => {
  let component: Page404GestioneScorteComponent;
  let fixture: ComponentFixture<Page404GestioneScorteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Page404GestioneScorteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404GestioneScorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
