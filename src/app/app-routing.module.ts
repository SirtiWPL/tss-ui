import { AuthGuard } from './auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Page404Component } from './page404/page404.component';
import { LoginComponent } from './login/login.component';


/* cfg delle rotte nel padre */
const appRoutes: Routes = [
    { path: '', component: Page404Component},
    { path: 'login', component: LoginComponent},
    {
      path: 'home',
      canActivate: [AuthGuard],
      //canLoad: [CanLoadAuthGuard],
      loadChildren: () => import(`./admin-lte/admin-lte.module`).then(m => m.AdminLteModule),
    },
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes,{ enableTracing: false,useHash: true}),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
