import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { MenuCfgModule } from '../../config/menu-cfg.module' /* cfg del menu  */
import { UiService } from '../../ui-service.service';

import { iSidebarDataModule } from './sidebar-data.interface';

@Component({
  selector: 'app-sidebar-page',
  templateUrl: './sidebar-page.component.html',
  styleUrls: ['./sidebar-page.component.css']
})
export class SidebarPageComponent implements OnInit,OnChanges {
  
  /* istanzio l'oggetto HeaderDataModule e lo rendo visibile */
  
  @Input() sidebarDataObj: iSidebarDataModule;
  @Input() sideBarMenuCfg; /* definisco la cfg del menu della sidebar */
  
  constructor(private uiService: UiService) {
    
    const mainMenuCfgObj     = new MenuCfgModule(this.uiService);
    this.sideBarMenuCfg       = mainMenuCfgObj.mainMenuCfg;
    
    //this.sideBarMenuCfg = [];
    console.debug('SidebarPageComponent',this.sideBarMenuCfg);
     
    }
    
  /* metodo per leggere la colore */
  getCssStyleColorSideBar(): string {
    return this.sidebarDataObj.cssStyleColorSideBar;
  }
  
  /* metodo per leggere la versione */
  getCssStyleLogoSideBar(): string {
    return this.sidebarDataObj.cssStyleLogoSideBar;
  }
  
  /* metodo per leggere la versione */
  getLogoImageClienteSideBar(): string {
    return this.sidebarDataObj.logoImageCliente;
  }
  
  /* metodo per leggere la versione */
  getLogoImageSirtiSideBar(): string {
    return this.sidebarDataObj.logoImageSirti;
  }
  
  /* metodo per leggere la versione */
  getNomeAppSideBar(): string {
    return this.sidebarDataObj.nomeApp;
  }
  
  /* metodo per leggere la versione */
  getMostraLogoCustomSideBar(): boolean {
    return this.sidebarDataObj.mostraLogoCustom;
  }
  
  
  
  
  

  ngOnInit(): void {
    const mainMenuCfgObj     = new MenuCfgModule(this.uiService);
    this.sideBarMenuCfg       = mainMenuCfgObj.mainMenuCfg;
    this.sidebarDataObj.cssStyleColorSideBar  = (( this.sidebarDataObj.cssStyleColorSideBar === '') ? 'sidebar-dark-indigo' : this.sidebarDataObj.cssStyleColorSideBar );
    this.sidebarDataObj.cssStyleLogoSideBar   = (( this.sidebarDataObj.cssStyleLogoSideBar === '') ? 'navbar-indigo' : this.sidebarDataObj.cssStyleLogoSideBar );
    this.sidebarDataObj.logoImageCliente      = (( this.sidebarDataObj.logoImageCliente === '') ? '' : this.sidebarDataObj.logoImageCliente );
    this.sidebarDataObj.logoImageSirti        = (( this.sidebarDataObj.logoImageSirti === '') ? '' : this.sidebarDataObj.logoImageSirti );
    this.sidebarDataObj.nomeApp               = (( this.sidebarDataObj.nomeApp === '') ? '' : this.sidebarDataObj.nomeApp );
    this.sidebarDataObj.mostraLogoCustom      = (( this.sidebarDataObj.mostraLogoCustom) ? true : false );

  }
  
  ngOnChanges(): void {
  }

}
