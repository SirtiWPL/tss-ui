import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneApprovatoComponent } from './azione-approvato.component';

describe('AzioneApprovatoComponent', () => {
  let component: AzioneApprovatoComponent;
  let fixture: ComponentFixture<AzioneApprovatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneApprovatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneApprovatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
