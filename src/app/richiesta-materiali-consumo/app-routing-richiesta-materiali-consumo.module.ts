import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RichiestaMaterialiConsumoComponent } from './richiesta-materiali-consumo.component';
import { PaginaProvaComponent } from '../admin-lte/common-component/pagina-prova/pagina-prova.component';
import { Page404RichiestaMaterialiConsumoComponent } from './page404-richiesta-materiali-consumo/page404-richiesta-materiali-consumo.component';
import { AuthGuard } from './../auth.guard';
import { ExternalIframeComponent } from '../admin-lte/common-component/external-iframe/external-iframe.component';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: RichiestaMaterialiConsumoComponent,
    data : {livello : 'richiestaMaterialiConsumo',icon : 'fas fa-clipboard-list',titolo:'Richiesta Materiali a Consumo'},
    children: [
      { path: 'nuovaRichiestaConsumo', component: ExternalIframeComponent,
        canActivate: [AuthGuard],
        data : {livello : 'nuovaRichiestaConsumo',icon : 'fas fa-edit',titolo:'Nuova Richiesta', urlCfg:'WIND_V2/#/ingaggioWind/nuovaRichiestaConsumo'}
      },
      { path: 'approvazioneRichiesteConsumo', component: ExternalIframeComponent,
        canActivate: [AuthGuard],
        data : {livello : 'approvazioneRichiesteConsumo',icon : 'fas fa-tasks',titolo:'Approvazione Richieste', urlCfg:'WIND_V2/#/ingaggioWind/GestioneRichiesteMatConsu'}
      },
      { path: 'statoRichiesteConsumo', component: ExternalIframeComponent,
        canActivate: [AuthGuard],
        data : {livello : 'statoRichiesteConsumo',icon : 'fas fa-list-alt',titolo:'Stato Richieste',urlCfg:'WIND_V2/#/ingaggioWind/ReportRichiesteMatConsu'}
      },
    ]
  },
  { path: '404', component: Page404RichiestaMaterialiConsumoComponent },
  
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingRichiestaMaterialiConsumoModule { }
