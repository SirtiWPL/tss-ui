import { Component, OnInit, Input, Injectable, Inject } from '@angular/core';
import { UiService } from './../ui-service.service';
import { RouterModule, Routes,ActivatedRoute, Router } from '@angular/router';
import { SidebarCfgModule } from '../config/sidebar-cfg.module' /* cfg del menu  */
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { environment } from 'src/environments/environment';

const authType           = environment.authType; /* per distinguere tra JWT O AUTH_PROXY */

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {
  public headerDataObj: any;

  public pageSidebarCfgObj : SidebarCfgModule;
  private authType           = authType; /* per distinguere tra JWT O AUTH_PROXY */
  

  isAuthenticationLoader : boolean;
  error404:boolean;
  errorMsg:any = {};
  public autenticato = false;

  public userProfileForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(
    public uiService: UiService,
    private route:ActivatedRoute,
    private router: Router ) {

    this.pageSidebarCfgObj  = new SidebarCfgModule();
    this.headerDataObj     = this.pageSidebarCfgObj.sidebarCfg;


    console.log('canActivate - LoginComponent 2',this.headerDataObj);
  }

  ngOnInit(): void {
    this.autenticato = false;
    this.pageSidebarCfgObj  = new SidebarCfgModule();
    this.headerDataObj     = this.pageSidebarCfgObj.sidebarCfg;

    this.isAuthenticationLoader = true;
    this.error404 = false;
    this.errorMsg = {};
    console.log('canActivate - LoginComponent 1');
    //this.uiService.configService().subscribe(( data )=>{
      this.uiService.getUserInfo().subscribe(( data )=>{
        console.log('canActivate - LoginComponent 2',data);
        this.isAuthenticationLoader = false;
        if (this.uiService) {
          this.autenticato = true;
          this.router.navigateByUrl('home');
        } else {
          this.error404 = true;
        }


        }, ( err )=>{
            console.debug('canActivate - LoginComponent 3:',err,sessionStorage);
            //this.router.navigateByUrl('login'); /* fix me per geocall */
            sessionStorage.setItem('ConfigUI', null);
            console.debug('canActivate - LoginComponent 4:',sessionStorage,this.uiService.getCurrentUser);
            this.isAuthenticationLoader = false;
            this.autenticato = false;
            if (this.authType === 'JWT') {
              
              this.error404 = false;
            }  else {
            this.error404 = true;
            this.errorMsg = err.status+' - '+err.statusText;
            this.errorMsg = {
              status      : err.status,
              statusText  : err.statusText
              };
            }

            });
    
      
    //});

  }

  login(): void {
    this.isAuthenticationLoader = true;
    this.autenticato = false;
    console.log(this.userProfileForm.value);

    const qParams = this.userProfileForm.value;

    this.uiService.configService(qParams).subscribe(( data )=>{
      console.log(data);
      this.isAuthenticationLoader = false;
      this.autenticato = true;
      this.uiService.getUserInfo().subscribe(( data )=>{
      this.router.navigateByUrl('home');
      });
      
    }, ( err )=>{
            console.debug('canActivate - LoginComponent 3:',err);
            //this.router.navigateByUrl('login'); /* fix me per geocall */
            this.isAuthenticationLoader = false;
            this.autenticato = false;
            if (this.authType === 'JWT') {
              
              this.error404 = false;
            }  else {
            this.error404 = true;
            this.errorMsg = err.status+' - '+err.statusText;
            this.errorMsg = {
              status      : err.status,
              statusText  : err.statusText
              };
            }

            });


  }

}
