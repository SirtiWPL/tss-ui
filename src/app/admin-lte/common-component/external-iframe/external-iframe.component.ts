import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { UiService } from './../../../ui-service.service';
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const apiURLAdvlogHomePage  = environment.apiUrlAdvlogHomePage;

@Component({
  selector: 'app-external-iframe',
  templateUrl: './external-iframe.component.html',
  styleUrls: ['./external-iframe.component.css']
})





export class ExternalIframeComponent implements OnInit {
  
  public livelloAttuale : string;
  public titoloPagina   : string;
  public urlCfg         : string;
  public urlSafe        : SafeResourceUrl;
  private UrlConfigPyramide   = apiURLAdvlogHomePage;
  

  constructor(private uiService: UiService,
              private activatedroute:ActivatedRoute,
              private router: Router,
              public sanitizer: DomSanitizer) {
    
    console.debug('sono ExternalIframeComponent => ngOnInit => valori:: ', this.activatedroute.snapshot.data);
    
      this.livelloAttuale = this.activatedroute.snapshot.data.livello;
      this.titoloPagina   = this.activatedroute.snapshot.data.titolo;
      this.urlCfg         = this.activatedroute.snapshot.data.urlCfg;    
    
    
    }

  ngOnInit(): void {
    
    console.debug('sono ExternalIframeComponent => ngOnInit');
    let indirizzoFull = this.UrlConfigPyramide+'/'+this.urlCfg+'?token='+this.uiService.getJwtToken.jwtToken;
    console.debug('sono ExternalIframeComponent => ngOnInit => indirizzoFull', indirizzoFull);
    this.urlSafe  = this.sanitizer.bypassSecurityTrustResourceUrl(indirizzoFull);
    
    

    
  }

}
