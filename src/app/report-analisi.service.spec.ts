import { TestBed } from '@angular/core/testing';

import { ReportAnalisiService } from './report-analisi.service';

describe('ReportAnalisiService', () => {
  let service: ReportAnalisiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReportAnalisiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
