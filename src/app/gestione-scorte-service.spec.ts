import { TestBed } from '@angular/core/testing';

import { GestioneScorteService } from './gestione-scorte.service';

describe('GestioneScorteService', () => {
  let service: GestioneScorteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestioneScorteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
