import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
import { User } from 'src/app/user.model';
import { map } from 'rxjs/operators';


const apiURL = environment.apiUrlWindServices;
const apiURLAdvlogHomePage = environment.apiUrlAdvlogHomePage;

@Injectable({
  providedIn: 'root'
})
export class VediAttivitaService {

   // url per servizio utility
   private UrlConfig = apiURL;
   private UrlConfigPyramide = apiURLAdvlogHomePage;

   constructor(
     private http: HttpClient
   ) {

   }


  getVediAttAttivita( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/attivita';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getVediAttStoria( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/storia';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getVediAttDTA( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/DTA';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getVediAttAllegati( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/allegati';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getVediAttAzioni( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/azioni';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  getVediAttRelazioni( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/relazioni';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  downloadFile(qParams:any ): any {
    const Url = this.UrlConfigPyramide + '/attivita/ws/ws_download_allegati.html';
    //return this.http.get('http://localhost:8080/employees/download', {responseType: 'blob'});
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  downloadFileRest( qParams:any ):any {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/vedi_attivita/download_allegati';
    return this.http.get(Url,{params : new HttpParams({ fromObject: qParams }),responseType: 'arraybuffer'});
  }
  
  

}
