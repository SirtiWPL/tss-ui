import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { MenuCfgModule } from '../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from './../ui-service.service';
import { GestioneScorteService } from './../gestione-scorte.service';


@Component({
  selector: 'app-gestione-scorte',
  templateUrl: './gestione-scorte.component.html',
  styleUrls: ['./gestione-scorte.component.css']
})

export class GestioneScorteComponent implements OnInit {
  
  navTileMenuCfgObj;
  navTileMenuCfg;
  livelloAttuale;
  
  livelloPassato:any;
  
  statistics:any;
  
  
  constructor(private activatedroute:ActivatedRoute,
              private router: Router,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) {
    console.log('funziona GestioneScorteComponent');
    console.log('funziona canActivate GestioneScorteComponent',this.uiService);
    
    this.navTileMenuCfgObj = new MenuCfgModule(this.uiService);
    this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
    this.livelloPassato = this.buildLivello(this.activatedroute.root);
    this.livelloAttuale = this.activatedroute.snapshot.data.livello;
    

        
        
    /*if ((this.livelloPassato === 'gestioneRichieste') || (this.livelloPassato === 'gestioneScorte') ||
        (this.livelloAttuale === 'gestioneRichieste') || (this.livelloAttuale === 'gestioneScorte')
        ) {*/
    if ((this.livelloPassato === 'gestioneRichieste') || (this.livelloAttuale === 'gestioneRichieste')) { 
      
      this.statistics = {};
      
      const qParamsNumRichAttiveOperatore = {
        RICHIEDENTE     : 'INTERNO',
      };
      
      this.gestScorteServices.getNumRichAttiveOperatore(qParamsNumRichAttiveOperatore).subscribe(( data )=>{
        
        console.debug('sono GestioneScorteComponent =>  getNumRichAttiveOperatore: ', data.data.results );
        console.debug('sono GestioneScorteComponent =>  livelloPassato: ', this.livelloPassato );
        console.debug('sono GestioneScorteComponent =>  livelloAttuale: ', this.livelloAttuale );
        
        for (let col in data.data.results) {
          this.statistics[data.data.results[col].TIPO] = data.data.results[col].TOTALE;
        }
        
        console.debug('sono GestioneScorteComponent =>  statistics: ', this.statistics );
        
      });
      
    }
        


    }

  ngOnInit(): void {
    
    console.log('funziona canActivate GestioneScorteComponent',this.uiService);
    
     /* this.activatedroute.data.subscribe(data => {
          this.livelloPassato=data.livello;
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
      });*/
      
    console.debug('qui:', this.activatedroute.snapshot.data.livello); 
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.livelloPassato = this.buildLivello(this.activatedroute.root);
        this.livelloAttuale = this.activatedroute.snapshot.data.livello;
        
        
        
        /*if ((this.livelloPassato === 'gestioneRichieste') || (this.livelloPassato === 'gestioneScorte') ||
            (this.livelloAttuale === 'gestioneRichieste') || (this.livelloAttuale === 'gestioneScorte')
            ) {*/
        if ((this.livelloPassato === 'gestioneRichieste') || (this.livelloAttuale === 'gestioneRichieste')) {          
          this.statistics = {};
          
          const qParamsNumRichAttiveOperatore = {
            RICHIEDENTE     : 'INTERNO',
          };
          
          this.gestScorteServices.getNumRichAttiveOperatore(qParamsNumRichAttiveOperatore).subscribe(( data )=>{
            
            console.debug('sono GestioneScorteComponent =>  getNumRichAttiveOperatore: ', data.data.results );
            console.debug('sono GestioneScorteComponent =>  livelloPassato: ', this.livelloPassato );
            console.debug('sono GestioneScorteComponent =>  livelloAttuale: ', this.livelloAttuale );
            
            for (let col in data.data.results) {
              this.statistics[data.data.results[col].TIPO] = data.data.results[col].TOTALE;
            }
            
            console.debug('sono GestioneScorteComponent =>  statistics: ', this.statistics );
            
          });
          
        }
        
        
    })
    
    
    console.debug('sono GestioneScorteComponent 1: ',this.navTileMenuCfg);
    console.debug('sono GestioneScorteComponent 1: ',this.livelloPassato);
    
  }

  ngOnChanges(): void {
    
    console.debug('sono GestioneScorteComponent 2: ',this.navTileMenuCfg);
    console.debug('sono GestioneScorteComponent 2: ',this.livelloPassato);
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
  }
  
  buildLivello(route: ActivatedRoute): string {
      console.debug('buildBreadCrumb: ',route);
      let livello = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      if (route.firstChild) {

          return this.buildLivello(route.firstChild);
      }
      return livello;
  } 

}

