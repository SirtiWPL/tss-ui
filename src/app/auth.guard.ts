import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router,CanLoad,Route,UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { UiService } from './ui-service.service';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode';
//import { HttpClient, HttpHeaders,HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private uiService: UiService) {
    //this.header = new HttpHeaders();
   // console.debug('NuovaRichiestaComponent => decodedJwt 2',this.header.getAll('content-type'));
    
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    //const currentUser = {};
    const currentUser = this.uiService.getCurrentUser; /* rimesso */
    console.debug('AuthGuard => canActivate => next',next);
    console.debug('AuthGuard => canActivate => state',state);
    
    console.debug('AuthGuard => canActivate => currentUser: ',currentUser);

    if (currentUser) {
      
      console.debug('AuthGuard => canActivate => currentUser = si',currentUser);
      if (this.isVisibileUrl(state.url,currentUser)) {
        console.debug('AuthGuard => canActivate => sei abilitato',currentUser);
        //this.router.navigateByUrl(state.url);
        return true;
      } else {
        console.debug('AuthGuard => canActivate => non sei abilitato',currentUser);
        //this.router.navigateByUrl('404');
        this.router.navigateByUrl('login');
        return false;
        
      }
    } else {
        console.debug('AuthGuard => canActivate => currentUser = NO',currentUser);  
        //this.uiService.configService().subscribe(( data )=>{
          console.debug('AuthGuard => canActivate => getUserInfo');  
          this.uiService.getUserInfo().subscribe(( data )=>{
            
            console.debug('AuthGuard => canActivate => getUserInfo=si',data);

            if (this.uiService) {
              console.debug('AuthGuard => canActivate => uiService = si',this.uiService.getCurrentUser);

              if (this.isVisibileUrl(state.url,this.uiService.getCurrentUser)) {
                console.debug('AuthGuard => canActivate => uiService => sei abilitato',this.uiService.getCurrentUser);
                this.router.navigateByUrl(state.url);
                return true;
              } else {
                console.debug('AuthGuard => canActivate => uiService => non sei abilitato',this.uiService.getCurrentUser);
                //this.router.navigateByUrl('404');
                this.router.navigateByUrl('login');
                return false;
              }

            } else {
              console.debug('AuthGuard => canActivate => uiService = no',this.uiService.getCurrentUser);
               //this.router.navigateByUrl('404');
               this.router.navigateByUrl('login');
               return false;
            }


            }, ( err )=>{
              console.debug('AuthGuard => canActivate => getUserInfo=no',err);
              //this.isAuthenticationLoader = false;
              this.router.navigateByUrl('login');
              });
        // });
    }

  }

  /* funzione che gestisce le autorizzazioni custom di visibilita del menu routing */
  public isVisibileUrl(url:string,currentUser:any):boolean {
    console.debug('AuthGuard => isVisibileUrl =>',url,currentUser);
    let isAbilitato = true;

    if (url==='/gestioneScorte/ricercaIdSirti') {
      isAbilitato = !currentUser.isRoot && currentUser.isOperatoreAziendaEsterna ? false : true;
    }
    if (url==='/gestioneScorte/approvazioneHq') {
      isAbilitato = currentUser.isRoot || currentUser.isWindHq ? true : false;
    }
    if (url==='/gestioneScorte/trasferimentoWind') {
      isAbilitato = currentUser.isAbilitatoTrasfermento;
    }
    if (url==='/gestioneScorte/gestCompMat') {
      isAbilitato = currentUser.isAbilitatoGestCompatibili;
    }
    if (url==='/gestioneScorte/presidiMagazzini') {
      isAbilitato = currentUser.isAbilitatoPresidioMagaz;
    }
    if (url==='/logistica') {
      isAbilitato = currentUser.isAbilitatoMenuLogistica;
    }
    if (url==='/logistica/prelieviDaMagazzini') {
      isAbilitato = currentUser.isAbilitatoRichFornitura;
    }
    if (url==='/logistica/altreAttivita') {
      isAbilitato = currentUser.isAbilitatoMinerva;
    }
    if (url==='/logistica/vincoloSvincoloMateriali') {
      isAbilitato = currentUser.isAbilitatoVincoloSvincolo;
    }
    if (url==='/reportAnalisi') {
      isAbilitato = !currentUser.isRoot && currentUser.isOperatoreAziendaEsterna ? false : true;
    }
    if (url==='/reportAnalisi/kpiGrezzi') {
      isAbilitato = currentUser.isRoot || (!currentUser.isOperatoreAziendaEsterna  && currentUser.isAbilitatoKpi) ? true : false;
    }
    if (url==='/reportAnalisi/datiGrezzi') {
      isAbilitato = currentUser.isRoot || (!currentUser.isOperatoreAziendaEsterna  && currentUser.isAbilitatoKpi) ? true : false;
    }
    if (url==='/documentazione') {
      isAbilitato = !currentUser.isRoot && currentUser.isOperatoreAziendaEsterna ? false : true;
    }
    if (url==='/richiestaMaterialiConsumo') {
      isAbilitato = currentUser.isAbilitatoMenuRicMatConsumo;
    }
    if (url==='/richiestaMaterialiConsumo/nuovaRichiestaConsumo') {
      isAbilitato = currentUser.isAbilitatoNuovaRicMatConsumo; 
    }
    if (url==='/richiestaMaterialiConsumo/approvazioneRichiesteConsumo') {
      isAbilitato = currentUser.isAbilitatoAppRicMatConsumo;
    }
    if (url==='/richiestaMaterialiConsumo/statoRichiesteConsumo') {
      isAbilitato = currentUser.isAbilitatoStatoRicMatConsumo;
    }

    return isAbilitato;


  }
  
  
  getDecodedAccessToken(token: string): any {
    console.debug('NuovaRichiestaComponent => getDecodedAccessToken => decodedJwt',token);
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }


}

