import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneSospesoComponent } from './azione-sospeso.component';

describe('AzioneSospesoComponent', () => {
  let component: AzioneSospesoComponent;
  let fixture: ComponentFixture<AzioneSospesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneSospesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneSospesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
