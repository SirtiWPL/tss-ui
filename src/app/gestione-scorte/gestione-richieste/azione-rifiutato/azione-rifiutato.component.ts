import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-rifiutato',
  templateUrl: './azione-rifiutato.component.html',
  styleUrls: ['./azione-rifiutato.component.css']
})
export class AzioneRifiutatoComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzRifiutato: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzRifiutato: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService             
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneRifiutatoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzRifiutato */

    this.reactiveFormAzRifiutato = this.formBuilderAzRifiutato.group({
      sCausaleCheck            : [null],
      sRichiedenteRifCheck     : [null],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneRifiutatoComponent => eseguiAzione: ',this.reactiveFormAzRifiutato.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
      CAUSALE         : this.reactiveFormAzRifiutato.value.sCausaleCheck,
      RICHIEDENTE_RIF : this.reactiveFormAzRifiutato.value.sRichiedenteRifCheck,
    };
    
    this.gestScorteServices.rifiutatoRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneRifiutatoComponent => rifiutatoRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
      }

    });
    
  }

}
