import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompatibiliEquivalentiComponent } from './compatibili-equivalenti.component';

describe('CompatibiliEquivalentiComponent', () => {
  let component: CompatibiliEquivalentiComponent;
  let fixture: ComponentFixture<CompatibiliEquivalentiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompatibiliEquivalentiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompatibiliEquivalentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
