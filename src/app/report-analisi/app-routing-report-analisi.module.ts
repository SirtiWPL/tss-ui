import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReportAnalisiComponent } from './report-analisi.component';
import { PaginaProvaComponent } from '../admin-lte/common-component/pagina-prova/pagina-prova.component';
import { AuthGuard } from './../auth.guard';
import { ReportComponent } from './report/report.component';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: ReportAnalisiComponent,
    data : {livello : 'reportAnalisi',icon : 'fas fa-chart-bar',titolo:'Report e Analisi'},
    children: [
      { path: 'mappaGiacenze',
        component: PaginaProvaComponent,
        data : {livello : 'mappaGiacenze',icon : 'fas fa-map-marked-alt',titolo:'Mappa Giacenze'}
      },
      { path: 'report',
        component: ReportComponent,
        data : {livello : 'report',icon : 'fas fa-chart-line',titolo:'Report'}
      },
      { /*routing - kpiGrezzi  - Inizio  */
      path: 'kpiGrezzi',
      component: ReportAnalisiComponent,
      canActivate: [AuthGuard],
      data : {livello : 'kpiGrezzi',icon : 'far fa-list-alt',titolo:'KPI Grezzi'},
      children: [
        { /*routing - kpiAnswerRichiesteSp  - Inizio  */
          path: 'kpiAnswerRichiesteSp',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiAnswerRichiesteSp',icon : 'far fa-folder',titolo:'KPI Answer Richieste SP'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 1'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 1'}
            },
          ]
        }, /*routing - kpiAnswerRichiesteSp  - fine  */
        { /*routing - kpiReso  - Inizio  */
          path: 'kpiReso',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiReso',icon : 'far fa-folder',titolo:'KPI Reso'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 2'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 2'}
            },
          ]
        }, /*routing - kpiReso  - fine  */
        { /*routing - kpiRiparazioneKg  - Inizio  */
          path: 'kpiRiparazioneKg',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRiparazioneKg',icon : 'far fa-folder',titolo:'KPI Riparazione'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 3'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 3'}
            },
          ]
        }, /*routing - kpiRiparazioneKg  - fine  */
        { /*routing - kpiRqVendorNr  - Inizio  */
          path: 'kpiRqVendorNr',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqVendorNr',icon : 'far fa-folder',titolo:'KPI Repair Quality - Vendor NR'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 4'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 4'}
            },
          ]
        }, /*routing - kpiRqVendorNr  - fine  */
        { /*routing - kpiRqVendorRgPerDm  - Inizio  */
          path: 'kpiRqVendorRgPerDm',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqVendorRgPerDm',icon : 'far fa-folder',titolo:'KPI Repair Quality - Vendor RG per dominio tecnologico'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 5'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 5'}
            },
          ]
        }, /*routing - kpiRqVendorRgPerDm  - fine  */
        { /*routing - kpiRqVendorRgPerRip  - Inizio  */
          path: 'kpiRqVendorRgPerRip',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqVendorRgPerRip',icon : 'far fa-folder',titolo:'KPI Repair Quality - Vendor RG per riparatore'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 6'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 6'}
            },
          ]
        }, /*routing - kpiRqVendorRgPerRip  - fine  */
      ]
      }, /*routing - kpiGrezzi  - Fine */
      { /*routing - datiGrezzi  - Inizio  */
      path: 'datiGrezzi',
      component: ReportAnalisiComponent,
      canActivate: [AuthGuard],
      data : {livello : 'datiGrezzi',icon : 'fas fa-chart-area',titolo:'Dati grezzi'},
      children: [
        { /*routing - kpiRiparazioneDg  - Inizio  */
          path: 'kpiRiparazioneDg',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRiparazioneDg',icon : 'far fa-folder',titolo:'KPI Riparazione'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 1'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 1'}
            },
          ]
        }, /*routing - kpiRiparazioneDg  - fine  */
        { /*routing - kpiRqRmaChiuse  - Inizio  */
          path: 'kpiRqRmaChiuse',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqRmaChiuse',icon : 'far fa-folder',titolo:'KPI Repair Quality - RMA CHIUSE'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 2'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 2'}
            },
          ]
        }, /*routing - kpiRqRmaChiuse  - fine  */
        { /*routing - kpiRqRmaRifiutoMatAcc  - Inizio  */
          path: 'kpiRqRmaRifiutoMatAcc',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqRmaRifiutoMatAcc',icon : 'far fa-folder',titolo:'KPI Repair Quality - RMA Rifiuto Materiali Accettati'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 3'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 3'}
            },
          ]
        }, /*routing - kpiRqRmaRifiutoMatAcc  - fine  */
        { /*routing - kpiRqRmaIrriparabili  - Inizio  */
          path: 'kpiRqRmaIrriparabili',
          component: ReportAnalisiComponent,
          canActivate: [AuthGuard],
          data : {livello : 'kpiRqRmaIrriparabili',icon : 'far fa-folder',titolo:'KPI Repair Quality - RMA IRRIPARABILI'},
          children: [
            { path: 'meseCorrente',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'meseCorrente',icon : 'fas fa-calendar-alt',titolo:'Mese Corrente 4'}
            },
            { path: 'storico',
              component: PaginaProvaComponent,
              canActivate: [AuthGuard],
              data : {livello : 'storico',icon : 'fas fa-calendar',titolo:'Storico 4'}
            },
          ]
        }, /*routing - kpiRqRmaIrriparabili  - fine  */
      ]
      }, /*routing - datiGrezzi  - Fine */
    ]
  }              
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingReportAnalisiModule { }
