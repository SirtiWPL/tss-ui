import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportAnalisiComponent } from './report-analisi.component';

describe('ReportAnalisiComponent', () => {
  let component: ReportAnalisiComponent;
  let fixture: ComponentFixture<ReportAnalisiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportAnalisiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportAnalisiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
