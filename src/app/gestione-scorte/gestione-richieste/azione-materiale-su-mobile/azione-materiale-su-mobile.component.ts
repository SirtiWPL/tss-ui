import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-materiale-su-mobile',
  templateUrl: './azione-materiale-su-mobile.component.html',
  styleUrls: ['./azione-materiale-su-mobile.component.css']
})
export class AzioneMaterialeSuMobileComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzMaterialeSuMobile: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzMaterialeSuMobile: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService                  
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneMaterialeSuMobileComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzMaterialeSuMobile */

    this.reactiveFormAzMaterialeSuMobile = this.formBuilderAzMaterialeSuMobile.group({
      nIdSirtiCheck       : [null,[Validators.required]],
      sSerialNumberCheck  : [null,[Validators.required]],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    
    /* pre-compilo i campi edit previsti */
    
    this.reactiveFormAzMaterialeSuMobile.get("nIdSirtiCheck").setValue(this.datiAzioneObj.materiale.ID_SIRTI, {onlySelf: false});
    this.reactiveFormAzMaterialeSuMobile.get("sSerialNumberCheck").setValue(this.datiAzioneObj.materiale.SERIAL_NUMBER, {onlySelf: false});
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneMaterialeSuMobileComponent => eseguiAzione: ',this.reactiveFormAzMaterialeSuMobile.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
    };
    
    this.gestScorteServices.matSuMobileRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneMaterialeSuMobileComponent => matSuMobileRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}
