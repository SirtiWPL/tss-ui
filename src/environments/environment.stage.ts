export const environment = {
  production: true,
  apiUrlAdvlogHomePage      : 'https://testservices.sirti.net',
  apiUrlAdvlogProfile       : 'https://testservices.sirti.net/artadvlog/advlogws/core/profile',
  apiUrlWindServices        : 'https://testservices.sirti.net/auth/geocall/windtrews/windtrews/core',
  apiUrlWindServicesRoot    : 'https://testservices.sirti.net/auth/geocall/windtrews/windtrews',
  apiUrlAdvlogWs            : 'https://testservices.sirti.net/artadvlog/advlogws',
  injectXWebAuthUser        : true, /* true x AUTH_PROXY, false per JWT */
  authType                  : 'AUTH_PROXY', /* JWT O AUTH_PROXY */
  xWebAuthUser              : 'ROSSIP',
  JWT: ''
};
