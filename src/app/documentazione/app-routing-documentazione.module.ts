import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DocumentazioneComponent } from './documentazione.component';
import { PaginaProvaComponent } from '../admin-lte/common-component/pagina-prova/pagina-prova.component';
import { AuthGuard } from './../auth.guard';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: DocumentazioneComponent,
    data : {livello : 'documentazione',icon : 'fas fa-file',titolo:'Documentazione'},
    children: [/*
      { path: '01', component: DocumentoComponent,
        canActivate: [AuthGuard],
        data : {livello : '01',icon : 'far fa-file-pdf',titolo:'Manuale Operativo'}
      },
      { path: '02', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '02',icon : 'far fa-file-pdf',titolo:'Logistica Manuale Operativo'}
      },
      { path: '03', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '03',icon : 'far fa-file-excel',titolo:'Modulo di Carico/Scarico'}
      },
      { path: '04', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '04',icon : 'far fa-file-pdf',titolo:'Dettagli Report Pyramide'}
      },
      { path: '05', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '05',icon : 'far fa-file-pdf',titolo:'Norma Gestione scorte guaste'}
      },
      { path: '06', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '06',icon : 'far fa-file-pdf',titolo:'FAQ Pyramide'}
      },
      { path: '07', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '07',icon : 'fas fa-link',titolo:'Documentazione Tecnica Scorte e Logistica (Link Teams)'}
      },
      { path: '08', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '08',icon : 'far fa-file-powerpoint',titolo:'PROCEDURA RICHIESTA SCORTE MTNE HUAWEI'}
      },
      { path: '09', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : '09',icon : 'fas fa-link',titolo:'Sedi GLS'}
      },*/
    ]
  }              
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingDocumentazioneModule { }
