import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresidioMagazziniComponent } from './presidio-magazzini.component';

describe('PresidioMagazziniComponent', () => {
  let component: PresidioMagazziniComponent;
  let fixture: ComponentFixture<PresidioMagazziniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresidioMagazziniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresidioMagazziniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
