import { Component, OnInit,Input,OnChanges,Inject  } from '@angular/core';
import { MenuCfgModule } from '../../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from '../../ui-service.service';
import { GestioneScorteService } from './../../gestione-scorte.service';
import { DOCUMENT } from '@angular/common';


type tMagazziniWind = {
  ID_MAG_SIRTI                      : number,
  SEDE                              : string,
  INDIRIZZO                         : string,
  COMUNE                            : string,
  PROVINCIA                         : string,
  LATITUDINE                        : string,
  LONGITUDINE                       : string,
  PROPRIETA                         : string,
  MAGAZZINO_SIRTI_CORRISPONDENTE    : string,
  DISTANZA_MAG_SIRTI_CORR_METRI     : number,
  CAP                               : string,
  ID_OPERATORE_RESPONSABILE_ZONA    : string,
  ZONA                              : string,
  REGIONE                           : string
  };

@Component({
  selector: 'app-gestione-richieste',
  templateUrl: './gestione-richieste.component.html',
  styleUrls: ['./gestione-richieste.component.css']
})
export class GestioneRichiesteComponent implements OnInit {

  navTileMenuCfgObj;
  navTileMenuCfg;
  
  livelloAttuale;
  titoloPagina;
  sModuloChiamante;
  sNomeOggettoTabella;
  sTipoRichiesta;
  
  livelloPassato:any;
  
  public aMagazziniOperatore: tMagazziniWind[];
  public richiedente:string = 'INTERNO';
  public aRichiesteAperte;
  public aCfgColonne;
  public loaderSearch:boolean = false;
  public loaderOnSelectRow:boolean = false;
  public visualizzaSelectRow:boolean = false;
  public gridConfig:any = {};
  public rowSelected:any;
  public infoElement = {};
  public listaAzioniPossibili = [];
  public apriAzioneSelezionata:boolean;
  public isAzioneEseguita:boolean;
  
  public azioniPossibiliCfg = {
      RMA_WIND : {
                    APPROVATO : [
                                  {
                                  nome      : 'RIFIUTATO',
                                  etichetta : 'Rifiutato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                  }          
                                ],
                    APPROVAZIONE_HQ : [
                                {
                                  nome      : 'APPROVATO',
                                  etichetta : 'Approvato',
                                  icona     : 'fas fa-thumbs-up',
                                  visible   : true
                                },  
                                {
                                  nome      : 'RIFIUTATO',
                                  etichetta : 'Rifiutato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },                                                              
                                ],
                    ATTESA_MATERIALE : [
                                {
                                  nome      : 'ANNULLAMENTO_ATTESA_MATERIALE',
                                  etichetta : 'Annullamento Attesa Materiale',
                                  icona     : 'fas fa-ban',
                                  visible   : true
                                },  
                                {
                                  nome      : 'CAMBIO_PRIORITA',
                                  etichetta : 'Cambio Priorita\'',
                                  icona     : 'fas fa-exchange-alt',
                                  visible   : true
                                },
                                {
                                  nome      : 'APRI_RIPIN',
                                  etichetta : 'Apri Ripin',
                                  icona     : 'fas fa-edit',
                                  visible   : false
                                },  
                                ],
                    CONSEGNA_ACCETTATA : [
                                {
                                  nome      : 'MATERIALE_UTILIZZATO',
                                  etichetta : 'Materiale utilizzato',
                                  icona     : 'fas fa-box-open',
                                  visible   : true
                                },  
                                {
                                  nome      : 'RIENTRO_A_MAGAZZINO',
                                  etichetta : 'Rientro al magazzino',
                                  icona     : 'fas fa-warehouse',
                                  visible   : true
                                },
                                {
                                  nome      : 'RIFIUTO_MATERIALE_ACCETTATO',
                                  etichetta : 'Rifiuto materiale Accettato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },                                                                  
                                          
                                          
                                ],
                    CONSEGNATO  : [
                                {
                                  nome      : 'MATERIALE_UTILIZZATO',
                                  etichetta : 'Materiale utilizzato',
                                  icona     : 'fas fa-box-open',
                                  visible   : true
                                },  
                                {
                                  nome      : 'RIENTRO_A_MAGAZZINO',
                                  etichetta : 'Rientro al magazzino',
                                  icona     : 'fas fa-warehouse',
                                  visible   : true
                                },
                                {
                                  nome      : 'RIFIUTO_MATERIALE_ACCETTATO',
                                  etichetta : 'Rifiuto materiale Accettato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },  
                                ],
                    SPEDITA_DA_CONFERMARE_RICEZIONE : [
                                {
                                  nome      : 'MATERIALE_UTILIZZATO',
                                  etichetta : 'Materiale utilizzato',
                                  icona     : 'fas fa-box-open',
                                  visible   : true
                                },  
                                {
                                  nome      : 'RIENTRO_A_MAGAZZINO',
                                  etichetta : 'Rientro al magazzino',
                                  icona     : 'fas fa-warehouse',
                                  visible   : true
                                },
                                {
                                  nome      : 'RIFIUTO_MATERIALE_ACCETTATO',
                                  etichetta : 'Rifiuto materiale Accettato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },                                                                             
                                                        
                                ],
                    CORRIERE  : [
                                {
                                  nome      : 'MSG_CUSTOM',
                                  etichetta : 'Attesa Riscontro consegna corriere',
                                  icona     : '',
                                  visible   : true
                                },                                     
                                ],
                    MATERIALE_SU_MOBILE : [
                                {
                                  nome      : 'MATERIALE_UTILIZZATO',
                                  etichetta : 'Materiale utilizzato',
                                  icona     : 'fas fa-box-open',
                                  visible   : true
                                },  
                                {
                                  nome      : 'RIENTRO_A_MAGAZZINO',
                                  etichetta : 'Rientro al magazzino',
                                  icona     : 'fas fa-warehouse',
                                  visible   : true
                                },                                                                 
                                ],
                    RITIRO_MATERIALE  : [
                                {
                                  nome      : 'ANNULLAMENTO_PRENOTAZIONE',
                                  etichetta : 'Annullamento Prenotazione',
                                  icona     : 'fas fa-ban',
                                  visible   : true
                                },  
                                {
                                  nome      : 'MATERIALE_SU_MOBILE',
                                  etichetta : 'Materiale Su Mobile',
                                  icona     : 'fas fa-truck-loading',
                                  visible   : true
                                },                                                                
                                ],
                  },
      RESO  :   {
                    APERTA    : [
                                {
                                  nome      : 'ANNULLAMENTO_RESO',
                                  etichetta : 'Annullamento Reso',
                                  icona     : 'fas fa-ban',
                                  visible   : true
                                },  
                                {
                                  nome      : 'CONFERMA_SOSPENSIONE',
                                  etichetta : 'Conferma Sospensione',
                                  icona     : 'fas fa-clipboard-check',
                                  visible   : true
                                },
                                {
                                  nome      : 'SOSPESO',
                                  etichetta : 'Sospeso',
                                  icona     : 'fas fa-hand-paper',
                                  visible   : true
                                },                                                         
                                ],
                    ASSEGNATO : [
                                {
                                  nome      : 'ANNULLAMENTO_RESO',
                                  etichetta : 'Annullamento Reso',
                                  icona     : 'fas fa-ban',
                                  visible   : true
                                },
                                {
                                  nome      : 'SOSPESO',
                                  etichetta : 'Sospeso',
                                  icona     : 'fas fa-hand-paper',
                                  visible   : true
                                },  
                                ],
                    SOSPESO   : [
                                  {
                                  nome      : 'ANNULLAMENTO_SOSPENSIONE',
                                  etichetta : 'Annullamento Sospensione',
                                  icona     : 'fas fa-ban',
                                  visible   : true
                                },  
                                {
                                  nome      : 'CONFERMA_SOSPENSIONE',
                                  etichetta : 'Conferma Sospensione',
                                  icona     : 'fas fa-clipboard-check',
                                  visible   : true
                                }

                                ],
                  },
      TRASFERIMENTO : {
                          APPROVATO : [
                                {
                                  nome      : 'MSG_CUSTOM',
                                  etichetta : 'CHIEDERE AL PM DI ANNULLARE LA RICHIESTA DI TRASFERIMENTO',
                                  icona     : '',
                                  visible   : true
                                },                                                   
                          ]
                        },
      REINTEGRO  : {},
      CARICO_MATERIALI  : {},
      LOGISTICA_WIND  :   {
                    ATTESA_APPROVAZIONE    : [
                                {
                                  nome      : 'ANNULLATA',
                                  etichetta : 'Annullata',
                                  icona     : 'fas fa-trash',
                                  visible   : true
                                },  
                                {
                                  nome      : 'APPROVATO',
                                  etichetta : 'Approvato',
                                  icona     : 'fas fa-thumbs-up',
                                  visible   : true
                                },  
                                ],
                    CONSEGNATO : [
                                {
                                  nome      : 'ANNULLATA',
                                  etichetta : 'Annullata',
                                  icona     : 'fas fa-trash',
                                  visible   : true
                                },  
                                {
                                  nome      : 'CONSEGNA_ACCETTATA',
                                  etichetta : 'Consegna Accettata',
                                  icona     : 'fas fa-thumbs-up',
                                  visible   : true
                                },  
                                {
                                  nome      : 'MATERIALE_RIFIUTATO',
                                  etichetta : 'Materiale Rifiutato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },
                                ],
                    SPEDITA_DA_CONFERMARE_RICEZIONE   : [
                                {
                                  nome      : 'ANNULLATA',
                                  etichetta : 'Annullata',
                                  icona     : 'fas fa-trash',
                                  visible   : true
                                },  
                                {
                                  nome      : 'CONSEGNA_ACCETTATA',
                                  etichetta : 'Consegna Accettata',
                                  icona     : 'fas fa-thumbs-up',
                                  visible   : true
                                },  
                                {
                                  nome      : 'MATERIALE_RIFIUTATO',
                                  etichetta : 'Materiale Rifiutato',
                                  icona     : 'fas fa-thumbs-down',
                                  visible   : true
                                },
                                ],
                  },
    };
    
  public azioneSelezionataObj:any = {
    nome      : null,
    etichetta : null,
    icona     : null,
    tipo      : null,
    stato     : null,
  };
  
  constructor(private activatedroute:ActivatedRoute,
              private router: Router,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService,
              @Inject(DOCUMENT) private document: Document) {
    console.debug('funziona GestioneRichiesteComponent');
    this.navTileMenuCfgObj = new MenuCfgModule(this.uiService);
    this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
    this.livelloPassato   = this.buildLivello(this.activatedroute.root);
    this.livelloAttuale   = this.activatedroute.snapshot.data.livello;
    this.titoloPagina     = this.activatedroute.snapshot.data.titolo;
    this.sTipoRichiesta   = this.activatedroute.snapshot.data.sTipoRichiesta;
    this.sModuloChiamante = this.activatedroute.snapshot.data.sModuloChiamante;

    

  }

  ngOnInit(): void {
    
     
    

    
   
    this.loaderSearch           = true;
    this.visualizzaSelectRow    = false;
    this.apriAzioneSelezionata  = false;
    
    console.debug('GestioneRichiesteComponent => user: ',this.uiService.getCurrentUser);
    /*console.debug('GestioneRichiesteComponent => token: ',this.uiService.getJwtToken.jwtToken);*/
    
      /* recupero i magazzini dell'operatore - Inizio */

      const qParamsMagazziniOp = {
        RICHIEDENTE: this.richiedente
      };

      this.gestScorteServices.getWarehouses(qParamsMagazziniOp).subscribe(( data )=>{
        console.debug( data );
        this.aMagazziniOperatore = data.data.results;
        console.debug('GestioneRichiesteComponent => Magazzini Operatore: ',this.aMagazziniOperatore);
        
        
        /* recupero la lista delle richieste aperte - Inizio */
  
        const qParamsOpenRequests = {
          RICHIEDENTE     : this.richiedente,
          TIPO_RICHIESTA  : this.sTipoRichiesta
        };
        
        if ((this.sModuloChiamante) && (this.sModuloChiamante === 'MOD_GEST_RICHIESTE')) { /* MOD_GEST_RICHIESTE */
  
          this.gestScorteServices.getOpenRequests(qParamsOpenRequests).subscribe(( data )=>{
            //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
            this.aRichiesteAperte = data.data.results;
            console.debug('GestioneRichiesteComponent => Richieste Operatore: ',this.aRichiesteAperte);
            
            
            
            /* recupero la lista delle colonne - Inizio */
      
            const qParamsGridColumns = {
              RICHIEDENTE     : this.richiedente,
              CHIAMANTE       : this.sModuloChiamante,
              MODULO          : this.sModuloChiamante,
              TIPO_RICHIESTA  : this.sTipoRichiesta
            };
      
            this.gestScorteServices.getGridColumns(qParamsGridColumns).subscribe(( data )=>{
              //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
              this.aCfgColonne = data.data.results;
              console.debug('GestioneRichiesteComponent => cfg colonne: ',this.aCfgColonne);
       
              let customFilter = [];
       
              for (let col in this.aCfgColonne) {
                 
                this.infoElement[this.aCfgColonne[col].NOME_COLONNA] = {  ETICHETTA : this.aCfgColonne[col].LABEL,
                                                                          TIPO      : this.aCfgColonne[col].TIPO,
                                                                          POSIZIONE : this.aCfgColonne[col].POSIZIONE,
                
                                                                        };
                /* COSTRUISCO IL FILTRI CUSTOM per la grid */                                                        
                if (this.aCfgColonne[col].NOME_COLONNA === 'STATO') {
                  
                  
                  console.debug('GestioneRichiesteComponent => cfg colonne => filtro STATO');
                  
                  const qParamsStatiGestRich = {
                    RICHIEDENTE     : this.richiedente,
                    TIPO_RICHIESTA  : this.sTipoRichiesta
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => cfg colonne => filtro STATO => qParamsStatiGestRich',qParamsStatiGestRich);
                  
                  this.gestScorteServices.getStatiGestRich(qParamsStatiGestRich).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent => cfg colonne => filtro STATO =>  getStatiGestRich: ', data.data.results );
                    
                    let aStatiDB = [];
                    for (let nStato in data.data.results) {
                      aStatiDB.push(data.data.results[nStato].STATO);
                    }
                    
                    customFilter[this.aCfgColonne[col].NOME_COLONNA+'-filter'] = {
                      TIPO    : this.aCfgColonne[col].TIPO,
                      INPUT   : 'SELECT',
                      VALORI  : aStatiDB
                      };
                
                    
                  });                    
                  

                } else if (this.aCfgColonne[col].NOME_COLONNA === 'RESO_PRESSO_GLS') {
                  customFilter[this.aCfgColonne[col].NOME_COLONNA+'-filter'] = {
                    TIPO    : this.aCfgColonne[col].TIPO,
                    INPUT   : 'SELECT',
                    VALORI  : ['SEDE WIND','PUNTO GLS']
                    };
                }
                /*
                else if ((this.aCfgColonne[col].NOME_COLONNA === 'MAGAZZINO_PARTENZA') || (this.aCfgColonne[col].NOME_COLONNA === 'MAGAZZINO_DESTINAZIONE') ) {
                  
                  let filterMag = [];
                  for (let magOp in this.aMagazziniOperatore) {
                    filterMag.push(this.aMagazziniOperatore[magOp].SEDE);
                  }
                  
                  customFilter[this.aCfgColonne[col].NOME_COLONNA+'-filter'] = {
                    TIPO    : this.aCfgColonne[col].TIPO,
                    INPUT   : 'SELECT',
                    VALORI  : filterMag
                    };
                }
                */
                
                else {
                }
                
                
                //this.aMagazziniOperatore
              }
              
              this.loaderSearch = false;
              /* cfg della griglia */
              this.gridConfig = {
                nColonneInput     : null,
                abilitaDettaglio  : true,
                abilitaSelectAll  : false,
                abilitaClick      : true,
                abilitaExport     : true,
                nomeSheet         : this.sTipoRichiesta,
                colonne           : this.aCfgColonne,
                record            : this.aRichiesteAperte,
                customFilter      : customFilter,
              }
              
              
            });
            
            /* recupero la lista delle colonne - Fine */   
            
            
            
          });
        
          /* recupero la lista delle richieste aperte - Fine */       
        } /* MOD_GEST_RICHIESTE - Fine */
        
        
        else if ((this.sModuloChiamante) && (this.sModuloChiamante === 'MOD_NUOVA_RICHIESTA')) { /* MOD_NUOVA_RICHIESTA */
  
          this.gestScorteServices.getRmaResi(qParamsOpenRequests).subscribe(( data )=>{
            //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
            this.aRichiesteAperte = data.data.results;
            console.debug('GestioneRichiesteComponent => getRmaResi: ',this.aRichiesteAperte);
            
            
            
            /* recupero la lista delle colonne - Inizio */
      
            const qParamsGridColumns = {
              RICHIEDENTE     : this.richiedente,
              CHIAMANTE       : this.sModuloChiamante,
              MODULO          : this.sModuloChiamante,
              TIPO_RICHIESTA  : this.sTipoRichiesta
            };
      
            this.gestScorteServices.getGridColumns(qParamsGridColumns).subscribe(( data )=>{
              //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
              this.aCfgColonne = data.data.results;
              console.debug('GestioneRichiesteComponent => cfg colonne: ',this.aCfgColonne);
       
              let customFilter = [];
       
              for (let col in this.aCfgColonne) {
                 
                this.infoElement[this.aCfgColonne[col].NOME_COLONNA] = {  ETICHETTA : this.aCfgColonne[col].LABEL,
                                                                          TIPO      : this.aCfgColonne[col].TIPO,
                                                                          POSIZIONE : this.aCfgColonne[col].POSIZIONE,
                
                                                                        };
                /* COSTRUISCO IL FILTRI CUSTOM per la grid */                                                        
                if (this.aCfgColonne[col].NOME_COLONNA === 'STATO') {
                  customFilter[this.aCfgColonne[col].NOME_COLONNA+'-filter'] = {
                    TIPO    : this.aCfgColonne[col].TIPO,
                    INPUT   : 'SELECT',
                    VALORI  : [] /*Object.keys(this.azioniPossibiliCfg[this.sTipoRichiesta.replace(/ /g, '_')]).map(x => x.replace(/_/g,' '))*/
                    };
                }                
                else {
                }
                
                
                this.aMagazziniOperatore
              }
              
              /* cfg della griglia */
              this.gridConfig = {
                nColonneInput     : null,
                abilitaDettaglio  : true,
                abilitaSelectAll  : false,
                abilitaClick      : true,
                abilitaExport     : true,
                nomeSheet         : this.sTipoRichiesta,
                colonne           : this.aCfgColonne,
                record            : this.aRichiesteAperte,
                customFilter      : customFilter,
              }
              
              this.loaderSearch = false;
            });
            
            /* recupero la lista delle colonne - Fine */   
            
            
            
          });
        
          /* recupero la lista delle richieste aperte - Fine */       
        } /* MOD_NUOVA_RICHIESTA - Fine */
        
        
        else if ((this.sModuloChiamante) && (this.sModuloChiamante === 'MOD_APPROVAZIONE_HQ')) { /* MOD_APPROVAZIONE_HQ */
  
          this.gestScorteServices.getRmaAppHQ(qParamsOpenRequests).subscribe(( data )=>{
            //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
            this.aRichiesteAperte = data.data.results;
            console.debug('GestioneRichiesteComponent => getRmaResi: ',this.aRichiesteAperte);
            
            
            
            /* recupero la lista delle colonne - Inizio */
      
            const qParamsGridColumns = {
              RICHIEDENTE     : this.richiedente,
              CHIAMANTE       : this.sModuloChiamante,
              MODULO          : this.sModuloChiamante,
              TIPO_RICHIESTA  : this.sTipoRichiesta
            };
      
            this.gestScorteServices.getGridColumns(qParamsGridColumns).subscribe(( data )=>{
              //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
              this.aCfgColonne = data.data.results;
              console.debug('GestioneRichiesteComponent => cfg colonne: ',this.aCfgColonne);
       
              let customFilter = [];
       
              for (let col in this.aCfgColonne) {
                 
                this.infoElement[this.aCfgColonne[col].NOME_COLONNA] = {  ETICHETTA : this.aCfgColonne[col].LABEL,
                                                                          TIPO      : this.aCfgColonne[col].TIPO,
                                                                          POSIZIONE : this.aCfgColonne[col].POSIZIONE,
                
                                                                        };
                /* COSTRUISCO IL FILTRI CUSTOM per la grid */                                                        
                if (this.aCfgColonne[col].NOME_COLONNA === 'STATO') {
                  customFilter[this.aCfgColonne[col].NOME_COLONNA+'-filter'] = {
                    TIPO    : this.aCfgColonne[col].TIPO,
                    INPUT   : 'SELECT',
                    VALORI  : [] /*Object.keys(this.azioniPossibiliCfg[this.sTipoRichiesta.replace(/ /g, '_')]).map(x => x.replace(/_/g,' '))*/
                    };
                }                
                else {
                }
                
                
                this.aMagazziniOperatore
              }
              
              /* cfg della griglia */
              this.gridConfig = {
                nColonneInput     : null,
                abilitaDettaglio  : true,
                abilitaSelectAll  : false,
                abilitaClick      : true,
                abilitaExport     : true,
                nomeSheet         : this.sTipoRichiesta,
                colonne           : this.aCfgColonne,
                record            : this.aRichiesteAperte,
                customFilter      : customFilter,
              }
              
              this.loaderSearch = false;
            });
            
            /* recupero la lista delle colonne - Fine */   
            
            
            
          });
        
          /* recupero la lista delle richieste aperte - Fine */       
        } /* MOD_APPROVAZIONE_HQ - Fine */
        
        else {
          console.debug('GestioneRichiesteComponent => Modulo Non Configurato : ',this.sModuloChiamante);
          this.loaderSearch = false;
        }
        
      });
      
      /* recupero i magazzini dell'operatore - Fine */
      

      

    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.livelloPassato   = this.buildLivello(this.activatedroute.root);
        this.livelloAttuale   = this.activatedroute.snapshot.data.livello;
        this.titoloPagina     = this.activatedroute.snapshot.data.titolo;
        this.sTipoRichiesta   = this.activatedroute.snapshot.data.sTipoRichiesta;
        this.sModuloChiamante = this.activatedroute.snapshot.data.sModuloChiamante;

    })
    
    
    console.debug('sono GestioneRichiesteComponent 1: ',this.navTileMenuCfg);
    console.debug('sono GestioneRichiesteComponent 1: ',this.livelloPassato);
    
  }

  ngOnChanges(): void {
    
    console.debug('sono GestioneRichiesteComponent 2: ',this.navTileMenuCfg);
    console.debug('sono GestioneRichiesteComponent 2: ',this.livelloPassato);
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
  }
  
  buildLivello(route: ActivatedRoute): string {
      console.debug('buildBreadCrumb: ',route);
      let livello = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      if (route.firstChild) {

          return this.buildLivello(route.firstChild);
      }
      return livello;
  }
  
  
  
  public getRecordSelezionato(rowSelected: any):void {
     //setTimeout(()=>{
      this.visualizzaSelectRow = true;
      this.loaderOnSelectRow = true;
      this.rowSelected = null;
      this.resetAction();
      console.debug('sono GestioneRichiesteComponent => rowSelected: ', rowSelected);
      this.rowSelected = rowSelected;
      
      /* invoco lo a nuova rotta che legge le azioni configurate a DB nella GUI_WIND3_CONFIG_AZIONI indipendententemente dal modulo */
      
      const qParamsGetAzioni = {
            RICHIEDENTE     : this.richiedente,
            ID_ATTIVITA     : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
            MODULO          : this.sModuloChiamante,
            TIPO_RICHIESTA  : this.sTipoRichiesta
          };
          
      console.debug('sono GestioneRichiesteComponent => qParamsGetAzioni: ', qParamsGetAzioni);
      this.gestScorteServices.getAzioni(qParamsGetAzioni).subscribe(( data )=>{
          console.debug('sono GestioneRichiesteComponent =>  getAzioni: ', data.data.results );
          this.listaAzioniPossibili = [];
          if ((data.data.results) && (data.data.results.length > 0)) {
            
          
          
            for (let cAzione in data.data.results) {
              
              console.debug('sono GestioneRichiesteComponent =>  getAzioni => azioneTrovata: ', data.data.results[cAzione] );
              
              let azioneTrovata   = data.data.results[cAzione];
              let azioneBuffObj   = {
                  nome                : null,
                  etichetta           : null,
                  icona               : null,
                  visible             : null,
                  presidioMagazWind   : null,     
                  orario              : null,     
                  descrizioneGuasto   : null,     
                  motivazioneRifiuto  : null,     
                  causaleSospensione  : null,
                  compatibiliEquiv    : null,
                  multiStep           : null,       
                };
              
              let isStatoCustom           = azioneTrovata.IS_STATO_CUSTOM             === 1   ||  azioneTrovata.IS_STATO_CUSTOM             === '1' ? true:false;
              let isAzioneCustom          = azioneTrovata.IS_AZIONE_CUSTOM            === 1   ||  azioneTrovata.IS_AZIONE_CUSTOM            === '1' ? true:false;
              let isMultiStep             = azioneTrovata.IS_MULTI_STEP               === 1   ||  azioneTrovata.IS_MULTI_STEP               === '1' ? true:false;
              let checkRipin              = azioneTrovata.CHECK_RIPIN                 === 1   ||  azioneTrovata.CHECK_RIPIN                 === '1' ? true:false;
              let getPresidioMagazzino    = azioneTrovata.GET_PRESIDIO_MAGAZZINO      === 1   ||  azioneTrovata.GET_PRESIDIO_MAGAZZINO      === '1' ? true:false;
              let getDescGuasto           = azioneTrovata.GET_DESC_GUASTO             === 1   ||  azioneTrovata.GET_DESC_GUASTO             === '1' ? true:false;
              let getMotivRifiuto         = azioneTrovata.GET_MOTIVAZIONE_RIFIUTO     === 1   ||  azioneTrovata.GET_MOTIVAZIONE_RIFIUTO     === '1' ? true:false;
              let getCausaliSosp          = azioneTrovata.GET_CAUSALI_SOSPENSIONE     === 1   ||  azioneTrovata.GET_CAUSALI_SOSPENSIONE     === '1' ? true:false;
              let isCompatibiliEquiv      = azioneTrovata.IS_COMPATIBILI_EQUIVALENTI  === 1   ||  azioneTrovata.IS_COMPATIBILI_EQUIVALENTI  === '1' ? true:false;

              /* per i moduli dove non esiste il filtro STATO, devo ingettarlo dalla Azione */
              if (!this.rowSelected.STATO) {
                console.debug('sono GestioneRichiesteComponent =>  getAzioni => il record non contiene lo STATO, si prende quello della AZIONE ');
                this.rowSelected.STATO = azioneTrovata.STATO;
              }
              
              
  
              /* se IS_STATO_CUSTOM = true , allora devo prendere le relative azioni in base al NOME_STATO_CUSTOM */
              /* se IS_STATO_CUSTOM = false , allora devo prendere le relative azioni in base allo STATO */
              
              if  (            
                  ((isStatoCustom) && (this.rowSelected.STATO.replace(/ /g, '_') === azioneTrovata.NOME_STATO_CUSTOM.replace(/ /g, '_'))) ||
                  ((!isStatoCustom) && (this.rowSelected.STATO.replace(/ /g, '_') === azioneTrovata.STATO.replace(/ /g, '_')))
                ) {
                
                console.debug('sono GestioneRichiesteComponent =>  getAzioni => azioneTrovata => CONSISTENTE');
                
                if (isMultiStep) { /* se l'azione e' IS_MULTI_STEP allora devo prendere l'ultima azione nella LISTA_ID_AZIONE_MULTIPLE*/
                  let aListaAzMulti = azioneTrovata.LISTA_ID_AZIONE_MULTIPLE.split(';');
                  let nMaxListaAzMulti = aListaAzMulti.length;
                  azioneBuffObj.nome      = aListaAzMulti[nMaxListaAzMulti-1].replace(/ /g, '_');
                } else { /* se l'azione non e' IS_MULTI_STEP */
                  if (isAzioneCustom) { /* se e' una azione IS_AZIONE_CUSTOM - leggo dal NOME_AZIONE_CUSTOM */
                    azioneBuffObj.nome      = azioneTrovata.NOME_AZIONE_CUSTOM.replace(/ /g, '_');
                  } else { /* e' STANDARD */
                    azioneBuffObj.nome      = azioneTrovata.ACTION.replace(/ /g, '_');
                  }
                  
                }
                
                azioneBuffObj.etichetta = azioneTrovata.ETICHETTA_AZIONE;
                azioneBuffObj.icona     = azioneTrovata.ICONA_AZIONE;
                azioneBuffObj.visible   = azioneTrovata.VISIBILE === 1  ||  azioneTrovata.VISIBILE  === '1' ? true:false;
                azioneBuffObj.multiStep = isMultiStep;
   
                if ((isAzioneCustom) && (checkRipin)) { /* per questo caso custom devo invocare la logica dedicata */
                  
                  console.debug('sono GestioneRichiesteComponent =>  getAzioni => isAzioneCustom => checkRipin ');
                  
                  azioneBuffObj.visible = false;
                  
                  const qParamsCheckRipin = {
                    RICHIEDENTE     : this.richiedente,
                    ID_ATTIVITA_RMA : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => isAzioneCustom => checkRipin controllo per abilitare/disabilitare APRI_RIPIN',qParamsCheckRipin);
            
                  this.gestScorteServices.getCheckRipin(qParamsCheckRipin).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent =>  getCheckRipin: ', data );
                    
                    if (parseInt(data.data.APRI_RIPIN) === 1) {
                      azioneBuffObj.visible = true;
                      console.debug('sono GestioneRichiesteComponent =>  APRI_RIPIN = SI ');
  
                    } else {
                      azioneBuffObj.visible = false;
                      console.debug('sono GestioneRichiesteComponent =>  APRI_RIPIN = NO ');
                    }
   
                  });                
                }
                
                if (getPresidioMagazzino) { /* gestisco il presidio del magazzino */
                  
                  console.debug('sono GestioneRichiesteComponent =>  getAzioni => getPresidioMagazzino');
                  
                  const qParamsPresidioMagWind = {
                    RICHIEDENTE     : this.richiedente,
                    SEDE_MAG_WIND   : this.rowSelected.MAGAZZINO_DESTINAZIONE,
                    ID_ATTIVITA     : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getPresidioMagazzino => qParamsPresidioMagWind',qParamsPresidioMagWind);
                  
                  this.gestScorteServices.getPresidioMagWind(qParamsPresidioMagWind).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent => getAzioni => getPresidioMagazzino => getPresidioMagWind: ', data.data.results );
                    azioneBuffObj.presidioMagazWind = data.data.results[0];
                    azioneBuffObj.orario = {min: '08:00:00', max: '19:59:00'}; 
                  
                    
                  });              
                  
                } else {
                    azioneBuffObj.presidioMagazWind = null; 
                    azioneBuffObj.orario = null;
                } 
                
                if  (getDescGuasto) { /* gestisco la descrizione guasto */
  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getDescGuasto');
                  
                  const qParamsGuastoGestRich = {
                    RICHIEDENTE     : this.richiedente,
                    ID_ATTIVITA     : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getDescGuasto => qParamsGuastoGestRich',qParamsGuastoGestRich);
                  
                  this.gestScorteServices.getGuastoGestRich(qParamsGuastoGestRich).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent =>  getAzioni => getDescGuasto => getGuastoGestRich: ', data.data.results );
                    azioneBuffObj.descrizioneGuasto = data.data.results;      
  
                  });                
  
                } else {
                  azioneBuffObj.descrizioneGuasto = null;
                }
                
                
                
                if (getMotivRifiuto) { /* gestisco la motivazione rifiuto */              
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getMotivRifiuto');
                  
                  const qParamsRifiutoGestRich = {
                    RICHIEDENTE     : this.richiedente,
                    ID_ATTIVITA     : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getDescGuasto => qParamsRifiutoGestRich',qParamsRifiutoGestRich);
                  
                  this.gestScorteServices.getRifiutoGestRich(qParamsRifiutoGestRich).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent => getAzioni => getDescGuasto =>  getRifiutoGestRich: ', data.data.results );
                    azioneBuffObj.motivazioneRifiuto = data.data.results; /* ingetto */
                  
                    
                  });                
                  
                } else {
                  azioneBuffObj.motivazioneRifiuto = null;
                }
                
                if (getCausaliSosp) { /* gestisco CAUSALE SOSPENSIONE */
                  
                  
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getCausaliSosp');
                  
                  const qParamsCausaliSospReso = {
                    RICHIEDENTE     : this.richiedente,
                    ID_ATTIVITA     : this.rowSelected.ID ? this.rowSelected.ID : this.rowSelected.ID_ATTIVITA ? this.rowSelected.ID_ATTIVITA : this.rowSelected.ID_ATTIVITA_RMA,
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => getCausaliSosp => qParamsCausaliSospReso',qParamsCausaliSospReso);
                  
                  this.gestScorteServices.getCausaliSospReso(qParamsCausaliSospReso).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent => getAzioni => getCausaliSosp =>  getCausaliSospReso: ', data.data.results );
                    azioneBuffObj.causaleSospensione = data.data.results; /* ingetto */
                
                    
                  });                
                  
                  
                } else {
                  azioneBuffObj.causaleSospensione = null;
                }
                
                
                
                
                if (isCompatibiliEquiv) { /* gestisco i compatibili e equivalenti */               
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => isCompatibiliEquiv');
                  
                  const qParamsMatCompatibili = {
                    RICHIEDENTE     : this.richiedente,
                    PART_NUMBER     : this.rowSelected.PART_NUMBER,
                    SAP_CODE        : this.rowSelected.SAP_CODE
                  };
                  
                  console.debug('sono GestioneRichiesteComponent => getAzioni => isCompatibiliEquiv => qParamsMatCompatibili',qParamsMatCompatibili);
                  
                  this.gestScorteServices.getMatCompatibili(qParamsMatCompatibili).subscribe(( data )=>{
                    
                    console.debug('sono GestioneRichiesteComponent => getAzioni => getCausaliSosp =>  getMatCompatibili: ', data.data.results );
                    azioneBuffObj.compatibiliEquiv = data.data.results; /* ingetto */
                
                    
                  });                   
                  
                } else {
                  azioneBuffObj.compatibiliEquiv = null;
                }
                
                
                
              } else {
                azioneBuffObj = null;
                console.debug('sono GestioneRichiesteComponent =>  getAzioni => azioneTrovata => NON CONSISTENTE - VEDERE LA CFG');
              }
              
              
              if (azioneBuffObj) {
                console.debug('sono GestioneRichiesteComponent => getAzioni => azioneBuffObj: ', azioneBuffObj); 
                this.listaAzioniPossibili.push(azioneBuffObj);
              }
              
  
              
              
               
                
            } /* fine del ciclo azioni */
          } else {
              this.listaAzioniPossibili.push( {
                  nome                : 'MSG_CUSTOM',
                  etichetta           : 'Attivita\' in carico a Sirti - Nessuna azione disponibile',
                  icona               : '',
                  visible             : true,
                  presidioMagazWind   : null,     
                  orario              : null,     
                  descrizioneGuasto   : null,     
                  motivazioneRifiuto  : null,     
                  causaleSospensione  : null,
                  compatibiliEquiv    : null,
                  multiStep           : null,       
                });
          }
          
          console.debug('sono GestioneRichiesteComponent => getAzioni => listaAzioniPossibili: ', this.listaAzioniPossibili);
          this.loaderOnSelectRow  = false;

      });
      


      //}, 100);
      /*
      setTimeout(()=>{
        let focusInput = document.getElementById('btn-action-'+this.listaAzioniPossibili[0].nome) as HTMLInputElement;
        console.debug('sono GestioneRichiesteComponent => focusInput: ', focusInput);
        focusInput.focus();
        
      }, 100);
      */
 
  }
  
  
  public resetAction():void {
    this.azioneSelezionataObj   = {};
    this.apriAzioneSelezionata  = false;
  };
  
 
  public closeRecordSelezionato():void {
    console.debug('sono GestioneRichiesteComponent => closeRecordSelezionato: ', this.rowSelected);
    this.rowSelected = null;
    this.visualizzaSelectRow = false;
    this.resetAction();
    if (this.isAzioneEseguita) {
      this.isAzioneEseguita = false;
      this.ngOnInit();
    }
  }
  
  
  public clickOpenAzione(itemAzione:any):void {
    this.azioneSelezionataObj = {};
    this.isAzioneEseguita = false;
    console.debug('sono GestioneRichiesteComponent => clickOpenAzione: ', itemAzione);
    
    this.azioneSelezionataObj = {
      nome                : itemAzione.nome     ,
      etichetta           : itemAzione.etichetta,
      icona               : itemAzione.icona    ,
      tipo                : this.sTipoRichiesta.replace(/ /g, '_'),
      stato               : this.rowSelected.STATO.replace(/ /g, '_'),
      materiale           : this.rowSelected,
      presidioMagazWind   : itemAzione.presidioMagazWind  ? itemAzione.presidioMagazWind : null,
      orario              : itemAzione.orario             ? itemAzione.orario : null,
      descrizioneGuasto   : itemAzione.descrizioneGuasto  ? itemAzione.descrizioneGuasto : null,
      motivazioneRifiuto  : itemAzione.motivazioneRifiuto ? itemAzione.motivazioneRifiuto : null,
      causaleSospensione  : itemAzione.causaleSospensione ? itemAzione.causaleSospensione : null,
      compatibiliEquiv    : itemAzione.compatibiliEquiv   ? itemAzione.compatibiliEquiv : null,
      multiStep           : itemAzione.multiStep          ? itemAzione.multiStep : false,
      richiedente         : this.richiedente
    };
    
    this.apriAzioneSelezionata = true;

  }
  
  public clickCloseAzione():void {
    console.debug('sono GestioneRichiesteComponent => clickCloseAzione: ');
    this.resetAction();
    if (this.isAzioneEseguita) {
      this.isAzioneEseguita = false;
      this.ngOnInit();
    }

  }
  
  
  public focusOut(event: any):void {
    console.debug('sono GestioneRichiesteComponent => focusOut: ', event);
      this.visualizzaSelectRow = false;
      this.resetAction();
      //this.loaderSearch = true;
      //this.ngOnInit();
  }
  
  public reloadOut(event: any):void {
    console.debug('sono GestioneRichiesteComponent => reloadOut: ', event);
    this.resetAction();
    this.ngOnInit();
  }
  
  
  public azioneEseguita(event: any):void {
    console.debug('sono GestioneRichiesteComponent => azioneEseguita: ', event);
    this.isAzioneEseguita = event;
    

  }
  
  
  
  
  
  

}
