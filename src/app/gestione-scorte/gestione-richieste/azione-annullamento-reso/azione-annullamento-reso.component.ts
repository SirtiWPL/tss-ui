import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-azione-annullamento-reso',
  templateUrl: './azione-annullamento-reso.component.html',
  styleUrls: ['./azione-annullamento-reso.component.css']
})
export class AzioneAnnullamentoResoComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzAnnullamentoReso: FormGroup;
  
  loaderEseguiAzione:boolean;

  constructor(private formBuilderAzAnnullamentoReso: FormBuilder) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneAnnullamentoResoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzAnnullamentoReso */

    this.reactiveFormAzAnnullamentoReso = this.formBuilderAzAnnullamentoReso.group({
      sRichiedenteAnnullamentoCheck : [null,[Validators.required]],
      sCausaAnnullamentoCheck       : [null,[Validators.required]],
    });
    
    this.loaderEseguiAzione = false;
    
    
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneAnnullamentoResoComponent => eseguiAzione: ',this.reactiveFormAzAnnullamentoReso.value);
    this.outReload.emit(true);
  }

}