import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneRifiutoMaterialeAccettatoComponent } from './azione-rifiuto-materiale-accettato.component';

describe('AzioneRifiutoMaterialeAccettatoComponent', () => {
  let component: AzioneRifiutoMaterialeAccettatoComponent;
  let fixture: ComponentFixture<AzioneRifiutoMaterialeAccettatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneRifiutoMaterialeAccettatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneRifiutoMaterialeAccettatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
