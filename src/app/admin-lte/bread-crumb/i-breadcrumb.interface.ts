/* interfaccia del Breadcrumb */
export interface IBreadcrumb {
  label: string;
  url: string;
  icon: string;
  titolo: string;
}
