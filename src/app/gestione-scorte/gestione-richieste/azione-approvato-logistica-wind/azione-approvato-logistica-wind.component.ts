import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-approvato-logistica-wind',
  templateUrl: './azione-approvato-logistica-wind.component.html',
  styleUrls: ['./azione-approvato-logistica-wind.component.css']
})
export class AzioneApprovatoLogisticaWindComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzApprovatoLogisticaWind: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzApprovatoLogisticaWind: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService 
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneApprovatoLogisticaWindComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzApprovatoLogisticaWind */

    this.reactiveFormAzApprovatoLogisticaWind = this.formBuilderAzApprovatoLogisticaWind.group({
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneApprovatoLogisticaWindComponent => eseguiAzione: ',this.reactiveFormAzApprovatoLogisticaWind.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
    };
    
    this.gestScorteServices.fornituraApprovataLogisticaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneApprovatoLogisticaWindComponent => fornituraApprovataLogisticaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}
