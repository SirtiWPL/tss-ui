export class configUI {

  constructor(
    public jwtToken: string,
    public decoded:any
  ){}


  public getTokenJWT(): string {
    return this.jwtToken;
  }

  public setTokenJWT(jwtToken: string): void {
      this.jwtToken = jwtToken;
  }


}
