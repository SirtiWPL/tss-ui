import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-menu-tile',
  templateUrl: './menu-tile.component.html',
  styleUrls: ['./menu-tile.component.css']
})
export class MenuTileComponent implements OnInit {

  @Input() navTileMenuCfg: Array<any>;
  @Input() livelloPassato: string;
  @Input() livelloAttuale: string;
  @Input() statistics: any;
  
  constructor() { }

  ngOnInit(): void {
    
    console.debug('sono il MenuTileComponent: ',this.navTileMenuCfg);
    console.debug('sono il MenuTileComponent: ',this.livelloPassato);
    console.debug('sono il MenuTileComponent: ',this.livelloAttuale);
    console.debug('sono il MenuTileComponent: ',this.statistics);
  }

}
