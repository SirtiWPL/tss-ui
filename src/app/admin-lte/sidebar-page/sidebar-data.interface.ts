export interface iSidebarDataModule {
  cssStyleColorSideBar  : string;
  cssStyleLogoSideBar   : string;
  logoImageCliente      : string;
  logoImageSirti        : string; 
  nomeOperatore         : string;
  cognomeOperatore      : string;
  nomeApp               : string;
  mostraLogoCustom      : boolean;
}
