import {AfterViewInit, Component, ViewChild,ChangeDetectorRef,Input,OnInit,Output,EventEmitter } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectionModel} from '@angular/cdk/collections';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {formatDate} from '@angular/common';

/* Static data */ 

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class GridComponent implements AfterViewInit,OnInit {
  
  /* input params grid */
  @Input() gridColObj: Array<any>;
  @Input() gridRecordObj: Array<any>;
  @Input() nColonneInput: number;
  @Input() abilitaDettaglio:any;
  @Input() abilitaSelectAll:any;
  @Input() filtriCustom:any;
  @Input() abilitaClick:any;
  @Input() abilitaExport:any;
  @Input() nomeSheet:any;
  
  
  /* out event emitter grid */
  @Output() outRecordSelezionato  = new EventEmitter<any>();
  @Output() outFocus              = new EventEmitter<any>();
  @Output() outReload             = new EventEmitter<any>();
  
  
  
  displayedColumns: string[] = [];
  displayedColumnFilters: string[] = [];
  nCololonneVisibili:number;
  filtriApplicati = {};
  expandedElement:null;
  infoElement = {};
  mostraDettaglio:boolean;
  abilitaSelectMultipla:boolean;
  selection = new SelectionModel<any>(true, []);
  abilitaClickRecord:boolean;
  abilitaExportExcel:boolean;
  nomeSheetExcel:string;
  loaderExportExcel: boolean;
  abilitaVediAttivita:boolean;
  vediAttivitaCfg:any = {
    mostraVediAttivita  : false,
    cssStyle : {
      icona   : '',
      colore  : '',
    },
    idAttivita : null,
    nomeColonnaIdAttivita: null,
  };
  
  
  dataSource;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //@ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor() {

  
  
  }
  
  
  
  public onReloadGrid(event: Event): void {
      event.stopPropagation();
      console.debug('sono GridComponent => onReloadGrid: ',event);
      this.outReload.emit(true);
  }
  
  /* procedura di click che permette di gestire o selezionare il record e lo espone al chiamante */
  public onClickRow(event: Event,row: any): void {
      event.stopPropagation();
      if(!this.abilitaSelectMultipla) {
        console.debug('sono GridComponent => onClickRow: ',row);
        
        this.outRecordSelezionato.emit(row);
      } else {
        console.debug('sono GridComponent => onClickRow: ',this.selection);
        
        this.outRecordSelezionato.emit(this.selection);
      }
  }
  
  


  /* procedura di click che apre il vedi attivita */
  public onClickVediAttivita(event: Event,row: any): void {
      event.stopPropagation();
      console.debug('sono GridComponent => onClickVediAttivita: ',row);
      
    this.vediAttivitaCfg.cssStyle.icona     = 'far fa-list-alt';
    this.vediAttivitaCfg.cssStyle.colore    = 'bg-orange bg-color-palette';
    this.vediAttivitaCfg.cssStyle.card      = 'card-orange';
    this.vediAttivitaCfg.cssStyle.size      = 'xl';
    this.vediAttivitaCfg.idAttivita         = row[this.vediAttivitaCfg.nomeColonnaIdAttivita];      
    this.vediAttivitaCfg.mostraVediAttivita = true;
  
  }
  
  public vediAttivitaClose(event): void {
    console.debug('sono GridComponent => vediAttivitaClose: ',event);
    this.vediAttivitaCfg.mostraVediAttivita = false;
  }
  
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;

  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
    console.debug('sono GridComponent => isAllSelected: ',this.selection);
    this.outRecordSelezionato.emit(this.selection);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  
  /* procedura che esegue l'export excel */
  public exportExcel(event: Event) : void {
    //event.stopPropagation();
    console.debug('sono GridComponent => exportExcel');
    
    this.loaderExportExcel = true;
    
    let confHeader	= {};
    let dataValue:any	=	[];
    let nomeFile 		= '';
    let nomeSheet		= '';
    
    nomeSheet = this.nomeSheetExcel;
    
    Object.entries(this.gridColObj).forEach(entry => {
      const [key, value] = entry;
      //console.debug(key,value);
      if (value.VISIBILE === '1') {
        confHeader[key] = {name : value.NOME_COLONNA, id : value.NOME_COLONNA, label : value.NOME_COLONNA, visible : true, enabled : false, type:'string' };
      }
      
    });
    
    
    console.debug('sono GridComponent => exportExcel => confHeader: ',confHeader);
    
    dataValue = this.dataSource.filteredData;
    
    console.debug('sono GridComponent => exportExcel => dataValue: ',dataValue);
    

    let dateNow = formatDate(Date.now(),'yyyyMMddHHmmss','en-US'); 
    
    

    let ws =  XLSX.utils.json_to_sheet (dataValue);
		// costruttore obj excel
		let wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, 'export_'+nomeSheet+'_'+dateNow+'.xlsx');
    setTimeout(()=>{
      this.loaderExportExcel = false;
    }, 1000);
    
  }
  
 
  ngOnInit(): void {
    console.debug('sono GridComponent => gridColObj: ',this.gridColObj);
    console.debug('sono GridComponent => gridRecordObj: ',this.gridRecordObj);
    console.debug('sono GridComponent => nColonneInput: ',this.nColonneInput);
    
    this.nCololonneVisibili = this.nColonneInput ? this.nColonneInput : this.gridColObj.length;
    
    console.debug('GridComponent => abilitaDettaglio',this.abilitaDettaglio);
    
    this.mostraDettaglio = this.abilitaDettaglio ? true : false;
    
    console.debug('GridComponent => abilitaClick',this.abilitaClick);
    
    this.abilitaClickRecord = this.abilitaClick ? true : false;
    
    console.debug('GridComponent => abilitaClick',this.abilitaExport);
    
    this.abilitaExportExcel = this.abilitaExport ? true : false;
    
    console.debug('GridComponent => abilitaClick',this.abilitaExport);
    
    this.nomeSheetExcel = this.nomeSheet ? this.nomeSheet : ''  ;
    
    
    
    console.debug('GridComponent => abilitaSelectAll',this.abilitaSelectAll);
    
    this.abilitaSelectMultipla = this.abilitaSelectAll ? true : false;
    
    this.displayedColumns.push('option');
    if (this.abilitaSelectMultipla) {
      
      this.displayedColumnFilters.push('select-filter');
    } else {
      this.displayedColumnFilters.push('option-filter');
    }
    
    this.abilitaVediAttivita = false;
    
    console.debug('GridComponent => filtriCustom',this.filtriCustom);
    
    //console.debug('colCfg',this.gridColObj);
    let contaCol = 0;
    for (let col in this.gridColObj) {
      
      if ((this.gridColObj[col].VISIBILE === '1') && (contaCol < this.nCololonneVisibili) ){
        this.displayedColumns.push(this.gridColObj[col].NOME_COLONNA);
        this.displayedColumnFilters.push(this.gridColObj[col].NOME_COLONNA+'-filter');
        
        if (this.gridColObj[col].ABILITA_VEDI_ATTIVITA ==='1') { /* verifico se esiste almeno una colonna che abilita il vedi attivita */
          this.abilitaVediAttivita = true;          
          this.vediAttivitaCfg.mostraVediAttivita = false;
          this.vediAttivitaCfg.nomeColonnaIdAttivita = this.gridColObj[col].NOME_COLONNA;
        }

        contaCol++;
      }
      
      this.infoElement[this.gridColObj[col].NOME_COLONNA] = { ETICHETTA : this.gridColObj[col].LABEL,
                                                              TIPO      : this.gridColObj[col].TIPO,
                                                              POSIZIONE : this.gridColObj[col].POSIZIONE,
                                                            };

      
    }
    
    console.debug('GridComponent => displayedColumns',this.displayedColumns);
    console.debug('GridComponent => displayedColumnFilters',this.displayedColumnFilters);

    console.debug('GridComponent => mostraDettaglio',this.mostraDettaglio);
    
    this.dataSource = new MatTableDataSource(this.gridRecordObj);
    console.log('dataSource',this.dataSource);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = this.customFilterPredicate(this.displayedColumns,this.infoElement);
    this.loaderExportExcel = false;
    
  }

  ngAfterViewInit() {
    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }
  
  isSticky(id: string) {
    return false;
    //return id === 'ID' || id === 'TIPO' ? true:false;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  
  
  customFilterPredicate(cfgCol,gridColObj) {
    //console.debug('GridComponent => customFilterPredicate');
    const myFilterPredicate = function(data:any,filter:string) :boolean {
      
      let searchString = JSON.parse(filter);
      //console.debug('GridComponent => customFilterPredicate => searchString',searchString);
      //console.debug('GridComponent => customFilterPredicate => cfgCol',cfgCol);
      //console.debug('GridComponent => customFilterPredicate => gridColObj',gridColObj);
      //console.debug('GridComponent => customFilterPredicate => displayedColumnFilters',this.displayedColumnFilters);
      let valueFound;
      const cfgCol2 = Object.keys(searchString);
      let contaOk = 0;
      for (let col in cfgCol2) {
        const indexCol = cfgCol2[col];
        //console.debug('GridComponent => customFilterPredicate => indexCol',indexCol);
        //console.debug('GridComponent => customFilterPredicate => big',searchString[indexCol].toLowerCase());
        if (searchString[indexCol]) {
          
          if (gridColObj[indexCol].TIPO !== 'DATE') {
            valueFound = data[indexCol].toString().trim().toLowerCase().indexOf(searchString[indexCol].toLowerCase()) !== -1;
          } else {
            //change this to the format in which date is displayed
            
            let sDataSelected = data[indexCol].toString();
            
            let dateParts = sDataSelected.substring(0, 10).split("/");
            let oreMinParts = sDataSelected.substring(11, 19).split(":");
      
            const dateFiltroGrid = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));            

            let m = (dateFiltroGrid.getDate() < 10 ? '0'+dateFiltroGrid.getDate() : dateFiltroGrid.getDate())+'-'+((dateFiltroGrid.getMonth() + 1) < 10 ? '0'+(dateFiltroGrid.getMonth() + 1): (dateFiltroGrid.getMonth() + 1))+'-'+dateFiltroGrid.getFullYear();
            //console.debug('GridComponent => customFilterPredicate =>  m',m,searchString[indexCol].toLowerCase().replace(/\//g,'-'));
            valueFound =  m.trim().toLowerCase().indexOf(searchString[indexCol].toLowerCase().replace(/\//g,'-')) !== -1

          }
          
          if (valueFound) {
            //return valueFound;
            contaOk = contaOk + 1;
          }
          
        }
  
        
      }
      
      if (contaOk === Object.keys(searchString).length) {
        return true;
      } else {
        return false
      }
      
      
      

    }
    return myFilterPredicate;
  }
  
  filterchangedSelect(event:any,filterId:string) {
    console.debug('GridComponent => filterchangedSelect',event.value);
    this.filterchanged(event.value,filterId);
  }
  
  filterchanged(filterValue: string,filterId:string) {
    
    this.outFocus.emit(true);
    
    console.debug('GridComponent => filterchanged',filterValue,filterId);
      const indexId = filterId.split('-')[0];
      
      
      this.filtriApplicati[indexId] = filterValue ? filterValue.trim().toLowerCase() : '',
      
      

      
      console.debug('GridComponent => filterchanged => filtriApplicati',this.filtriApplicati);
      
      let notEmpty = {};
      for (let colFa in this.filtriApplicati) {
        console.debug('GridComponent => filterchanged => ciclo',this.filtriApplicati[colFa]);
        if (this.filtriApplicati[colFa] === '') {
        } else {
          notEmpty[colFa] = this.filtriApplicati[colFa];
        }
      }
      console.debug('GridComponent => filterchanged => notEmpty',notEmpty);
      this.filtriApplicati = notEmpty;
      
      if (Object.keys(this.filtriApplicati).length === 0) {
        console.debug('GridComponent => filterchanged => filtriApplicati tutti vuoti => resetto');
        this.dataSource.filter = filterValue;
      } else {
        this.dataSource.filter = JSON.stringify(this.filtriApplicati);
      }
      
      

      
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
  }  
  
}
