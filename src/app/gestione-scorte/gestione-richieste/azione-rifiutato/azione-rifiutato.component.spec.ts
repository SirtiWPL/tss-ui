import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneRifiutatoComponent } from './azione-rifiutato.component';

describe('AzioneRifiutatoComponent', () => {
  let component: AzioneRifiutatoComponent;
  let fixture: ComponentFixture<AzioneRifiutatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneRifiutatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneRifiutatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
