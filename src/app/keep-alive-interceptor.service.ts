import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { switchMap, take, filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { throwError, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { UiService } from './ui-service.service';

@Injectable()
export class KeepAliveInterceptorService implements HttpInterceptor {

  private refreshTokenSubject = new BehaviorSubject(null);
  private refreshTokenObservable = this.refreshTokenSubject.asObservable();
  private isRefreshingToken = false;

  constructor(private uiService: UiService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('KeepAliveInterceptorService');
    return next.handle(request);

  }

}
