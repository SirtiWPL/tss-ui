import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { MenuCfgModule } from '../../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from '../../ui-service.service';
import { ReportAnalisiService } from './../../report-analisi.service';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})

export class ReportComponent implements OnInit {
  
  navTileMenuCfgObj;
  navTileMenuCfg;
  
  livelloAttuale;
  titoloPagina;
  
  livelloPassato:any;
  public richiedente:string       = 'INTERNO';
  public aListaReport:any         = [];
  public aListaSezioniReport:any  = [];
  public reportSelezionato:any    = {};
  public loaderSearch:boolean;
  public gridConfig:any           = {};
  public aCfgColonne;
  public infoElement              = {};
  public aDatiVistaReport;
  
  constructor(private activatedroute:ActivatedRoute,
              private router: Router,
              private uiService: UiService,
              private reportAnalisiServices: ReportAnalisiService
              ) {
    console.log('funziona ReportComponent');
    this.navTileMenuCfgObj = new MenuCfgModule(this.uiService);
    this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
    this.livelloPassato = this.buildLivello(this.activatedroute.root);
     this.livelloAttuale = this.activatedroute.snapshot.data.livello;
     this.titoloPagina = this.activatedroute.snapshot.data.titolo;

    }

  ngOnInit(): void {
    
     /* this.activatedroute.data.subscribe(data => {
          this.livelloPassato=data.livello;
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
      });*/
      
     console.debug('qui:', this.activatedroute.snapshot.data.livello); 
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.livelloPassato = this.buildLivello(this.activatedroute.root);
        this.livelloAttuale = this.activatedroute.snapshot.data.livello;
        this.titoloPagina = this.activatedroute.snapshot.data.titolo;
    })
    
    
    this.getListaReport();
    
    console.debug('sono ReportComponent 1: ',this.navTileMenuCfg);
    console.debug('sono ReportComponent 1: ',this.livelloPassato);
    
  }
  
  
  /* lista dei report */
  getListaReport():void {
    
    this.aListaReport               = [];
    this.aListaSezioniReport        = [];
    this.reportSelezionato          = null;
    this.loaderSearch               = true;
    
    const qParamsListaReport= {
      RICHIEDENTE     : this.richiedente
    };
    
    console.debug('sono ReportComponent => getListaReport => qParamsListaReport',qParamsListaReport);
    
    this.reportAnalisiServices.getListaReport(qParamsListaReport).subscribe(( data )=>{
      
      console.debug('sono ReportComponent => getListaReport => result ', data.data.results );
      
      this.aListaSezioniReport  = [... new Set(data.data.results.map(data => data.SEZIONE_REPORT))];
      
      console.debug('sono ReportComponent => getListaReport => uniqueArrSezReport ', this.aListaSezioniReport );
     
      for (let cSezioneRep in this.aListaSezioniReport) {

        let aSubReport = data.data.results.filter(item => {
          return item.SEZIONE_REPORT === this.aListaSezioniReport[cSezioneRep];
        });
        
        for (let cReport in aSubReport) {
          
          
          this.aListaReport.push({
            posizione   : cReport,
            nome_report : aSubReport[cReport].NOME_REPORT,
            nome_gui    : aSubReport[cReport].NOME_GUI ? aSubReport[cReport].NOME_GUI : 'REPORT',
            sezione     : aSubReport[cReport].SEZIONE_REPORT,
            colorTile   : aSubReport[cReport].COLORE_TILE,
            titolo      : aSubReport[cReport].ETICHETTA_TILE,
            descrizione : aSubReport[cReport].DESC_REPORT,
            icon        : aSubReport[cReport].ICONA_TILE,
            visible     : true,
            disabled    : false
          });        
          
          
        }
        
      }
    
      this.loaderSearch               = false;
    });     
    
  
  }
  
  apriReport(reportSelezionatoObj:any): void {
    
    this.reportSelezionato  = null;
    this.loaderSearch       = true;    
    this.reportSelezionato  = reportSelezionatoObj;
    this.aCfgColonne        = [];
    this.gridConfig         = {};
    this.infoElement        = {};
    this.aDatiVistaReport   = [];
    
    console.debug('sono ReportComponent => apriReport => reportSelezionato ',this.reportSelezionato);
    
    const qParamsGetNomeVista= {
      RICHIEDENTE     : this.richiedente,
      NOME_GUI        : this.reportSelezionato.nome_gui,
      NOME_REPORT     : this.reportSelezionato.nome_report
    };
    
    console.debug('sono ReportComponent => apriReport => qParamsGetNomeVista',qParamsGetNomeVista);
    
    this.reportAnalisiServices.getNomeVistaReport(qParamsGetNomeVista).subscribe(( data )=>{
      
      console.debug('sono ReportComponent => apriReport => getNomeVistaReport => result ', data.data );
      
      
      /* recupero la lista delle colonne - Inizio */
      
      const qParamsGridColumns = {
        RICHIEDENTE     : this.richiedente,
        CHIAMANTE       : this.reportSelezionato.nome_gui,
        MODULO          : this.reportSelezionato.nome_gui,
        TIPO_RICHIESTA  : this.reportSelezionato.nome_report
      };
      
      let nomeVista = data.data.NOME_VISTA;
      
      this.reportAnalisiServices.getReportGridColumns(qParamsGridColumns).subscribe(( data )=>{
        //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
        this.aCfgColonne = data.data.results;
      
        console.debug('sono ReportComponent => apriReport => cfg colonne: ', this.aCfgColonne);
      
        let customFilter = [];
        
        let colonnaOrderBy = '';
      
        for (let col in this.aCfgColonne) {
          
          this.infoElement[this.aCfgColonne[col].NOME_COLONNA] = {  ETICHETTA : this.aCfgColonne[col].LABEL,
                                                                    TIPO      : this.aCfgColonne[col].TIPO,
                                                                    POSIZIONE : this.aCfgColonne[col].POSIZIONE,
          
                                                                  };
                                                                  
          if (parseInt(this.aCfgColonne[col].POSIZIONE) == 1 ) {
            colonnaOrderBy = this.aCfgColonne[col].NOME_COLONNA;
          }
      
        }
        
        
        const qParamsDatiVistaReport= {
          RICHIEDENTE     : this.richiedente,
          NOME_VISTA      : nomeVista,
          NOME_COLONNA    : colonnaOrderBy
        };
        
        console.debug('sono ReportComponent => apriReport => qParamsDatiVistaReport',qParamsDatiVistaReport);
        
        this.reportAnalisiServices.getDatiVistaReport(qParamsDatiVistaReport).subscribe(( data )=>{
          
          console.debug('sono ReportComponent => apriReport => getDatiVistaReport => result ', data.data.results );
          
          this.aDatiVistaReport = data.data.results;
          
          /* cfg della griglia */
          this.gridConfig = {
            nColonneInput     : null,
            abilitaDettaglio  : true,
            abilitaSelectAll  : false,
            abilitaClick      : false,
            abilitaExport     : true,
            nomeSheet         : this.reportSelezionato.nome_report,
            colonne           : this.aCfgColonne,
            record            : this.aDatiVistaReport,
            customFilter      : customFilter,
          }
          
          this.loaderSearch = false;    
          
        });    
        

      });      
      
      
      

      
      

    }); 
    
    

    

  
  }
  
  public getRecordSelezionato(rowSelected: any):void {
    console.debug('sono ReportComponent => getRecordSelezionato ', rowSelected );
  }
  
  
  public focusOut(event: any):void {
    console.debug('sono ReportComponent => focusOut ', event );

    //this.apriReport(this.reportSelezionato);
  }
  
  public reloadOut(event: any):void {

    
    console.debug('sono ReportComponent => reloadOut ', event );

    this.apriReport(this.reportSelezionato);
  }

  ngOnChanges(): void {
    
    console.debug('sono ReportComponent 2: ',this.navTileMenuCfg);
    console.debug('sono ReportComponent 2: ',this.livelloPassato);
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
  }
  
  buildLivello(route: ActivatedRoute): string {
      console.debug('buildBreadCrumb: ',route);
      let livello = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      if (route.firstChild) {

          return this.buildLivello(route.firstChild);
      }
      return livello;
  } 

}

