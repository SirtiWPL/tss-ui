import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
import { User } from 'src/app/user.model';
import { map } from 'rxjs/operators';

const apiURL = environment.apiUrlWindServices;

@Injectable({
  providedIn: 'root'
})

export class ReportAnalisiService {

   // url per servizio utility
   private UrlConfig = apiURL;

   constructor(
     private http: HttpClient
   ) {

   }
   
  getListaReport( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/lista_report';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  
  getDatiVistaReport( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/get_dati_vista_report';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }
  
  
  getNomeVistaReport( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/geocall/get_nome_vista_report';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
  }
  
  getReportGridColumns( qParams:any ): Observable<any> {
    const Url = this.UrlConfig + '/pyramide/customers/WIND/contracts/MV_SPMS/grid_columns';
    return this.http.get<any>(Url,{params : new HttpParams({ fromObject: qParams })});
  }

   
   
}
