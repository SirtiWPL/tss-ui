// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrlAdvlogHomePage      : 'http://dvmas003.sirtisistemi.net:10102',
  apiUrlAdvlogProfile       : 'http://dvmas003.ict.sirti.net:10095/advlogws/core/profile',
  apiUrlWindServices        : 'http://dvmas003.ict.sirti.net:10179/windtrews/core',
  apiUrlWindServicesRoot    : 'http://dvmas003.ict.sirti.net:10179/windtrews',
  apiUrlAdvlogWs            : 'http://dvmas003.ict.sirti.net:10095/advlogws',
  injectXWebAuthUser        : true, /* true x AUTH_PROXY, false per JWT */
  authType                  : 'AUTH_PROXY', /* JWT O AUTH_PROXY */
  xWebAuthUser              : 'ROOT',
  JWT: ''
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
