import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingDocumentazioneModule } from './app-routing-documentazione.module';
import { DocumentazioneComponent } from './documentazione.component';
import { CommonComponentModule } from '../admin-lte/common-component/common-component.module';

@NgModule({
  declarations: [DocumentazioneComponent],
  imports: [
    CommonModule,
    AppRoutingDocumentazioneModule,
    CommonComponentModule
  ],

})

export class DocumentazioneModule { }
