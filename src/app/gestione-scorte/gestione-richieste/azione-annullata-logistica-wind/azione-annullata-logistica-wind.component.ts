import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-annullata-logistica-wind',
  templateUrl: './azione-annullata-logistica-wind.component.html',
  styleUrls: ['./azione-annullata-logistica-wind.component.css']
})
export class AzioneAnnullataLogisticaWindComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzAnnullataLogisticaWind: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzAnnullataLogisticaWind: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService 
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneAnnullataLogisticaWindComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzAnnullataLogisticaWind */

    this.reactiveFormAzAnnullataLogisticaWind = this.formBuilderAzAnnullataLogisticaWind.group({
      sMotivoCheck  : [null,[Validators.required]],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneAnnullataLogisticaWindComponent => eseguiAzione: ',this.reactiveFormAzAnnullataLogisticaWind.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
      PART_NUMBER     : this.datiAzioneObj.materiale.PART_NUMBER,
      ID_SIRTI        : this.datiAzioneObj.materiale.ID_SIRTI,
      MOTIVO          : this.reactiveFormAzAnnullataLogisticaWind.value.sMotivoCheck ? this.reactiveFormAzAnnullataLogisticaWind.value.sMotivoCheck : null
    };
    
    this.gestScorteServices.fornituraAnnullataLogisticaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneAnnullataLogisticaWindComponent => fornituraAnnullataLogisticaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}