import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingLogisticaModule } from './app-routing-logistica.module';
import { LogisticaComponent } from './logistica.component';
import { CommonComponentModule } from '../admin-lte/common-component/common-component.module';
import { Page404LogisticaComponent } from './page404-logistica/page404-logistica.component';

@NgModule({
  declarations: [LogisticaComponent, Page404LogisticaComponent],
  imports: [
    CommonModule,
    AppRoutingLogisticaModule,
    CommonComponentModule
  ],

})

export class LogisticaModule { }
