import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuTileComponent } from './menu-tile/menu-tile.component';
import { PaginaProvaComponent } from './pagina-prova/pagina-prova.component';
import { AppRoutingAdminLteModule } from '../app-routing-admin-lte.module';


import { AppRoutingGestioneScorteModule } from '../../gestione-scorte/app-routing-gestione-scorte.module';
import { AppRoutingLogisticaModule } from '../../logistica/app-routing-logistica.module';
import { AppRoutingReportAnalisiModule } from '../../report-analisi/app-routing-report-analisi.module';
import { AppRoutingDocumentazioneModule } from '../../documentazione/app-routing-documentazione.module';
import { AngularMaterialModule } from '../../angular-material.module';
import { GridComponent } from '../../grid/grid.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VediAttivitaComponent } from '../../vedi-attivita/vedi-attivita.component';
import { ExternalIframeComponent } from './external-iframe/external-iframe.component';


@NgModule({
  declarations: [MenuTileComponent,PaginaProvaComponent,GridComponent,VediAttivitaComponent, ExternalIframeComponent],
  imports: [
    CommonModule,
    AppRoutingAdminLteModule,
    AppRoutingGestioneScorteModule,
    AppRoutingLogisticaModule,
    AppRoutingReportAnalisiModule,
    AppRoutingDocumentazioneModule,
    AngularMaterialModule,
    NgbModule
  ],
  exports: [MenuTileComponent,PaginaProvaComponent,GridComponent,VediAttivitaComponent],
})
export class CommonComponentModule { }
