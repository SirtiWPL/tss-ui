import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-annullamento-sospensione',
  templateUrl: './azione-annullamento-sospensione.component.html',
  styleUrls: ['./azione-annullamento-sospensione.component.css']
})
export class AzioneAnnullamentoSospensioneComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzAnnullamentoSospensione: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzAnnullamentoSospensione: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneAnnullamentoSospensioneComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzAnnullamentoSospensione */

    this.reactiveFormAzAnnullamentoSospensione = this.formBuilderAzAnnullamentoSospensione.group({
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneAnnullamentoSospensioneComponent => eseguiAzione: ',this.reactiveFormAzAnnullamentoSospensione.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente,
      ID_ATT_RESO             : this.datiAzioneObj.materiale.ID,
    };
    
    this.gestScorteServices.annullaSospResoWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneAnnullamentoSospensioneComponent => annullaSospResoWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}
