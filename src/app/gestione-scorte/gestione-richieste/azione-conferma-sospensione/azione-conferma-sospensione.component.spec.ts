import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneConfermaSospensioneComponent } from './azione-conferma-sospensione.component';

describe('AzioneConfermaSospensioneComponent', () => {
  let component: AzioneConfermaSospensioneComponent;
  let fixture: ComponentFixture<AzioneConfermaSospensioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneConfermaSospensioneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneConfermaSospensioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
