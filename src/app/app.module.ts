/* add modulo adminlte 3.0 */
import { AdminLteModule } from './admin-lte/admin-lte.module';

import { AppRoutingModule } from './app-routing.module';

import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { Page404Component } from './page404/page404.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { JwtInterceptorService } from './jwt-interceptor.service';
import { KeepAliveInterceptorService } from './keep-alive-interceptor.service';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ContattiSpocComponent } from './contatti-spoc/contatti-spoc.component';
import { EmergencyEscalationComponent } from './emergency-escalation/emergency-escalation.component';
import { LoginComponent } from './login/login.component';

import { ReactiveFormsModule } from '@angular/forms';
//import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CookieService } from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    ContattiSpocComponent,
    EmergencyEscalationComponent,
    LoginComponent,

  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    AdminLteModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    
  ],
  exports: [],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeepAliveInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
