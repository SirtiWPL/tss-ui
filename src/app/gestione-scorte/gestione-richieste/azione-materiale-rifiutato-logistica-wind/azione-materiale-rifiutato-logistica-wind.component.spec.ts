import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneMaterialeRifiutatoLogisticaWindComponent } from './azione-materiale-rifiutato-logistica-wind.component';

describe('AzioneMaterialeRifiutatoLogisticaWindComponent', () => {
  let component: AzioneMaterialeRifiutatoLogisticaWindComponent;
  let fixture: ComponentFixture<AzioneMaterialeRifiutatoLogisticaWindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneMaterialeRifiutatoLogisticaWindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneMaterialeRifiutatoLogisticaWindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
