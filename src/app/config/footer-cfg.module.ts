import { iFooterDataModule } from '../admin-lte/footer-page/footer-data.interface' /* classe FooterDataModule */

export class FooterCfgModule {
  
  footerCfg: iFooterDataModule = {
      annoCopyright   : '2020-2021',
      linkTitle       : 'http://www.sirti.it',
      ragioneSociale  : 'Sirti S.p.A.',
      version         : '3.0'
		};
  
};  
