import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';




import { HeaderPageComponent } from './header-page/header-page.component';
import { BodyPageComponent } from './body-page/body-page.component';
import { FooterPageComponent } from './footer-page/footer-page.component';
import { SidebarPageComponent } from './sidebar-page/sidebar-page.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BreadCrumbComponent } from './bread-crumb/bread-crumb.component';
import { AppRoutingAdminLteModule } from './app-routing-admin-lte.module';
import { AdminLteComponent } from './admin-lte.component';
import { CommonComponentModule } from './common-component/common-component.module';
import { Page404AdminLteComponent } from './page404-admin-lte/page404-admin-lte.component';


@NgModule({
  declarations: [AdminLteComponent,HeaderPageComponent, BodyPageComponent, FooterPageComponent, SidebarPageComponent, NavMenuComponent, BreadCrumbComponent, Page404AdminLteComponent],
  imports: [
    CommonModule,
    AppRoutingAdminLteModule,
    CommonComponentModule
  ],
  exports: [HeaderPageComponent, BodyPageComponent, FooterPageComponent,SidebarPageComponent,NavMenuComponent,BreadCrumbComponent]
})

export class AdminLteModule {
  
  }
