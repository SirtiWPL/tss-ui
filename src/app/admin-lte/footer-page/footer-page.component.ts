import { Component, OnInit ,Input,OnChanges} from '@angular/core';

import { iFooterDataModule } from './footer-data.interface' /* classe FooterDataModule */

@Component({
  selector: 'app-footer-page',
  templateUrl: './footer-page.component.html',
  styleUrls: ['./footer-page.component.css']
})
export class FooterPageComponent implements OnInit,OnChanges {
  
  /* istanzio l'oggetto FooterDataModule e lo rendo visibile */
  
  @Input() footerDataObj: iFooterDataModule; 
  

  constructor() {
 
    
  }
  
  /* sezione dei metodi GET/SET - Inizio */
  
  /* metodo per leggere l'anno del Copyright */
  getAnnoCopyright(): string {
    return this.footerDataObj.annoCopyright;
  }
  
  /* metodo per leggere la versione */
  getLinkTitle(): string {
    return this.footerDataObj.linkTitle;
  }
  
  /* metodo per leggere la versione */
  getRagioneSociale(): string {
    return this.footerDataObj.ragioneSociale;
  }

  /* metodo per leggere la versione */
  getVersion(): string {
    return this.footerDataObj.version;
  }
  
  /* sezione dei metodi GET/SET - Fine */
  
  
  /* uso OnInit per pre-valorizzare */
  ngOnInit(): void {
    
    this.footerDataObj.annoCopyright  = (( this.footerDataObj.annoCopyright === '') ? '20xx-20xx' : this.footerDataObj.annoCopyright );
    this.footerDataObj.linkTitle      = (( this.footerDataObj.linkTitle === '') ? 'http://www.sirti.it' : this.footerDataObj.linkTitle );
    this.footerDataObj.ragioneSociale = (( this.footerDataObj.ragioneSociale === '') ? 'Sirti S.p.A.' : this.footerDataObj.ragioneSociale );
    this.footerDataObj.version        = (( this.footerDataObj.version === '') ? '1.0' : this.footerDataObj.version );
    
  }
  
  ngOnChanges(): void {

    
  }

}
