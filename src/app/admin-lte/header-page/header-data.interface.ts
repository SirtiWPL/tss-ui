export class iHeaderDataModule {
  cssStyleColorNavBar : string;
  cssStyleTextNavBar  : string;
  nomeOperatore       : string;
  cognomeOperatore    : string;
  nascondiMenu        : boolean;
  mostraLogo          : boolean;
}
