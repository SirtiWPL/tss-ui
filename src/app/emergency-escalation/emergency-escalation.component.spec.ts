import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyEscalationComponent } from './emergency-escalation.component';

describe('EmergencyEscalationComponent', () => {
  let component: EmergencyEscalationComponent;
  let fixture: ComponentFixture<EmergencyEscalationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencyEscalationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyEscalationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
