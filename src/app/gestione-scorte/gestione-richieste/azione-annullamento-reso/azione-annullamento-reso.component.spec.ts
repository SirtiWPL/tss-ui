import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneAnnullamentoResoComponent } from './azione-annullamento-reso.component';

describe('AzioneAnnullamentoResoComponent', () => {
  let component: AzioneAnnullamentoResoComponent;
  let fixture: ComponentFixture<AzioneAnnullamentoResoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneAnnullamentoResoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneAnnullamentoResoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
