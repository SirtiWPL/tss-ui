import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-cambio-priorita',
  templateUrl: './azione-cambio-priorita.component.html',
  styleUrls: ['./azione-cambio-priorita.component.css']
})
export class AzioneCambioPrioritaComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzCambioPriorita: FormGroup;
  public aPrioritaDisponibili:any = ['1','2','3','4','5','6','7','8','9','10'];
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzCambioPriorita: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneCambioPrioritaComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzCambioPriorita */

    this.reactiveFormAzCambioPriorita = this.formBuilderAzCambioPriorita.group({
      sPrioritaCheck  : [null,[Validators.required]],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
  }
  
  changeValue(e) {
    e.stopPropagation();
    this.reactiveFormAzCambioPriorita.get("sPrioritaCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneCambioPrioritaComponent => changeValue',this.reactiveFormAzCambioPriorita.value.sPrioritaCheck);

   }
  
  public eseguiAzione():void {
    console.debug('sono AzioneCambioPrioritaComponent => eseguiAzione: ',this.reactiveFormAzCambioPriorita.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente,
      ID_ATTIVITA             : this.datiAzioneObj.materiale.ID,
      PRIORITA                : this.reactiveFormAzCambioPriorita.value.sPrioritaCheck,
    };
    
    this.gestScorteServices.cambioPrioritaRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneCambioPrioritaComponent => cambioPrioritaRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
      }

    });
    
  }

}
