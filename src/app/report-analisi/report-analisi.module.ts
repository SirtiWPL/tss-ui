import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingReportAnalisiModule } from './app-routing-report-analisi.module';
import { ReportAnalisiComponent } from './report-analisi.component';
import { CommonComponentModule } from '../admin-lte/common-component/common-component.module';
import { ReportComponent } from './report/report.component';


@NgModule({
  declarations: [ReportAnalisiComponent, ReportComponent],
  imports: [
    CommonModule,
    AppRoutingReportAnalisiModule,
    CommonComponentModule,
    
  ],

})

export class ReportAnalisiModule { }
