import { Component,OnChanges,OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap ,RouterOutlet} from '@angular/router';
import { AfterViewInit, ViewChild,AfterViewChecked } from '@angular/core';
import { MenuCfgModule } from './config/menu-cfg.module' /* cfg del menu  */
import { FooterCfgModule } from './config/footer-cfg.module' /* cfg del menu  */
import { SidebarCfgModule } from './config/sidebar-cfg.module' /* cfg del menu  */
import { HeaderCfgModule } from './config/header-cfg.module' /* cfg del menu  */
import { UiService } from './ui-service.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
const authType           = environment.authType; /* per distinguere tra JWT O AUTH_PROXY */

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnChanges,OnInit {

  private cookieValue : string;

  private authType           = authType; /* per distinguere tra JWT O AUTH_PROXY */

  pagefooterCfgObj  : FooterCfgModule;
  pageSidebarCfgObj : SidebarCfgModule;
  pageHeaderCfgObj  : HeaderCfgModule;
  mainMenuCfgObj    : MenuCfgModule;

  pageSidebarObj  : any;
  pagefooterObj   : any;
  pageHeaderObj   : any;
  mainMenuCfg     : any;
  esterno         : string;
  isAuthenticationLoader : boolean;

  constructor(private activatedRoute: ActivatedRoute, private uiService: UiService,private router: Router, private cookieService: CookieService) {


    /* istanzio il header */
    this.pageHeaderCfgObj = new HeaderCfgModule();
		this.pageHeaderObj = this.pageHeaderCfgObj.headerCfg;

    /* istanzio il sidebar */
    this.pageSidebarCfgObj  = new SidebarCfgModule();
    this.pageSidebarObj     = this.pageSidebarCfgObj.sidebarCfg;


    /* istanzio il footer */
    this.pagefooterCfgObj   = new FooterCfgModule();
		this.pagefooterObj      = this.pagefooterCfgObj.footerCfg;

    /* istanzio il menu*/
    
    if (this.authType === 'AUTH_PROXY') {
      this.esterno            = 'SI';
      this.pageHeaderObj.nascondiMenu = true;
      this.pageHeaderObj.mostraLogo   = true;
    } else {
      this.pageHeaderObj.nascondiMenu = false;
      this.pageHeaderObj.mostraLogo   = false;
      this.esterno            = '';
    }

	}


  ngOnChanges() {
    console.debug('AppComponent==>ngOnChanges');


    this.activatedRoute.queryParams.subscribe(params => {
      this.esterno = params['esterno'] ? params['esterno'].toUpperCase() : null;
      console.debug('AppComponent==> costruttore',this.esterno);
      if (this.esterno === 'SI') {
        this.pageHeaderObj.nascondiMenu = true;
        this.pageHeaderObj.mostraLogo   = true;
      } else {
        //this.pageHeaderObj.nascondiMenu = false;
        //this.pageHeaderObj.mostraLogo   = false;
      }
    });

  }

  ngOnInit() {
    console.debug('AppComponent==>ngOnInit');
    this.isAuthenticationLoader = true;
    //this.uiService.configService().subscribe(( data )=>{
      this.uiService.getUserInfo().subscribe(( data )=>{
        this.isAuthenticationLoader = false;
        /*per commit */

        console.log('funziona AppComponent');
        console.debug('mainMenuCfg => ',this.mainMenuCfg);
        console.debug('mainMenuCfg => ',data);

        
        

        this.mainMenuCfgObj     = new MenuCfgModule(this.uiService);
        this.mainMenuCfg        = this.mainMenuCfgObj.mainMenuCfg;
        
        setTimeout(()=>{
          this.activatedRoute.queryParams.subscribe(params => {
            this.esterno = params['esterno'] ? params['esterno'].toUpperCase() : null;
            console.debug('AppComponent==> costruttore',this.esterno,this.router.url);
            if (this.esterno === 'SI') {
              this.pageHeaderObj.nascondiMenu = true;
              this.pageHeaderObj.mostraLogo   = true;
            } else {
              if (this.router.url === '/gestioneScorte/nuovaRichiesta/GEOCALL') {
                this.pageHeaderObj.nascondiMenu = true;
                this.pageHeaderObj.mostraLogo   = true;
                this.esterno            = 'SI';
              } else {
                this.pageHeaderObj.nascondiMenu = false;
                this.pageHeaderObj.mostraLogo   = false;
                this.esterno            = '';
              }
              //this.pageHeaderObj.nascondiMenu = false;
              //this.pageHeaderObj.mostraLogo   = false;
            }
          });
        }, 500);


        }, ( err )=>{
      console.debug('getUserInfo 2');
      this.isAuthenticationLoader = false;
      this.router.navigateByUrl('login'); /* fix me per geocall */
      });
    //});





  }









}
