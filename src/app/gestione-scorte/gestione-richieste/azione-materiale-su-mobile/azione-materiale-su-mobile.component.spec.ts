import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneMaterialeSuMobileComponent } from './azione-materiale-su-mobile.component';

describe('AzioneMaterialeSuMobileComponent', () => {
  let component: AzioneMaterialeSuMobileComponent;
  let fixture: ComponentFixture<AzioneMaterialeSuMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneMaterialeSuMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneMaterialeSuMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
