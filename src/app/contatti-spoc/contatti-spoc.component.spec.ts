import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContattiSpocComponent } from './contatti-spoc.component';

describe('ContattiSpocComponent', () => {
  let component: ContattiSpocComponent;
  let fixture: ComponentFixture<ContattiSpocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContattiSpocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContattiSpocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
