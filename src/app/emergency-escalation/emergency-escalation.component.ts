import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { MenuCfgModule } from '../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from '../ui-service.service';

@Component({
  selector: 'app-emergency-escalation',
  templateUrl: './emergency-escalation.component.html',
  styleUrls: ['./emergency-escalation.component.css']
})
export class EmergencyEscalationComponent implements OnInit {
  
  navTileMenuCfgObj;
  navTileMenuCfg;
  
  livelloAttuale;
  titoloPagina;
  
  livelloPassato:any;
  
  constructor(private activatedroute:ActivatedRoute,private router: Router,private uiService: UiService) {
    console.log('funziona EmergencyEscalationComponent');
    this.navTileMenuCfgObj = new MenuCfgModule(this.uiService);
    this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
    this.livelloPassato = this.buildLivello(this.activatedroute.root);
     this.livelloAttuale = this.activatedroute.snapshot.data.livello;
     this.titoloPagina = this.activatedroute.snapshot.data.titolo;

    }

  ngOnInit(): void {
    
     /* this.activatedroute.data.subscribe(data => {
          this.livelloPassato=data.livello;
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
      });*/
      
     console.debug('qui:', this.activatedroute.snapshot.data.livello); 
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.livelloPassato = this.buildLivello(this.activatedroute.root);
        this.livelloAttuale = this.activatedroute.snapshot.data.livello;
        this.titoloPagina = this.activatedroute.snapshot.data.titolo;
    })
    
    
    console.debug('sono EmergencyEscalationComponent 1: ',this.navTileMenuCfg);
    console.debug('sono EmergencyEscalationComponent 1: ',this.livelloPassato);
    
  }

  ngOnChanges(): void {
    
    console.debug('sono EmergencyEscalationComponent 2: ',this.navTileMenuCfg);
    console.debug('sono EmergencyEscalationComponent 2: ',this.livelloPassato);
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
  }
  
  buildLivello(route: ActivatedRoute): string {
      console.debug('buildBreadCrumb: ',route);
      let livello = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      if (route.firstChild) {

          return this.buildLivello(route.firstChild);
      }
      return livello;
  } 

}
