import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { MenuCfgModule } from '../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from './../ui-service.service';


@Component({
  selector: 'app-richiesta-materiali-consumo',
  templateUrl: './richiesta-materiali-consumo.component.html',
  styleUrls: ['./richiesta-materiali-consumo.component.css']
})

export class RichiestaMaterialiConsumoComponent implements OnInit {
  
  navTileMenuCfgObj;
  navTileMenuCfg;
  livelloAttuale;
  
  livelloPassato:any;
  
  
  constructor(private activatedroute:ActivatedRoute,private router: Router,private uiService: UiService) {
    console.log('funziona RichiestaMaterialiConsumoComponent');
    this.navTileMenuCfgObj = new MenuCfgModule(this.uiService);
    this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
    this.livelloPassato = this.buildLivello(this.activatedroute.root);
    this.livelloAttuale = this.activatedroute.snapshot.data.livello;

    }

  ngOnInit(): void {
    
     /* this.activatedroute.data.subscribe(data => {
          this.livelloPassato=data.livello;
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
      });*/
      
    console.log('qui:', this.activatedroute.snapshot.data.livello); 
    this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationEnd),
        distinctUntilChanged(),
    ).subscribe(() => {
        this.livelloPassato = this.buildLivello(this.activatedroute.root);
        this.livelloAttuale = this.activatedroute.snapshot.data.livello;
    })
    
    
    console.log('sono RichiestaMaterialiConsumoComponent 1: ',this.navTileMenuCfg);
    console.log('sono RichiestaMaterialiConsumoComponent 1: ',this.livelloPassato);
    
  }

  ngOnChanges(): void {
    
    console.log('sono RichiestaMaterialiConsumoComponent 2: ',this.navTileMenuCfg);
    console.log('sono RichiestaMaterialiConsumoComponent 2: ',this.livelloPassato);
          //this.navTileMenuCfgObj = new MenuCfgModule();
          //this.navTileMenuCfg = this.navTileMenuCfgObj.mainMenuCfg;
    
  }
  
  buildLivello(route: ActivatedRoute): string {
      console.log('buildBreadCrumb: ',route);
      let livello = route.routeConfig && route.routeConfig.data ? route.routeConfig.data.livello : '';
      if (route.firstChild) {

          return this.buildLivello(route.firstChild);
      }
      return livello;
  } 

}

