import { TestBed } from '@angular/core/testing';

import { VediAttivitaService } from './vedi-attivita.service';

describe('VediAttivitaService', () => {
  let service: VediAttivitaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VediAttivitaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
