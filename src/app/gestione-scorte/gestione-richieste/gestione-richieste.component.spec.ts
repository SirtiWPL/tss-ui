import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneRichiesteComponent } from './gestione-richieste.component';

describe('GestioneRichiesteComponent', () => {
  let component: GestioneRichiesteComponent;
  let fixture: ComponentFixture<GestioneRichiesteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestioneRichiesteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneRichiesteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
