import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VediAttivitaComponent } from './vedi-attivita.component';

describe('VediAttivitaComponent', () => {
  let component: VediAttivitaComponent;
  let fixture: ComponentFixture<VediAttivitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VediAttivitaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VediAttivitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
