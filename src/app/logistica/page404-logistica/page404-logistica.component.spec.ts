import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404LogisticaComponent } from './page404-logistica.component';

describe('Page404LogisticaComponent', () => {
  let component: Page404LogisticaComponent;
  let fixture: ComponentFixture<Page404LogisticaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Page404LogisticaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404LogisticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
