import { iSidebarDataModule } from '../admin-lte/sidebar-page/sidebar-data.interface'

export class SidebarCfgModule {
  
		sidebarCfg = {
      cssStyleColorSideBar  : 'sidebar-light-orange',
      cssStyleLogoSideBar   : 'navbar-orange navbar-dark',
      logoImageCliente      : 'assets/loghi/WIND_3_logo_bianco_full.png',
      logoImageSirti        : 'assets/loghi/navbar-sirti-small.png',
      nomeApp               : 'Wind3.0',
      mostraLogoCustom      : true
		};
  
};  
