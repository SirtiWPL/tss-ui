import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-body-page',
  templateUrl: './body-page.component.html',
  styleUrls: ['./body-page.component.css']
})
export class BodyPageComponent implements OnInit,OnChanges {


  @Input() bodyTileMenuCfg; /* definisco la cfg del menu a tile nel body */

  constructor() { }

  ngOnInit(): void {
    console.debug('sono il BodyPageComponent: ',this.bodyTileMenuCfg);
    
    
    
  }
  
  ngOnChanges(): void {
  }
  
  onActivate(componentReference) {
   console.debug(componentReference)
   componentReference.anyFunction(this.bodyTileMenuCfg);
  }

}
