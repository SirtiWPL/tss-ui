import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404RichiestaMaterialiConsumoComponent } from './page404-richiesta-materiali-consumo.component';

describe('Page404RichiestaMaterialiConsumoComponent', () => {
  let component: Page404RichiestaMaterialiConsumoComponent;
  let fixture: ComponentFixture<Page404RichiestaMaterialiConsumoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Page404RichiestaMaterialiConsumoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404RichiestaMaterialiConsumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
