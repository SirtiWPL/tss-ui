import { Component, OnInit ,Input,Output,EventEmitter,Injectable} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';


/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'app-azione-rifiuto-materiale-accettato',
  templateUrl: './azione-rifiuto-materiale-accettato.component.html',
  styleUrls: ['./azione-rifiuto-materiale-accettato.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider
})
export class AzioneRifiutoMaterialeAccettatoComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzRifiutoMaterialeAccettato: FormGroup;
  public aOptionValueResoPresso:any = [];
  public aOptionMotivazioneRifiuto:any = [];
  
  public loaderEseguiAzione             : boolean;
  public mostraEsitoAzione              : boolean;  
  public tipoEsitoEsitoAzione           : string;  
  public msgEsitoAzione                 : string;
  public mostraAltraMotivazioneRifiuto  : boolean;
  
  public markDisabled;
  public calendar: NgbCalendar;
  


  constructor(private formBuilderAzRifiutoMaterialeAccettato: FormBuilder,
              calendar: NgbCalendar,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) {
    this.calendar = calendar;
    
    }
    
  disableDays() {
      this.markDisabled = (date: NgbDate) => {
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.datiAzioneObj.presidioMagazWind);
        let ritorno = (this.calendar.getWeekday(date) >= 6);
        if(this.datiAzioneObj.presidioMagazWind) {
          if(this.datiAzioneObj.presidioMagazWind.LUNEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 1);
          }
          if(this.datiAzioneObj.presidioMagazWind.MARTEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 2);
          }
          if(this.datiAzioneObj.presidioMagazWind.MERCOLEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 3);
          }
          if(this.datiAzioneObj.presidioMagazWind.GIOVEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 4);
          }
          if(this.datiAzioneObj.presidioMagazWind.VENERDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 5);
          }
        }
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',ritorno);
        return ritorno;
      };
       //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.markDisabled);
    
  }

  ngOnInit(): void {
    
    console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzRifiutoMaterialeAccettato  in base al tipo materiale */
    
    if (this.datiAzioneObj.materiale.SERIAL_NUMBER) { /* SERIAL_NUMBER */
      
      this.reactiveFormAzRifiutoMaterialeAccettato = this.formBuilderAzRifiutoMaterialeAccettato.group({
        nQuantitaCheck                : [1,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1),Validators.max(parseInt(this.datiAzioneObj.materiale.QUANTITA))]],
        sTipologiaGuastoCheck         : [null,[Validators.required]],
        sCausaleCheck                 : [null,[Validators.required]],
        sMotivazioneRifiutoCheck      : [null,[Validators.required]],        
        sRestituzioneResoPressoCheck  : [null,[Validators.required]],
        sDataRitiroAppuntamentoCheck  : [null,[Validators.required]],
        sOraRitiroAppuntamentoCheck   : [null,[Validators.required,this.changeTime.bind(this)]],
        

      });
      
      
      this.aOptionValueResoPresso = [];
      
      /* se SEDE WIND allora forzo a SEDE WIND come unico valore */
      if (this.datiAzioneObj.materiale.RESO_PRESSO_GLS.toUpperCase() === 'SEDE WIND') {
        this.aOptionValueResoPresso = ['SEDE WIND'];
        this.reactiveFormAzRifiutoMaterialeAccettato.get("sRestituzioneResoPressoCheck").setValue('SEDE WIND', {onlySelf: false});
      } else {
        /* else allora forzo a PUNTO GLS ma permetto la selezine */
        this.aOptionValueResoPresso = ['PUNTO GLS','SEDE WIND'];
        this.reactiveFormAzRifiutoMaterialeAccettato.get("sRestituzioneResoPressoCheck").setValue('PUNTO GLS', {onlySelf: false});
        
      }
      
      
      this.aOptionMotivazioneRifiuto = this.datiAzioneObj.motivazioneRifiuto;

      
      this.disableDays();

    } else { /* CONSUMABILE */
      this.reactiveFormAzRifiutoMaterialeAccettato = this.formBuilderAzRifiutoMaterialeAccettato.group({
        nQuantitaCheck                : [parseInt(this.datiAzioneObj.materiale.QUANTITA),[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1),Validators.max(parseInt(this.datiAzioneObj.materiale.QUANTITA))]],
        sTipologiaGuastoCheck         : [null,[Validators.required]],
        sCausaleCheck                 : [null,[Validators.required]],
        sMotivazioneRifiutoCheck      : [null,[Validators.required]],
      });
      
      this.aOptionMotivazioneRifiuto      = this.datiAzioneObj.motivazioneRifiuto;
      this.mostraAltraMotivazioneRifiuto  = false;
      
      
    }
    
    /* gestisco il multistep */
    if (this.datiAzioneObj.multiStep) {
      this.reactiveFormAzRifiutoMaterialeAccettato.addControl('sEsitoConsegnaCheck', this.formBuilderAzRifiutoMaterialeAccettato.control(null, [Validators.required]));
      this.reactiveFormAzRifiutoMaterialeAccettato.addControl('sDataConsegnaCheck', this.formBuilderAzRifiutoMaterialeAccettato.control(null, [Validators.required]));
      this.reactiveFormAzRifiutoMaterialeAccettato.addControl('sOraConsegnaCheck', this.formBuilderAzRifiutoMaterialeAccettato.control(null, [Validators.required]));
      
    }

    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    

    
  }  
  
  public eseguiAzione():void {
    console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => eseguiAzione: ',this.reactiveFormAzRifiutoMaterialeAccettato.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    let descMotivoRifiuto = this.reactiveFormAzRifiutoMaterialeAccettato.value.sMotivazioneRifiutoCheck ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sMotivazioneRifiutoCheck : null;
    if (this.mostraAltraMotivazioneRifiuto) {
      descMotivoRifiuto = this.reactiveFormAzRifiutoMaterialeAccettato.value.sAltraMotivazioneRifiutoCheck ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sAltraMotivazioneRifiutoCheck : null;
    }
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente,
      ID_ATTIVITA             : this.datiAzioneObj.materiale.ID,
      QUANTITA             	  : this.reactiveFormAzRifiutoMaterialeAccettato.value.nQuantitaCheck           ? this.reactiveFormAzRifiutoMaterialeAccettato.value.nQuantitaCheck : null,    
      AZIONE             		  : 'RIFIUTO MATERIALE ACCETTATO',    
      CAUSALE_RIFIUTO         : this.reactiveFormAzRifiutoMaterialeAccettato.value.sCausaleCheck            ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sCausaleCheck            :null,      
      MOTIVO_RIFIUTO          :	descMotivoRifiuto,    
      TIPOLOGIA_GUASTO        : this.reactiveFormAzRifiutoMaterialeAccettato.value.sTipologiaGuastoCheck    ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sTipologiaGuastoCheck : null,     
      NOTE_GUASTO        		  : null,    
      TIPO_RICHIESTA_RESO     : 'RESO',      
      CAUSALE_RESO     		    : 'FUNZIONANTE',
      TIPOLOGIA_GUASTO_RESO   : 'Scheda Funzionante',     
      DDT_RESO   				      : null,
      SENZA_RESTIT_RESO   	  : null,    
      NODO_PROVENIENZA_RESO   : null,      
      DATA_APPRIT_RESO        : this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck+' '+this.reactiveFormAzRifiutoMaterialeAccettato.value.sOraRitiroAppuntamentoCheck : null,   
      VINC_CONS_CONC_RESO     : 'NO',      
      RESO_PRESSO_GLS         : this.reactiveFormAzRifiutoMaterialeAccettato.value.sRestituzioneResoPressoCheck ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sRestituzioneResoPressoCheck : null,    
      MULTI_STEP              : this.datiAzioneObj.multiStep                                                    ? 'SI' : 'NO',
      ESITO_CONSEGNA          : this.reactiveFormAzRifiutoMaterialeAccettato.value.sEsitoConsegnaCheck          ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sEsitoConsegnaCheck : null,
      DATA_CONSEGNA           : this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataConsegnaCheck           ? this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataConsegnaCheck+' '+this.reactiveFormAzRifiutoMaterialeAccettato.value.sOraConsegnaCheck : null,
    };
    
    this.gestScorteServices.materialeUtilizzatoRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneRifiutoMaterialeAccettatoComponent => materialeUtilizzatoRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo. Creato il Reso: '+data.data.ID_ATT_RESO;
        this.outReload.emit(true);
        
      }

    });
    
  }
  
  changeResoPressoValue(e) {
    e.stopPropagation();
    this.reactiveFormAzRifiutoMaterialeAccettato.get("sRestituzioneResoPressoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeResoPressoValue',this.reactiveFormAzRifiutoMaterialeAccettato.value.sRestituzioneResoPressoCheck);

   }
   
  changeMotivazioneRifiutoValue(e) {
    e.stopPropagation();
    
    this.reactiveFormAzRifiutoMaterialeAccettato.get("sMotivazioneRifiutoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeMotivazioneRifiutoValue',this.reactiveFormAzRifiutoMaterialeAccettato.value.sMotivazioneRifiutoCheck);
    if (this.reactiveFormAzRifiutoMaterialeAccettato.get("sAltraMotivazioneRifiutoCheck")) {
      this.reactiveFormAzRifiutoMaterialeAccettato.get("sAltraMotivazioneRifiutoCheck").clearValidators();
      this.reactiveFormAzRifiutoMaterialeAccettato.get("sAltraMotivazioneRifiutoCheck").updateValueAndValidity({ emitEvent : false });
      this.reactiveFormAzRifiutoMaterialeAccettato.removeControl('sAltraMotivazioneRifiutoCheck');
      
    }
    this.mostraAltraMotivazioneRifiuto  = false;
    if (this.reactiveFormAzRifiutoMaterialeAccettato.value.sMotivazioneRifiutoCheck === 'ALTRO: Inserire Descrizione') {
      this.reactiveFormAzRifiutoMaterialeAccettato.addControl('sAltraMotivazioneRifiutoCheck', this.formBuilderAzRifiutoMaterialeAccettato.control(null, [Validators.required]));
      this.mostraAltraMotivazioneRifiuto  = true;
    }
   }
   
   

   
  public onDateSelect(dateSelected,sChiamante) {
    console.debug('sono AzioneMaterialeUtilizzatoComponent => onDateSelect',dateSelected);
    if (sChiamante.toUpperCase() === 'DATA_CONSEGNA') { /* DATA_CONSEGNA */ 
      this.reactiveFormAzRifiutoMaterialeAccettato.get("sOraConsegnaCheck").setValue('00:00:00', {onlySelf: false});
    } else { /* DATA_RITIRO_APPUNTAMENTO */ 
      this.reactiveFormAzRifiutoMaterialeAccettato.get("sOraRitiroAppuntamentoCheck").setValue(this.datiAzioneObj.orario.min, {onlySelf: false});
    }
    
  }
  
  public changeTime(timeSelected) {
    console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime',timeSelected);
    /*
     if (timeSelected.touched && !this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
     */ 
     if (timeSelected.touched && !timeSelected.value && this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
      
     if (timeSelected.touched && timeSelected.value && !this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
    
    
     if (timeSelected.value) {
      const aTimeSelected = timeSelected.value.split(':');
      const dataRipApp = this.reactiveFormAzRifiutoMaterialeAccettato.value.sDataRitiroAppuntamentoCheck;
      

      
      const sDataSelected = dataRipApp+' '+timeSelected.value;

      let dateParts = sDataSelected.substring(0, 10).split("-");
      let oreMinParts = sDataSelected.substring(11, 19).split(":");


      

      const dateNew = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => DATA SELECTED',sDataSelected,dateNew);
      
      
      const sDataMin = dataRipApp+' '+this.datiAzioneObj.orario.min;

      dateParts = sDataMin.substring(0, 10).split("-");
      oreMinParts = sDataMin.substring(11, 19).split(":");

     

      const dateMin = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
       console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => DATA MIN',sDataMin,dateMin);
      
      const sDataMax = dataRipApp+' '+this.datiAzioneObj.orario.max;

      dateParts = sDataMax.substring(0, 10).split("-");
      oreMinParts = sDataMax.substring(11, 19).split(":");

     

      const dateMax = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => DATA MAX',sDataMax,dateMax);
      
      
      if (dateNew < dateMin) {
       console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => ERROR A < MIN ',dateNew,dateMin);
       //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
       return {errorMinMax:true};
       //this.reactiveFormAzRifiutoMaterialeAccettato.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMin.substring(11, 19), {onlySelf: false});
      } else {
        if (dateNew > dateMax) {
         console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => ERROR A > MAX ',dateNew,dateMax);
         //this.reactiveFormAzRifiutoMaterialeAccettato.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMax.substring(11, 19), {onlySelf: false});
         //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
         return {errorMinMax:true};
        } else {
          console.debug('sono AzioneRifiutoMaterialeAccettatoComponent => changeTime => VALORE ACCETTATO:  MIN <= A <= MAX ',dateNew,dateMin,dateMax);
          //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
          return false;
        }
      }    
 
   }
  }

}

