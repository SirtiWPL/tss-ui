import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/user.model';
import { configUI } from 'src/app/configUI.model';
import { map, switchMap,catchError} from 'rxjs/operators';

import { CookieService } from 'ngx-cookie-service';

const apiUrlWindServices = environment.apiUrlWindServices;
const apiUrlWindServicesRoot = environment.apiUrlWindServicesRoot;

@Injectable({
  providedIn: 'root'
})
export class UiService {

  // url per servizio utility
  private UrlWindServices = apiUrlWindServices;
  private UrlWindServicesRoot = apiUrlWindServicesRoot;

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  private configUISubject: BehaviorSubject<configUI>;
  public configUI: Observable<configUI>;

  private cookieValue : string;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(sessionStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();

    this.configUISubject = new BehaviorSubject<configUI>(
      JSON.parse(sessionStorage.getItem('ConfigUI'))
    );
    this.configUI = this.configUISubject.asObservable();
  }

  public get getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  public get getJwtToken(): configUI {
    return this.configUISubject.value;
  }

  public refreshToken() {
    const Url = 'http://dvmas003.sirtisistemi.net:10101/keepalive.html';
    console.log('into refreshToken');
    return this.http.get<any>('http://dvmas003.sirtisistemi.net:10101/keepalive.html');
  }

  // Servizi esposti
  getUserInfo() {
    console.log('getUserInfo');
    sessionStorage.setItem('currentUser', null);


    this.currentUserSubject.next(null);
    return this.http.get(this.UrlWindServices + '/pyramide/customers/WIND/profile/user')
      .pipe(
        map(
          data => {
            console.debug('getUserInfo: ',data['data']);
            const newUser = <User>({
              nome                            :     data['data']['nome'],
              cognome                         :     data['data']['cognome'],
              idUtente                        :     data['data']['IdUtente'],
              nomeUtente                      :     data['data']['nomeUtente'],
              email                           :     data['data']['email'],
              gruppi                          :     data['data']['gruppi'],
              isRoot                          :     data['data']['IS_ROOT']                     ,
              isAssTecWind                    :     data['data']['IS_ASSTECWIND']               ,
              isMagaz                         :     data['data']['IS_MAGAZ']                    ,
              isOperatoreAziendaEsterna       :     data['data']['IS_OPERATOREAZIENDAESTERNA']  ,
              isPm                            :     data['data']['IS_PM']                       ,
              isSqWind                        :     data['data']['IS_SQWIND']                   ,
              isWindHq                        :     data['data']['IS_WINDHQ']                   ,
              isAbilitatoAcceleraSla          :     data['data']['IS_ABILITATOACCELERASLA']     ,
              isAbilitatoGestCompatibili      :     data['data']['IS_ABILITATOGESTCOMPATIBILI'] ,
              isAbilitatoKpi                  :     data['data']['IS_ABILITATOKPI']             ,
              isAbilitatoMenuLogistica        :     data['data']['IS_ABILITATOMENULOGISTICA']   ,
              isAbilitatoMinerva              :     data['data']['IS_ABILITATOMINERVA']         ,
              isAbilitatoPresidioMagaz        :     data['data']['IS_ABILITATOPRESIDIOMAGAZ']   ,
              isAbilitatoRichFornitura        :     data['data']['IS_ABILITATORICHFORNITURA']   ,
              isAbilitatoTrasfermento         :     data['data']['IS_ABILITATOTRASFERMENTO']    ,
              isAbilitatoVincoloSvincolo      :     data['data']['IS_ABILITATOVINCOLOSVINCOLO'] ,              
              isAbilitatoMenuRicMatConsumo    :     data['data']['IS_ABILITATOMENURICMATCONSUMO'] ,
              isAbilitatoNuovaRicMatConsumo   :     data['data']['IS_ABILITATONUOVARICMATCONSUMO'] ,
              isAbilitatoAppRicMatConsumo     :     data['data']['IS_ABILITATOAPPRICMATCONSUMO'] ,
              isAbilitatoStatoRicMatConsumo   :     data['data']['IS_ABILITATOSTATORICMATCONSUMO'] ,

            });

            sessionStorage.setItem('currentUser', JSON.stringify(newUser));


            this.currentUserSubject.next(newUser);
            return newUser;
          }
        ),
        /*
        switchMap(dataFromSvc1 => {
          //return this.http.get<any>('http://dvmas003.sirtisistemi.net:10101/keepalive.html');
          return this.http.get('http://dvmas003.sirtisistemi.net:10101/keepalive.html', {
           //headers: {'Content-Type': 'application/json', 'skipAuthorization': 'true'}
           headers: {'Content-Type': 'application/json'}
          });
       }),
        */

      )
      ;

  }

  configService( qParams:any ): Observable<any> {
    const Url = this.UrlWindServicesRoot + '/api/art/sessions';
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(qParams);
            this.configUISubject.next(null);
            sessionStorage.setItem('ConfigUI', null);
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body)
      .pipe(
        map(
          data => {
            const newUiConfig = <configUI>({
              jwtToken: data['token']
            });

            this.configUISubject.next(newUiConfig);
            sessionStorage.setItem('ConfigUI', JSON.stringify(newUiConfig));
            return newUiConfig;
          }
        )
      );
  }
  
  /* da testare */
  configServiceJwtTokenWind(): Observable<any> {
    const Url = this.UrlWindServices + '/pyramide/customers/WIND/geocall/checkToken';
    this.cookieValue = this.cookieService.get('geocall');
    console.log(this.cookieService.get('geocall'));
    console.log('Cookie GEOCALL: '+ this.cookieValue);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'x-wind-jwt'    : this.cookieValue
    });



    let options = { headers: headers };

    const body= {'wind-jwt' : this.cookieValue};
    
    //return this.http.post<any>(Url,{params : new HttpParams({ fromObject: qParams })});
    return this.http.post<any>(Url, body, options)
      .pipe(
        map(
          data => {
            const newUiConfig = <configUI>({
              jwtToken: data.data['token'],
              decoded:  data.data['decoded']
            });

            this.configUISubject.next(newUiConfig);
            sessionStorage.setItem('ConfigUI', JSON.stringify(newUiConfig));
            return newUiConfig;
          }
        )
      );
  }

  configServiceOld(): Observable<configUI> {
    return this.http.get('assets/config.json.html')
      .pipe(
        map(
          data => {
            const newUiConfig = <configUI>({
              jwtToken: data['jwtToken']
            });

            this.configUISubject.next(newUiConfig);
            return newUiConfig;
          }
        )
      );

  }


}
