export class User {

  constructor(
    public nome: string,
    public cognome: string,
    public idUtente: string,
    public nomeUtente: string,
    public email: string,
    public gruppi: Array<string>,
    public isRoot: boolean,
    public isAssTecWind                   : boolean,  
    public isMagaz                        : boolean,  
    public isOperatoreAziendaEsterna      : boolean,  
    public isPm                           : boolean,  
    public isSqWind                       : boolean,  
    public isWindHq                       : boolean,  
    public isAbilitatoAcceleraSla         : boolean,  
    public isAbilitatoGestCompatibili     : boolean,  
    public isAbilitatoKpi                 : boolean,  
    public isAbilitatoMenuLogistica       : boolean,  
    public isAbilitatoMinerva             : boolean,  
    public isAbilitatoPresidioMagaz       : boolean,  
    public isAbilitatoRichFornitura       : boolean,  
    public isAbilitatoTrasfermento        : boolean,  
    public isAbilitatoVincoloSvincolo     : boolean,    
    public isAbilitatoMenuRicMatConsumo   : boolean,
    public isAbilitatoNuovaRicMatConsumo  : boolean,
    public isAbilitatoAppRicMatConsumo    : boolean,
    public isAbilitatoStatoRicMatConsumo  : boolean  
  ){}


  public getNome(): string {
    return this.nome;
  }

  public setNome(nome: string): void {
      this.nome = nome;
  }

  public getCognome(): string {
      return this.cognome;
  }

  public setCognome(cognome: string): void {
      this.cognome = cognome;
  }

  public getNomeCognome(): string {
    return this.getNome() + ' ' + this.getCognome();
  }

  public setGruppi(gruppi: Array<string>): void {
    this.gruppi = gruppi;
  }

  public getGruppi(): Array<string> {
    return this.gruppi;
  }

  public setIdUtente(idUtente: string): void {
    this.idUtente = idUtente;
  }

  public getIdUtente(): string {
    return this.idUtente;
  }

  public setNomeUtente(nomeUtente: string): void {
    this.nomeUtente = nomeUtente;
  }

  public getNomeUtente(): string {
    return  this.nomeUtente;
  }

  public setEmail(email: string): void {
    this.email = email;
  }

  public getEmail(): string {
    return  this.email;
  }

}
