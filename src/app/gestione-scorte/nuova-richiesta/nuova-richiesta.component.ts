import { Injectable, Component, OnInit,Renderer2,Inject  } from '@angular/core';
import { RouterModule, Routes,ActivatedRoute, Router } from '@angular/router';

import { NgModule } from '@angular/core';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';

/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';

import { UiService } from './../../ui-service.service';
import { GestioneScorteService } from './../../gestione-scorte.service';
import { DOCUMENT } from '@angular/common';
/* google */
declare const google;

type tMagazziniWind = {
  ID_MAG_SIRTI                      : number,
  SEDE                              : string,
  INDIRIZZO                         : string,
  COMUNE                            : string,
  PROVINCIA                         : string,
  LATITUDINE                        : string,
  LONGITUDINE                       : string,
  PROPRIETA                         : string,
  MAGAZZINO_SIRTI_CORRISPONDENTE    : string,
  DISTANZA_MAG_SIRTI_CORR_METRI     : number,
  CAP                               : string,
  ID_OPERATORE_RESPONSABILE_ZONA    : string,
  ZONA                              : string,
  REGIONE                           : string
  };


/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}



@Component({
  selector: 'app-nuova-richiesta',
  templateUrl: './nuova-richiesta.component.html',
  styleUrls:  ['./nuova-richiesta.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider

})
export class NuovaRichiestaComponent implements OnInit {

  richiedente   : string;
  workOrderKey  : string;
  jwtTokenGeocall : string;
  styleInterno  : boolean;
  styleGeocall  : boolean;
  showToast     : boolean;
  delayToast    : number;
  autohideToast : boolean;
  messaggioToast  : string;
  tempoRestanteSessione : number;

  /* definisco il reactive form */
  reactiveFormCercaMateriale: FormGroup;
  reactiveFormCreaRma:        FormGroup;

  /* per la gestione degli autocomplete */
  public loaderAutocompleteMagaz:boolean;
  public loaderAutocompleteSedeGls:boolean;
  public loaderAutocompleteMat:boolean;
  public loaderAutocompleteMaps:boolean;
  public zeroResultMagazzino:boolean;
  public zeroResultMateriale:boolean;
  public zeroResultSedeGls:boolean;
  public zeroResultGoogleMaps:boolean;
  public googleMapsIsDefined:boolean;

  /* per la gestione del numeric qta */
  public abilitaQtaMatRichiesta:boolean;


  /* formatter degli autocomplete */
  formatterMagazWind  = (magazzinoWindObj: tMagazziniWind) => magazzinoWindObj.SEDE;
  formatterMateriale  = (materialeObj: any) => materialeObj.PART_NUMBER;
  formatterSedeGls    = (sedeGlsObj: any) => sedeGlsObj.SEDE;

  /* verifica giacenza */
  public loaderSearch:boolean;
  public resultVerificaGiacenza:any;
  public disabledDefaultEdit:boolean;
  public aOptionValueResoPresso:any = [];
  public aOptionValueTipoConsegna:any =[];

  public creaRmaSection:any = {
    tipoOperazione                  : '',
    titoloOperazione                : '',
    tipoMessaggio                   : '',
    mostraSezioneCreaRma            : false,
    mostraMsgEsitoRicercaMateriale  : false,
    msgSotituitoEquivalente         : '',
    mostraSezioneCustom             : false,
    mostraButtonRilassa             : false,
    mostraCampiRilassaSla           : false,
    mostraButtonAccelera            : false,
    mostraCampiAcceleraSla          : false,
    mostraOtrAccelera               : false,
    mostraCampiOtrAccelera          : false,
    mostraOtr                       : false,
    mostraCampiOtr                  : false,
    mostraTipoConsegna              : false,
    mostraCampiTipoConsegna         : false,
    dataAppConsMaxDate              : null,
    dataAppConsMinDate              : null,
    sDataAppConsMaxDateTime         : null,
    sDataAppConsMinDateTime         : null,
    abilitaCreaRma                  : false,
    loaderCreaRma                   : false,
    mostraEsitoCreaRma              : false,
    tipoEsitoCreaRma                : '',
    msgEsitoCreaRma                 : '',
    numeroRmaCreate                 : 0,
    isSwap                          : false,
    isGiacenza                      : false,
    isRmaAttesa                     : false,
    geocallScaduta                  : false,
    loaderSessionGeocall            : false,
    msgUltimaCreaRmaGeocall         : false,
  };

  /* aMagazziniOperatore */
  public aMagazziniOperatore: tMagazziniWind[];

  /* google */

  public aMarkersOtr    : any = [];
  public esitoValidaOtr :string;


  constructor(private route:ActivatedRoute,private router: Router,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService,
              private formBuilderCercaMateriale: FormBuilder,
              private formBuilderCreaRma: FormBuilder,
              @Inject(DOCUMENT) private document: Document,
              private renderer2: Renderer2
              ) {
    this.richiedente  = 'INTERNO';  // valore di default INTERNO
    this.styleInterno = true;       // mostro INTERNO
    this.styleGeocall = false;      // nascondo GEOCALL
    this.showToast      = false; // toast
    this.delayToast     = 0; // toast
    this.autohideToast  = false; // toast
    this.messaggioToast = '';
    this.tempoRestanteSessione = 0;
    


    /* reactiveFormCercaMateriale */

    this.reactiveFormCercaMateriale = this.formBuilderCercaMateriale.group({
      magazzinoCheck    : [null,[Validators.required]],
      materialeCheck    : [null,[Validators.required]],
      qtaCheck          : [1,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1)]]
    });

    /* reactiveFormCreaRma */
    this.reactiveFormCreaRma = this.formBuilderCreaRma.group({
      ttRemedyCheck                       : [null,[Validators.required]],
      restituzioneResoPressoCheck         : [null,[Validators.required]],
      slaOriginaleCheck                   : ['4',[Validators.required]],
      dataPrevistaConsegnaCheck           : ['4',[Validators.required]],
      noteStandardCheck                   : [null],
      tipoConsegnaCheck                   : [null,[Validators.required]],
      sedeGlsCheck                        : [null,[Validators.required]],
      indirizzoGlsCheck                   : [null,[Validators.required]],
      capGlsCheck                         : [null,[Validators.required]],
      localitaGlsCheck                    : [null,[Validators.required]],
      provinciaGlsCheck                   : [null,[Validators.required]],
      regioneGlsCheck                     : [null,[Validators.required]],
      telefonoGlsCheck                    : [null,[Validators.required]],
      faxGlsCheck                         : [null,[Validators.required]],
      indirizzoConsegnaCheck              : [null,[Validators.required]],
      capConsegnaCheck                    : [null,[Validators.required]],
      comuneConsegnaCheck                 : [null,[Validators.required]],
      provinciaConsegnaCheck              : [null,[Validators.required]],
      regioneConsegnaCheck                : [null,[Validators.required]],
      nazioneConsegnaCheck                : [null,[Validators.required]],
      telefonoConsegnaCheck               : [null,[Validators.required]],
      referenteConsegnaCheck              : [null,[Validators.required]],
      latitudineOtrCheck                  : [null,[Validators.required]],
      longitudineOtrCheck                 : [null,[Validators.required]],
      IndirizzoOtrCheck                   : [null,[Validators.required]],
      dataConsegnaSuAppuntamentoStdCheck  : [null],
      oraConsegnaSuAppuntamentoStdCheck   : [null,[this.changeTime.bind(this)]],
      slaAcceleraCheck                    : ['4',[Validators.required]],
      dataPrevistaConsegnaAcceleraCheck   : ['4',[Validators.required]],
      noteAcceleraCheck                   : [null],
      slaRilassaCheck                     : ['24',[Validators.required]],
      dataPrevistaConsegnaRilassaCheck    : ['24',[Validators.required]],
      noteRilassaCheck                    : [null],
    });

    this.disabledDefaultEdit  = true;
    this.esitoValidaOtr       = '';

  }

  ngOnInit(): void {

    console.debug('ngOnInit()');
    
    this.creaRmaSection.geocallScaduta                  = false;
    

    this.loadAutoComplete();

    console.log(this.uiService.getCurrentUser);
    //console.log(this.uiService.getJwtToken.jwtToken);

    this.googleMapsIsDefined = false;
    // First get the product id from the current route.
    const routeParams = this.route.snapshot.paramMap;
    this.richiedente  = routeParams.get('richiedente').toUpperCase();
    

    
    
    
    console.log('geocall workOrderKey: ',this.workOrderKey);
    console.log('geocall jwtTokenGeocall: ',this.jwtTokenGeocall);
    
    this.reactiveFormCercaMateriale.get("qtaCheck").setValue(1, { onlySelf: true });
    
    

    if (this.richiedente === 'INTERNO') {
      this.styleInterno = true;       // mostro INTERNO
      this.styleGeocall = false;      // nascondo GEOCALL

      /* recupero i magazzini dell'operatore - Inizio */

      const qParamsMagazziniOp = {
        RICHIEDENTE: this.richiedente
      };

      this.gestScorteServices.getWarehouses(qParamsMagazziniOp).subscribe(( data )=>{
        console.debug( data );
        this.aMagazziniOperatore = data.data.results;
      });
      /* recupero i magazzini dell'operatore - Fine */


      this.getRichiesteRmaCreate();

    } else if (this.richiedente === 'GEOCALL') {
      
      this.creaRmaSection.loaderSessionGeocall            = true;
      this.styleInterno = false;       // nascondo  INTERNO
      this.styleGeocall = true;      //  mostro GEOCALL
      
      
      this.uiService.configServiceJwtTokenWind().subscribe(( data )=>{
          console.debug('NuovaRichiestaComponent => GEOCALL => configServiceJwtTokenWind => OK => data',data);
          
         
          this.jwtTokenGeocall     = data.decoded && data.decoded.wo_sistema_esterno ? data.decoded.wo_sistema_esterno : null;
          this.workOrderKey        = data.decoded && data.decoded.wo_geocall         ? data.decoded.wo_geocall : null;
            
          /* registraJwtGeocall */
          
          const qParamsRegistraJwtGeocall = {
            RICHIEDENTE: this.richiedente
          };
          
          this.gestScorteServices.registraJwtGeocall(qParamsRegistraJwtGeocall).subscribe(( data )=>{
          console.debug('NuovaRichiestaComponent => GEOCALL => registraJwtGeocall => OK => data',data);
          
          this.creaRmaSection.geocallScaduta = false;
          
          this.creaRmaSection.loaderSessionGeocall            = false;
          
          if (data.data.ESITO === 'OK'){ /* fixme a OK */
            console.log('geocall workOrderKey: ',this.workOrderKey);
            console.log('geocall jwtTokenGeocall: ',this.jwtTokenGeocall);
            this.controllaSessioneGeocall(); /* procedura che controlla la sessione */  
              
            this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
            
            const qParamsMagazziniOp = {
              RICHIEDENTE: this.richiedente
            };
      
            this.gestScorteServices.getWarehouses(qParamsMagazziniOp).subscribe(( data )=>{
              console.debug( data );
              this.aMagazziniOperatore = data.data.results;
            });
  
      
      
            this.getRichiesteRmaCreate();
            
          } else {
            console.debug('NuovaRichiestaComponent => registraJwtGeocall => KO =>err: ',data.data.DESC_ESITO);
            
            //this.router.navigateByUrl("404");
            this.creaRmaSection.geocallScaduta = true; 
            this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
          }
          
          
          }, ( err )=>{
            console.debug('NuovaRichiestaComponent => registraJwtGeocall => KO =>err: ',err);
            this.creaRmaSection.loaderSessionGeocall            = false;
            //this.router.navigateByUrl("404");
            this.creaRmaSection.geocallScaduta = true;
            this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
           });   

        }, ( err )=>{
            console.debug('NuovaRichiestaComponent => configServiceJwtTokenWind => KO =>err: ',err);
            this.creaRmaSection.loaderSessionGeocall            = false;
            //this.router.navigateByUrl("404");
            this.creaRmaSection.geocallScaduta = true;
            this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
        });      
      

    } else {
      this.styleInterno = false;       // nascondo  INTERNO
      this.styleGeocall = false;      //  mostro GEOCALL
      // forzo redirect
      this.router.navigateByUrl("gestioneScorte/404");
    }






  }

  /* procedura che controlla le richieste jwt */
  getRichiesteRmaCreate():void {
      const qParamsRichiesteJwt = {
        RICHIEDENTE: this.richiedente
      };

      this.gestScorteServices.getRichiesteJwt(qParamsRichiesteJwt).subscribe(( data )=>{

        console.debug('getRichiesteJwt => ',data.data.RMA_CREATE );
        this.creaRmaSection.numeroRmaCreate = parseInt(data.data.RMA_CREATE);

      });
  }


  /* procedura inizializza maps */
  private loadAutoComplete():void {
    const url = 'https://maps.googleapis.com/maps/api/js?libraries=places&v=weekly&client=gme-sirtispa&channel=guiWindV3&signature=5O8ypjMBVY5U_dtcwrhtwV243NY=';
    this.loadScript(url).then((url) => {console.debug('url',url);});
  }

  /* procedura che invoca il lazy load le api di maps */
  private loadScript(url) {
    return new Promise((resolve, reject) => {
      const script = this.renderer2.createElement('script');
      script.type = 'text/javascript';
      script.src = url;
      script.text = ``;
      script.async = true;
      script.defer = true;
      script.onload = resolve;
      script.onerror = reject;
      this.renderer2.appendChild(this.document.head, script);
      this.googleMapsIsDefined = true;

    })
  }

  /* procedura che inizializza l'autocpomplete */
  public initAutocomplete() {

    const inputSearchIndirizzoConsegna = document.getElementById("inputSearchIndirizzoConsegna") as HTMLInputElement;

    const inputSearchIndirizzoOtr = document.getElementById("inputSearchIndirizzoOtr") as HTMLInputElement;

    let focusInput01 = document.getElementById("inputCapConsegna") as HTMLInputElement;


    console.debug('initAutocomplete - 01 -inputSearchIndirizzoConsegna');

    console.debug(document.getElementById("inputSearchIndirizzoConsegna"));

    const autoSearchIndirizzoConsegna = document.getElementById("inputSearchIndirizzoConsegna") ? new google.maps.places.Autocomplete(inputSearchIndirizzoConsegna) : null;

    if (document.getElementById("inputSearchIndirizzoConsegna")) {

      console.debug('initAutocomplete - 02 - inputSearchIndirizzoConsegna');

      autoSearchIndirizzoConsegna.setFields([
        "address_components",
        "geometry",
        "icon",
        "name"
      ]);

      console.debug('initAutocomplete - 03 - inputSearchIndirizzoConsegna');

      autoSearchIndirizzoConsegna.addListener("place_changed", () => {
        this.zeroResultGoogleMaps = false;
        this.loaderAutocompleteMaps = true;
        const place = autoSearchIndirizzoConsegna.getPlace();

        console.debug(place);

        if (!place.geometry) {
          this.zeroResultGoogleMaps = true;
          this.resetCampiAltroIndirizzo(); /* invoco l'apposita procedura */
          this.loaderAutocompleteMaps = false;
          return;
        } else {
          this.zeroResultGoogleMaps = false;
          this.resetCampiAltroIndirizzo(); /* invoco l'apposita procedura */
          let addressValue = "";
          let streetNumberValue = "";

          console.debug(place);
          for (const component of place.address_components) {
            const componentType = component.types[0];

            switch (componentType) {
              case "street_number": {
                streetNumberValue = component.long_name;
                break;
              }

              case "route": {
                addressValue = component.short_name;
                break;
              }

              case "postal_code": {

                  this.reactiveFormCreaRma.get("capConsegnaCheck").setValue(component.long_name, {onlySelf: false});


                break;
              }

              case "locality":

                  this.reactiveFormCreaRma.get("comuneConsegnaCheck").setValue(component.long_name, {onlySelf: false});


                break;

              case "administrative_area_level_1": {

                  this.reactiveFormCreaRma.get("regioneConsegnaCheck").setValue(component.long_name, {onlySelf: false});



                break;
              }
              case "administrative_area_level_2": {

                  this.reactiveFormCreaRma.get("provinciaConsegnaCheck").setValue(component.short_name, {onlySelf: false});



                break;
              }
              case "administrative_area_level_3": {

                  this.reactiveFormCreaRma.get("comuneConsegnaCheck").setValue(component.long_name, {onlySelf: false});


                break;
              }
              case "country":

                  this.reactiveFormCreaRma.get("nazioneConsegnaCheck").setValue(component.long_name, {onlySelf: false});


                break;
            }
          }
          const indirizzoComposto = streetNumberValue !== '' ? addressValue+', '+streetNumberValue : addressValue;

            this.reactiveFormCreaRma.get("indirizzoConsegnaCheck").setValue(indirizzoComposto, {onlySelf: false});


          this.loaderAutocompleteMaps = false;
          inputSearchIndirizzoConsegna.focus();
          focusInput01.focus();

          return;
        }
      });
    }



    console.debug('initAutocomplete - 01 - inputSearchIndirizzoOtr');

    console.debug(document.getElementById("inputSearchIndirizzoOtr"));

    const autoSearchIndirizzoOtr = document.getElementById("inputSearchIndirizzoOtr") ? new google.maps.places.Autocomplete(inputSearchIndirizzoOtr, {
             types: ["geocode"]
         }) : null;


    if (document.getElementById("inputSearchIndirizzoOtr") ) {

      console.debug('initAutocomplete - 02 - inputSearchIndirizzoOtr');


      autoSearchIndirizzoOtr.setFields([
        "address_components",
        "geometry",
        "icon",
        "name"
      ]);


      console.debug('initAutocomplete - 03 - inputSearchIndirizzoOtr');


      const mapOtr = new google.maps.Map(
        document.getElementById("mapOtr") as HTMLElement,
        {
          center: { lat: 41.9027835, lng: 12.496365500000024 },
          zoom: 6,
        }
      );


      this.aMarkersOtr = [];

       console.debug('initAutocomplete - 04 - inputSearchIndirizzoOtr');

       autoSearchIndirizzoOtr.addListener("place_changed", () => {
        this.zeroResultGoogleMaps   = false;
        this.loaderAutocompleteMaps = true;
        this.esitoValidaOtr         = '';
        const place = autoSearchIndirizzoOtr.getPlace();

         inputSearchIndirizzoOtr.focus();

        if (!place.geometry || !place.geometry.location) {
          this.zeroResultGoogleMaps = true;
          this.resetCampiOtr(); /* invoco l'apposita procedura */
          this.loaderAutocompleteMaps = false;
          return;
        } else {
          this.zeroResultGoogleMaps = false;
          this.resetCampiOtr(); /* invoco l'apposita procedura */

          let sNumberExists = false;
          for (const component of place.address_components) {
            const componentType = component.types[0];

            switch (componentType) {
              case "street_number": {
                sNumberExists = true;
                break;
              }
            }
          }

          if (!sNumberExists) {
            this.esitoValidaOtr = 'ERROR_STREET_NUMBER';
            this.loaderAutocompleteMaps = false;
          } else {

            /* richiamo il servizio che esegue la validazione dell'indirizzo */

            const qParamsOtr = {
              SLA                 : 4,
              MAGAZZINO_WIND      : this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE,
              MAGAZZINO_SIRTI     : this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE,
              LAT_OTR             : place.geometry['location'].lat(),
              LONG_OTR            : place.geometry['location'].lng(),
              PART_NUMBER         : this.resultVerificaGiacenza[0].PART_NUMBER_DISPONIBILE,
            };
            console.debug('getValidateOTR => qParamsOtr: ',qParamsOtr );
            this.loaderAutocompleteMaps = true;
            this.gestScorteServices.getValidateOTR(qParamsOtr).subscribe(( data )=>{
              this.esitoValidaOtr       = '';
              this.loaderAutocompleteMaps = false;

              console.debug('getValidateOTR => data: ',data );
              this.esitoValidaOtr = data.data.ESITO;

              /* indipendentemente dall'esito mostro i marker */

              this.reactiveFormCreaRma.get("latitudineOtrCheck").setValue(place.geometry['location'].lat(), {onlySelf: false});
              this.reactiveFormCreaRma.get("longitudineOtrCheck").setValue(place.geometry['location'].lng(), {onlySelf: false});

              const imageOtr = 'assets/maps/icona_otr.png';
              const myLatlngOTR = new google.maps.LatLng(place.geometry['location'].lat(),place.geometry['location'].lng());

              const markerOTR = new google.maps.Marker({
                position  : myLatlngOTR,
                title     : "Indirizzo OTR",
                icon      : imageOtr
              });


              const imagePdp = 'assets/maps/icona_wind.png';
              const myLatlngPdp = new google.maps.LatLng(data.data.LAT_PDP,data.data.LNG_PDP);

              const markerPdp = new google.maps.Marker({
                position  : myLatlngPdp,
                title     : "Magazzino WIND",
                icon      : imagePdp
              });

              const imageSirti = 'assets/maps/icona_sirti.png';
              const myLatlngSirti = new google.maps.LatLng(data.data.LAT_SIRTI,data.data.LNG_SIRTI);

              const markerSirti = new google.maps.Marker({
                position  : myLatlngSirti,
                title     : "Magazzino Sirti di Corrispondenza",
                icon      : imageSirti
              });

              // To add the marker to the map, call setMap();
              if (this.aMarkersOtr && this.aMarkersOtr.length > 0) {
                for (let i = 0; i < this.aMarkersOtr.length; i++) {
                  this.aMarkersOtr[i].setMap(null);
                }
              }

              markerOTR.setMap(mapOtr);
              this.aMarkersOtr.push(markerOTR);

              markerPdp.setMap(mapOtr);
              this.aMarkersOtr.push(markerPdp);

              markerSirti.setMap(mapOtr);
              this.aMarkersOtr.push(markerSirti);


              document.getElementById("inputOtrlongitudine").focus();

              if (data.data.ESITO === 'OK') {
                console.debug('getValidateOTR => OK');

                mapOtr.setCenter(place.geometry.location);
                mapOtr.setZoom(10);

              } else {
                console.debug('getValidateOTR => KO');

                mapOtr.setCenter(place.geometry.location);
                mapOtr.setZoom(6);

              }



            });
          }





        }
      });

    }


  }


  /* ricerca magazzino */
  searchMagazzinoWind: OperatorFunction<string, readonly {SEDE}[]> = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    //filter(term => term.length >= 2),
    map(term => {
        this.resetSezioniRisultatoRicerca(); /* resetto tutto */
        this.zeroResultMateriale    = false;
        this.zeroResultMagazzino    = false;
        this.loaderAutocompleteMagaz  = true;
        if (term.length >= 2) {
         const result = this.aMagazziniOperatore.filter(magazzinoWindObj => new RegExp('^'+term, 'mi').test(magazzinoWindObj.SEDE)).slice(0, 10);
         if (result.length > 0) {
          this.zeroResultMagazzino = false;
         } else {
          this.zeroResultMagazzino = true;
         }
        this.loaderAutocompleteMagaz  = false;
         return result;
        } else {
          if (term.length == 1) {
            this.loaderAutocompleteMagaz   = true;
          } else {
            this.loaderAutocompleteMagaz   = false;
            this.zeroResultMagazzino = true;
          }
          return [];
        }
      }
    ),
  )

  /* seleziona il magazzino  */
  selectedItemMagazzino(itemSelected): void {
    //this.abilitaQtaMatRichiesta = false;
    this.abilitaQtaMatRichiesta = false;
    console.debug('selectedItemMagazzino: ',itemSelected);

  }

 /* trova materiale - invoca il service per gestire l'autocomplete */
 trovaMateriale(term): any {
    this.resetSezioniRisultatoRicerca(); /* resetto tutto */
    /* creo l'obj dei parametri */
    const qParamsMaterialiOp = {
      RICHIEDENTE       : this.richiedente,
      /*
      CODICE_SIRTI      : term,
      PART_NUMBER       : term,
      SAP_CODE          : term,
      NOME_PART_NUMBER  : term,
      */
      MULTI_FILTER      : term
    };
    /* invoco il servizio che mi restituisce i materiali */
    if (term.length >= 4) {
      return this.gestScorteServices.getCatalog(qParamsMaterialiOp).pipe(
          map(response => {
            if (response.data.results.length > 0) {
              this.zeroResultMateriale = false;
              return response.data.results;
            } else {
              this.zeroResultMateriale = true;
              return of([]);
            }
          })
        );
      }
    else {
      this.zeroResultMateriale = true;
      return of([]);
    }
  }

 /* trova la sede gls - invoca il service per gestire l'autocomplete */
 trovaSedeGls(term): any {
    this.resetCampiSedeGls(); /* resetto tutto */
    /* creo l'obj dei parametri */
    const qParamsSedeGls = {
      REGIONE     : this.reactiveFormCercaMateriale.value.magazzinoCheck.REGIONE,
      FILTER      : term
    };
    /* invoco il servizio che le sedi gls */
    if (term.length >= 4) {
      return this.gestScorteServices.getHeadquartersGls(qParamsSedeGls).pipe(
          map(response => {
            if (response.data.results.length > 0) {
              this.zeroResultSedeGls = false;
              return response.data.results;
            } else {
              this.zeroResultSedeGls = true;
              return of([]);
            }
          })
        );
      }
    else {
      this.zeroResultSedeGls = true;
      return of([]);
    }
  }

  /* ricerca materiale invocata dall'autocomplete typehead */
  searchMaterialeWind: any = (text$: Observable<any>) =>
    text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    tap(() => this.loaderAutocompleteMat = true),
    switchMap(term =>
      this.trovaMateriale(term).pipe(
        tap(() => this.zeroResultMateriale = false),
        catchError(() => {
          this.zeroResultMateriale = true;
          return of([]);
        }))
    ),
    tap(() => this.loaderAutocompleteMat = false)
  )

  /* cerca sede gls invocata dall'autocomplete typehead */
  searchSedeGls: any = (text$: Observable<any>) =>
    text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    tap(() => this.loaderAutocompleteSedeGls = true),
    switchMap(term =>
      this.trovaSedeGls(term).pipe(
        tap(() => this.zeroResultSedeGls = false),
        catchError(() => {
          this.zeroResultSedeGls = true;
          return of([]);
        }))
    ),
    tap(() => this.loaderAutocompleteSedeGls = false)
  )

  /* seleziona il materiale */
  selectedItemMateriale(itemSelected): void {
    console.debug('selectedItemMateriale: ',itemSelected);
    this.resetSezioniRisultatoRicerca(); /* resetto tutto */
    this.abilitaQtaMatRichiesta = false;

    /* controllo se e' un materiale nella WIND_PN_GESTIONE_ERICSSON  */
    if ((itemSelected.item.ESISTE_MESSAGGIO) && ((itemSelected.item.ESISTE_MESSAGGIO) === 1 || (itemSelected.item.ESISTE_MESSAGGIO === '1'))) {
      this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
      this.creaRmaSection.tipoMessaggio                   = 'MSG-00'; /* il tipo messaggio e' MSG-03 */
      this.reactiveFormCercaMateriale.get("qtaCheck").setValue(0, { onlySelf: true });
    } else {
      this.reactiveFormCercaMateriale.get("qtaCheck").setValue(1, { onlySelf: true });
      if (itemSelected.item.CLASSIFICATION !== 'CONSUMABLES') {
        this.abilitaQtaMatRichiesta = false;
      } else {
        if (this.richiedente === 'GEOCALL') { /* nei casi di geocall anche il consumabili e 1 non modificabile */
          this.abilitaQtaMatRichiesta = false;
        } else {
          this.abilitaQtaMatRichiesta = true;
        }
      }
    }
  }

  /* seleziona sede gls */
  selectedItemSedeGls(itemSelected): void {
    console.debug('selectedItemSedeGls: ',itemSelected);
    this.reactiveFormCreaRma.get("indirizzoGlsCheck").setValue(itemSelected.item.INDIRIZZO, { onlySelf: true });
    this.reactiveFormCreaRma.get("capGlsCheck").setValue(itemSelected.item.CAP, { onlySelf: true });
    this.reactiveFormCreaRma.get("localitaGlsCheck").setValue(itemSelected.item.LOCALITA, { onlySelf: true });
    this.reactiveFormCreaRma.get("provinciaGlsCheck").setValue(itemSelected.item.PROVINCIA, { onlySelf: true });
    this.reactiveFormCreaRma.get("regioneGlsCheck").setValue(itemSelected.item.REGIONE, { onlySelf: true });
    this.reactiveFormCreaRma.get("telefonoGlsCheck").setValue(itemSelected.item.TELEFONO, { onlySelf: true });
    this.reactiveFormCreaRma.get("faxGlsCheck").setValue(itemSelected.item.FAX, { onlySelf: true });
  }

  /* reset dei campi della sede gls */
  resetCampiSedeGls(): void {
    this.reactiveFormCreaRma.controls['indirizzoGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['capGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['localitaGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['provinciaGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['regioneGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['telefonoGlsCheck'].reset('');
    this.reactiveFormCreaRma.controls['faxGlsCheck'].reset('');
  }

  /* reset dei campi altro indirizzo */
  resetCampiAltroIndirizzo(): void {
    console.debug('resetCampiAltroIndirizzo');
    this.reactiveFormCreaRma.controls['capConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['comuneConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['provinciaConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['regioneConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['nazioneConsegnaCheck'].reset('');
  }

  resetCampiOtr(): void {
    this.reactiveFormCreaRma.controls['latitudineOtrCheck'].reset('');
    this.reactiveFormCreaRma.controls['longitudineOtrCheck'].reset('');
    this.esitoValidaOtr       = '';
    // To add the marker to the map, call setMap();
    if (this.aMarkersOtr && this.aMarkersOtr.length > 0) {
      for (let i = 0; i < this.aMarkersOtr.length; i++) {
        this.aMarkersOtr[i].setMap(null);
      }
    }
  }

  resetCampiAcceleraSla(): void {
    this.reactiveFormCreaRma.controls['slaAcceleraCheck'].reset('');
    this.reactiveFormCreaRma.controls['dataPrevistaConsegnaAcceleraCheck'].reset('');
    this.reactiveFormCreaRma.controls['noteAcceleraCheck'].reset('');
  }

  resetCampiRilassaSla(): void {
    this.reactiveFormCreaRma.controls['slaRilassaCheck'].reset('');
    this.reactiveFormCreaRma.controls['dataPrevistaConsegnaRilassaCheck'].reset('');
    this.reactiveFormCreaRma.controls['noteRilassaCheck'].reset('');
  }

  /* reset delle sezioni nella fase di ricerca materiale */
  resetSezioniRisultatoRicerca(): void {
    console.debug('resetSezioniRisultatoRicerca');
    this.resultVerificaGiacenza                         = null;
    this.creaRmaSection.mostraSezioneCreaRma            = false;
    this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = false;
    this.creaRmaSection.tipoMessaggio                   = null;
    this.creaRmaSection.titoloOperazione                = null;
    this.creaRmaSection.msgSotituitoEquivalente         = null;

    /* altre sezioni custom */

    this.creaRmaSection.mostraSezioneCustom             = false;
    this.creaRmaSection.mostraButtonRilassa             = false;
    this.creaRmaSection.mostraCampiRilassaSla           = false;
    this.creaRmaSection.mostraButtonAccelera            = false;
    this.creaRmaSection.mostraCampiAcceleraSla          = false;
    this.creaRmaSection.mostraOtrAccelera               = false;
    this.creaRmaSection.mostraCampiOtrAccelera          = false;
    this.creaRmaSection.mostraOtr                       = false;
    this.creaRmaSection.mostraCampiOtr                  = false;
    this.creaRmaSection.mostraTipoConsegna              = false;
    this.creaRmaSection.mostraCampiTipoConsegna         = false;

    this.creaRmaSection.dataAppConsMaxDate              = null;
    this.creaRmaSection.dataAppConsMinDate              = null;

    this.creaRmaSection.sDataAppConsMaxDateTime         = null;
    this.creaRmaSection.sDataAppConsMinDateTime         = null;
    this.creaRmaSection.abilitaCreaRma                  = false;
    this.creaRmaSection.loaderCreaRma                   = false;
    this.creaRmaSection.mostraEsitoCreaRma              = false;
    this.creaRmaSection.tipoEsitoCreaRma                = '';
    this.creaRmaSection.msgEsitoCreaRma                 = '';
    this.creaRmaSection.isSwap                          = false;
    this.creaRmaSection.isGiacenza                      = false;
    this.creaRmaSection.isRmaAttesa                     = false;
    
    this.creaRmaSection.geocallScaduta                  = false;
    this.creaRmaSection.loaderSessionGeocall            = false;
    this.creaRmaSection.msgUltimaCreaRmaGeocall         = false;
    



    //this.getRichiesteRmaCreate();




    /* ripulisco il reactive form del crea rma */
    this.reactiveFormCreaRma.controls['ttRemedyCheck'].reset(null);
    this.reactiveFormCreaRma.controls['restituzioneResoPressoCheck'].reset('');
    this.reactiveFormCreaRma.controls['slaOriginaleCheck'].reset('');
    this.reactiveFormCreaRma.controls['dataPrevistaConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['dataConsegnaSuAppuntamentoStdCheck'].reset('');
    this.reactiveFormCreaRma.controls['oraConsegnaSuAppuntamentoStdCheck'].reset('');

    this.reactiveFormCreaRma.controls['noteStandardCheck'].reset('');
    this.reactiveFormCreaRma.controls['tipoConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['sedeGlsCheck'].reset('');
    this.resetCampiSedeGls(); /* invoco l'apposita procedura */
    this.reactiveFormCreaRma.controls['indirizzoConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['telefonoConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['referenteConsegnaCheck'].reset('');
    this.resetCampiAltroIndirizzo(); /* invoco l'apposita procedura */
    this.reactiveFormCreaRma.controls['IndirizzoOtrCheck'].reset('');
    this.resetCampiOtr(); /* invoco l'apposita procedura */
    this.resetCampiAcceleraSla(); /* invoco l'apposita procedura */
    this.resetCampiRilassaSla(); /* invoco l'apposita procedura */



  }

  /* cercaMateriale */
  cercaMateriale(sCriterioRicerca:string): void {

    console.debug('cercaMateriale: ',sCriterioRicerca);
    console.debug('cercaMateriale -> magazzinoCheck: ',this.reactiveFormCercaMateriale.value.magazzinoCheck);
    console.debug('cercaMateriale -> materialeCheck: ',this.reactiveFormCercaMateriale.value.materialeCheck);
    console.debug('cercaMateriale -> qtaCheck: ',this.reactiveFormCercaMateriale.value.qtaCheck);
    this.loaderSearch = true;

    /*reset all sezioni */

    this.resetSezioniRisultatoRicerca();

    /* invoco il servizio */

    /* creo l'obj dei parametri */
    const qParamsCercaMateriale = {
      RICHIEDENTE     : this.richiedente,
      CRITERIO        : sCriterioRicerca,
      MAGAZZINO_DEST  : this.reactiveFormCercaMateriale.value.magazzinoCheck.ID_MAG_SIRTI+''+this.reactiveFormCercaMateriale.value.magazzinoCheck.PROVINCIA,
      PART_NUMBER     : this.reactiveFormCercaMateriale.value.materialeCheck.PART_NUMBER,
      SAP_CODE        : this.reactiveFormCercaMateriale.value.materialeCheck.SAP_CODE,
      QUANTITA        : this.reactiveFormCercaMateriale.value.qtaCheck,
    };

    console.debug('cercaMateriale -> getStock -> qParamsMaterialiOp: ',qParamsCercaMateriale);

    this.gestScorteServices.getStock(qParamsCercaMateriale).subscribe(( data )=>{
      console.debug('cercaMateriale -> getStock -> data: ',data);

      /* verifico se la ricerca eseguita ritorna dei risultati */
      if ((!data.data.ESITO) && (data.data.results.length > 0)) {

        this.resultVerificaGiacenza = data.data.results;

        let sCriterioVerificato = this.resultVerificaGiacenza[0].CRITERIO.toUpperCase();

        console.debug('cercaMateriale -> CRITERIO verificato: ',sCriterioVerificato);

        /* caso START - Inizio */

        if (((sCriterioRicerca.toUpperCase() === 'START') || (sCriterioRicerca.toUpperCase() === 'EQUIVALENTE ALTRI PDP')
             || (sCriterioRicerca.toUpperCase() === 'RICERCA IN SIRTI') || (sCriterioRicerca.toUpperCase() === 'RICERCA COMPATIBILI')
             || (sCriterioRicerca.toUpperCase() === 'RICERCA SAP_CODE'))
            && (sCriterioVerificato === sCriterioRicerca.toUpperCase())) {

          //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.enable();
          this.aOptionValueResoPresso = [];
          //this.resultVerificaGiacenza = resultVerificaGiacenzaCaso3;

          console.debug('cercaMateriale -> sono nel blocco dei casi uso');


          /* verifico se sono nel caso 1 - stesso materiale nel magazzino wind richiedente */
          if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'IDENTICO') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() === this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 1 - identico nel magazzino wind richiedente: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' = '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = 'PRENOTAZIONE';
            this.creaRmaSection.mostraSezioneCreaRma            = true; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-01'; /* il tipo messaggio e' MSG-01 */
            if (this.richiedente === 'GEOCALL') {
              //this.reactiveFormCreaRma.get("ttRemedyCheck").setValue(this.workOrderKey, { onlySelf: true });
              this.reactiveFormCreaRma.get("ttRemedyCheck").setValue(this.workOrderKey, { onlySelf: true });
              
            }
            this.creaRmaSection.titoloOperazione                = ''+this.resultVerificaGiacenza[0].PART_NUMBER_RICHIESTO.toUpperCase()+' a '+this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE.toUpperCase();
            this.reactiveFormCreaRma.get("slaOriginaleCheck").setValue(this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR, { onlySelf: true });
            this.reactiveFormCreaRma.get("dataPrevistaConsegnaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA, { onlySelf: true });
            /* se il materiale e' consumabile, forzo reso presso a a SEDE WIND non modificabile */
            if (this.resultVerificaGiacenza[0].CONSUMABILE.toUpperCase() === 'SI') {
              this.aOptionValueResoPresso = ['SEDE WIND'];
              this.reactiveFormCreaRma.get("restituzioneResoPressoCheck").setValue(this.aOptionValueResoPresso[0], {onlySelf: true});

              //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.disable();
            } else {
              this.aOptionValueResoPresso = ['SEDE WIND','PUNTO GLS'];
              //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.enable();
            }

            /*gestisco le sezioni custom OTR,rilassa,Accelera e tipoConsegna */
            //this.gestisciSezioniCustom('STANDARD'); /* fixme da capire */

            this.loaderSearch = false;
            this.creaRmaSection.abilitaCreaRma = true;
          } /* caso 1  - fine */
          /* verifico se sono nel caso 2 - equivalente nel magazzino wind richiedente */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'EQUIVALENTE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() === this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 2 - equivalente nel magazzino wind richiedente: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' == '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());

            /* devo forzare il cambio del materiale prima di mostrare tutti i messaggi */

            this.loaderSearch = true;
            const qParamsMaterialiOp = {
              RICHIEDENTE       : this.richiedente,
              PART_NUMBER       : this.resultVerificaGiacenza[0].PART_NUMBER_DISPONIBILE,
              SAP_CODE          : this.resultVerificaGiacenza[0].SAP_CODE_DISPONIBILE,
            };

            this.gestScorteServices.getCatalog(qParamsMaterialiOp).subscribe(( data )=>{
              console.debug('cercaMateriale -> getCatalog -> data: ',data);
              if (data.data.results.length === 1) { /* se trovato */
                this.reactiveFormCercaMateriale.controls.materialeCheck.setValue(data.data.results[0], {emitEvent:false}); /* sostituisco esistente */
                this.creaRmaSection.tipoOperazione                  = 'PRENOTAZIONE';
                this.creaRmaSection.msgSotituitoEquivalente         = 'Sostituito Part Number '+this.resultVerificaGiacenza[0].PART_NUMBER_RICHIESTO+' con Part Number equivalente';
                this.creaRmaSection.mostraSezioneCreaRma            = true; /* mostro la sezione di crea rma dei dati principali */
                this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
                this.creaRmaSection.tipoMessaggio                   = 'MSG-02'; /* il tipo messaggio e' MSG-02*/
                this.creaRmaSection.titoloOperazione                = ''+this.reactiveFormCercaMateriale.value.materialeCheck.PART_NUMBER+' a '+this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE.toUpperCase();
                
                if (this.richiedente === 'GEOCALL') {
                  this.reactiveFormCreaRma.get("ttRemedyCheck").setValue(this.workOrderKey, { onlySelf: true });
                  
                }                
                
                this.reactiveFormCreaRma.get("slaOriginaleCheck").setValue(this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR, { onlySelf: true });
                this.reactiveFormCreaRma.get("dataPrevistaConsegnaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA, { onlySelf: true });
                /* se il materiale e' consumabile, forzo reso presso a a SEDE WIND non modificabile */
                if (this.resultVerificaGiacenza[0].CONSUMABILE.toUpperCase() === 'SI') {
                  this.aOptionValueResoPresso = ['SEDE WIND'];
                  this.reactiveFormCreaRma.get("restituzioneResoPressoCheck").setValue(this.aOptionValueResoPresso[0], {onlySelf: true});

                  //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.disable();
                } else {
                  this.aOptionValueResoPresso = ['SEDE WIND','PUNTO GLS'];
                  //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.enable();
                }


                /*gestisco le sezioni custom OTR,rilassa,Accelera e tipoConsegna */
                this.gestisciSezioniCustom('STANDARD');

                this.loaderSearch = false;
                this.creaRmaSection.abilitaCreaRma = true;



              } else {
                this.loaderSearch = false;
              }

            });
          } /* caso 2  - fine */

          /* verifico se sono nel caso 2a - compatibile nel magazzino wind richiedente */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'COMPATIBILE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() === this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 2a - compatibile nel magazzino wind richiedente: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' == '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-02A'; /* il tipo messaggio e' MSG-02A*/
            this.loaderSearch = false;
          } /* caso 2a  - fine */
          /* verifico se sono nel caso 3 - stesso materiale in altro magazzino wind visibile operatore*/
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'IDENTICO') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 3 - identico in altro wind operatore: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-03'; /* il tipo messaggio e' MSG-03 */
            this.loaderSearch = false;
          } /* caso 3  - fine */
          /* verifico se sono nel caso 4 - equivalente in altro magazzino wind visibile operatore */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'EQUIVALENTE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 4 - equivalente in altro wind operatore: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-04'; /* il tipo messaggio e' MSG-04 */
            this.loaderSearch = false;
          } /* caso 4  - fine */
          /* verifico se sono nel caso 5 o 12 - stesso materiale in sirti corrispondenza */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'IDENTICO') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('2')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 5 o 12 - identico  in sirti corrispondenza o Sirti piu vicino: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = 'SPEDIZIONE';
            this.creaRmaSection.mostraSezioneCreaRma            = true; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-05/12'; /* il tipo messaggio e' MSG-05 */
            this.creaRmaSection.titoloOperazione                = ''+this.resultVerificaGiacenza[0].PART_NUMBER_RICHIESTO.toUpperCase()+' a '+this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE.toUpperCase();
            
            if (this.richiedente === 'GEOCALL') {
              this.reactiveFormCreaRma.get("ttRemedyCheck").setValue(this.workOrderKey, { onlySelf: true });
              
            }            

            this.reactiveFormCreaRma.get("slaOriginaleCheck").setValue(this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR, { onlySelf: true });
            this.reactiveFormCreaRma.get("dataPrevistaConsegnaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA, { onlySelf: true });
            /* se il materiale e' consumabile, forzo reso presso a a SEDE WIND non modificabile */
            if (this.resultVerificaGiacenza[0].CONSUMABILE.toUpperCase() === 'SI') {
              this.aOptionValueResoPresso = ['SEDE WIND'];
              this.reactiveFormCreaRma.get("restituzioneResoPressoCheck").setValue(this.aOptionValueResoPresso[0], {onlySelf: true});

              //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.disable();
            } else {
              this.aOptionValueResoPresso = ['SEDE WIND','PUNTO GLS'];
              //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.enable();
            }

            /*gestisco le sezioni custom OTR,rilassa,Accelera e tipoConsegna */
            this.gestisciSezioniCustom('STANDARD');



            this.loaderSearch = false;
            this.creaRmaSection.abilitaCreaRma = true;
          } /* caso 5 o 12  - fine */
          /* verifico se sono nel caso 6 o 7  - equivalente nel magazzino Sirti di corrispondenza o Sirti piu vicino */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'EQUIVALENTE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('2')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 6 o 7 - equivalente nel magazzino Sirti di corrispondenza o Sirti piu vicino: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());

            /* devo forzare il cambio del materiale prima di mostrare tutti i messaggi */

            this.loaderSearch = true;
            const qParamsMaterialiOp = {
              RICHIEDENTE       : this.richiedente,
              PART_NUMBER       : this.resultVerificaGiacenza[0].PART_NUMBER_DISPONIBILE,
              SAP_CODE          : this.resultVerificaGiacenza[0].SAP_CODE_DISPONIBILE,
            };

            this.gestScorteServices.getCatalog(qParamsMaterialiOp).subscribe(( data )=>{
              console.debug('cercaMateriale -> getCatalog -> data: ',data);
              if (data.data.results.length === 1) { /* se trovato */
                this.reactiveFormCercaMateriale.controls.materialeCheck.setValue(data.data.results[0], {emitEvent:false}); /* sostituisco esistente */
                this.creaRmaSection.tipoOperazione                  = 'SPEDIZIONE';
                this.creaRmaSection.msgSotituitoEquivalente         = 'Sostituito Part Number '+this.resultVerificaGiacenza[0].PART_NUMBER_RICHIESTO+' con Part Number equivalente';
                this.creaRmaSection.mostraSezioneCreaRma            = true; /* mostro la sezione di crea rma dei dati principali */
                this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
                this.creaRmaSection.tipoMessaggio                   = 'MSG-06/07'; /* il tipo messaggio e' MSG-06/07*/
                this.creaRmaSection.titoloOperazione                = ''+this.reactiveFormCercaMateriale.value.materialeCheck.PART_NUMBER+' a '+this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE.toUpperCase();
                
                if (this.richiedente === 'GEOCALL') {
                  this.reactiveFormCreaRma.get("ttRemedyCheck").setValue(this.workOrderKey, { onlySelf: true });
                  
                }                
                
                
                this.reactiveFormCreaRma.get("slaOriginaleCheck").setValue(this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR, { onlySelf: true });
                this.reactiveFormCreaRma.get("dataPrevistaConsegnaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA, { onlySelf: true });
                /* se il materiale e' consumabile, forzo reso presso a a SEDE WIND non modificabile */
                if (this.resultVerificaGiacenza[0].CONSUMABILE.toUpperCase() === 'SI') {
                  this.aOptionValueResoPresso = ['SEDE WIND'];
                  this.reactiveFormCreaRma.get("restituzioneResoPressoCheck").setValue(this.aOptionValueResoPresso[0], {onlySelf: true});

                  //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.disable();
                } else {
                  this.aOptionValueResoPresso = ['SEDE WIND','PUNTO GLS'];
                  //this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.enable();
                }



                /*gestisco le sezioni custom OTR,rilassa,Accelera e tipoConsegna */
                this.gestisciSezioniCustom('STANDARD');



                this.loaderSearch = false;
                this.creaRmaSection.abilitaCreaRma = true;
              } else {
                this.loaderSearch = false;
              }

            });

          } /* caso 6 o 7 - fine */
          /* verifico se sono nel caso 8 - compatibile in altro magazzino wind visibile operatore */
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'COMPATIBILE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('1')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 8 - compatibile in altro magazzino wind visibile operatore: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-08/09/10'; /* il tipo messaggio e' MSG-08/09/10*/
            this.loaderSearch = false;

          } /* caso 8 - fine */
          /* verifico se sono nel caso 9 o 10 - compatibile nel magazzino Sirti di corrispondenza o Sirti piu vicino*/
          else if ((this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'COMPATIBILE') && (this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.startsWith('2')) && (this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase() !== this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase())) {
            console.debug('cercaMateriale -> CASO 9 e 10 - compatibile nel magazzino Sirti di corrispondenza o Sirti piu vicino: ',this.resultVerificaGiacenza[0].SIGLA_PDP_RICHIEDENTE.toUpperCase()+' != '+this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE.toUpperCase());
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-08/09/10'; /* il tipo messaggio e' MSG-08/09/10*/
            this.loaderSearch = false;
          } /* caso 9 o 10 - fine */
          /* verifico se sono nel caso 11 - Ricerca per stesso SAP CODE, ma diverso Part Number*/
          else if (this.resultVerificaGiacenza[0].TIPO_MAT_TROVATO === 'SAP_CODE') {
            console.debug('cercaMateriale -> CASO 11 - Ricerca per stesso SAP CODE, ma diverso Part Number: ');
            this.creaRmaSection.tipoOperazione                  = '';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-11'; /* il tipo messaggio e' MSG-11*/
            this.loaderSearch = false;

          } /* caso 11 - fine */
          else {
            console.debug('cercaMateriale -> Nessun dei casi trovati');
          }
        } /* caso START - Fine */

        else {
          /* controllo sempre se sono arrivato alla fine , in modo da gestire le RMA attesa scorta */
          if ( sCriterioRicerca.toUpperCase() === 'RICERCA SAP_CODE') {
            console.debug('cercaMateriale -> Ricerca Conclusa 1 - Nessun dei casi trovati - devo gestire le RMA attesa scorta');
            this.creaRmaSection.tipoOperazione                  = 'CREA-RMA-ATTESA';
            this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
            this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
            this.creaRmaSection.tipoMessaggio                   = 'MSG-CREA-RMA-ATTESA'; /* il tipo messaggio e' MSG-CREA-RMA-ATTESA*/
            this.loaderSearch = false;
            this.creaRmaSection.isSwap        = (data.data.SWAP && data.data.SWAP === 'SI') ? true : false;
            this.creaRmaSection.isGiacenza    = (data.data.GIACENZA && data.data.GIACENZA === 'SI') ? true : false;
            this.creaRmaSection.isRmaAttesa   = (data.data.RMA_ATTESA && data.data.RMA_ATTESA === 'SI') ? true : false;
          } else {
            this.proseguiRicercaMateriale(sCriterioRicerca); /* proseguo la ricerca */
          }

        }

      } else {
        /* controllo sempre se sono arrivato alla fine , in modo da gestire le RMA attesa scorta */
        if ( sCriterioRicerca.toUpperCase() === 'RICERCA SAP_CODE') {
          console.debug('cercaMateriale -> Ricerca Conclusa 2 - Nessun dei casi trovati - devo gestire le RMA attesa scorta');
          this.creaRmaSection.tipoOperazione                  = 'CREA-RMA-ATTESA';
          this.creaRmaSection.mostraSezioneCreaRma            = false; /* mostro la sezione di crea rma dei dati principali */
          this.creaRmaSection.mostraMsgEsitoRicercaMateriale  = true; /* mostro il messaggio */
          this.creaRmaSection.tipoMessaggio                   = 'MSG-CREA-RMA-ATTESA'; /* il tipo messaggio e' MSG-CREA-RMA-ATTESA*/
          this.loaderSearch = false;
          this.creaRmaSection.isSwap        = (data.data.SWAP && data.data.SWAP === 'SI') ? true : false;
          this.creaRmaSection.isGiacenza    = (data.data.GIACENZA && data.data.GIACENZA === 'SI') ? true : false;
          this.creaRmaSection.isRmaAttesa   = (data.data.RMA_ATTESA && data.data.RMA_ATTESA === 'SI') ? true : false;
        } else {
          this.proseguiRicercaMateriale(sCriterioRicerca); /* proseguo la ricerca */
        }
      }


    });


  }

  /* cambiaMagazzinoAndMaterialeRicerca per caso 4*/
  cambiaMagazzinoAndMaterialeRicerca(sSedeNuovoMagazzino:string,sSiglaNuovoMagazzino:string,sNuovoPartNumber:string,sNuovoSapCode:string,sCriterioAttuale:string): void {
    console.debug('cambiaMagazzinoAndMaterialeRicerca -> sCriterioAttuale: ',sCriterioAttuale);
    console.debug('cambiaMagazzinoAndMaterialeRicerca -> sSedeNuovoMagazzino: ',sSedeNuovoMagazzino);
    console.debug('cambiaMagazzinoAndMaterialeRicerca -> sSiglaNuovoMagazzino: ',sSiglaNuovoMagazzino);
    console.debug('cambiaMagazzinoAndMaterialeRicerca -> sNuovoPartNumber: ',sNuovoPartNumber);
    console.debug('cambiaMagazzinoAndMaterialeRicerca -> sNuovoSapCode: ',sNuovoSapCode);

    this.loaderSearch = true;


    /* recupero i dati del materiale completo dal servizio dedicato */

    this.loaderSearch = true;
    const qParamsMaterialiOp = {
      RICHIEDENTE       : this.richiedente,
      PART_NUMBER       : sNuovoPartNumber,
      SAP_CODE          : sNuovoSapCode,
    };

    this.gestScorteServices.getCatalog(qParamsMaterialiOp).subscribe(( data )=>{
      console.debug('cambiaMagazzinoAndMaterialeRicerca -> getCatalog -> data: ',data);
      if (data.data.results.length === 1) { /* se trovato */
        this.reactiveFormCercaMateriale.controls.materialeCheck.setValue(data.data.results[0], {emitEvent:false}); /* sostituisco esistente */

        /* sostituisco il magazzino leggendolo da quelli del service */
        const nuovoMagazzinoSelezionato = this.aMagazziniOperatore.filter(magazzinoWindObj => new RegExp('^'+sSedeNuovoMagazzino, 'mi').test(magazzinoWindObj.SEDE)).slice(0, 10);
        console.debug('cambiaMagazzinoAndMaterialeRicerca -> nuovoMagazzinoSelezionato: ',nuovoMagazzinoSelezionato[0]);
        this.reactiveFormCercaMateriale.controls.magazzinoCheck.setValue(nuovoMagazzinoSelezionato[0], {emitEvent:false});

        this.cercaMateriale(sCriterioAttuale); /* eseguo la ricerca da ZERO */
      } else {
         this.loaderSearch = false;
      }
    });

  }

  /* cambiaMagazzinoRicerca per caso 3*/
  cambiaMagazzinoRicerca(sSedeNuovoMagazzino:string,sSiglaNuovoMagazzino:string,sCriterioAttuale:string): void {
    console.debug('cambiaMagazzinoRicerca -> sCriterioAttuale: ',sCriterioAttuale);
    console.debug('cambiaMagazzinoRicerca -> sSedeNuovoMagazzino: ',sSedeNuovoMagazzino);
    console.debug('cambiaMagazzinoRicerca -> sSiglaNuovoMagazzino: ',sSiglaNuovoMagazzino);

    /* sostituisco il magazzino leggendolo da quelli del service */
    const nuovoMagazzinoSelezionato = this.aMagazziniOperatore.filter(magazzinoWindObj => new RegExp('^'+sSedeNuovoMagazzino, 'mi').test(magazzinoWindObj.SEDE)).slice(0, 10);
    console.debug('cambiaMagazzinoRicerca -> nuovoMagazzinoSelezionato: ',nuovoMagazzinoSelezionato[0]);
    this.reactiveFormCercaMateriale.controls.magazzinoCheck.setValue(nuovoMagazzinoSelezionato[0], {emitEvent:false});
    this.cercaMateriale(sCriterioAttuale);

  }

  /* cambiaMaterialeRicerca per caso 2A */
  cambiaMaterialeRicerca(sNuovoPartNumber:string,sNuovoSapCode:string,sCriterioAttuale:string): void {
    console.debug('cambiaMaterialeRicerca -> sCriterioAttuale: ',sCriterioAttuale);
    console.debug('cambiaMaterialeRicerca -> sNuovoPartNumber: ',sNuovoPartNumber);
    console.debug('cambiaMaterialeRicerca -> sNuovoSapCode: ',sNuovoSapCode);

    /* recupero i dati del materiale completo dal servizio dedicato */

    this.loaderSearch = true;
    const qParamsMaterialiOp = {
      RICHIEDENTE       : this.richiedente,
      PART_NUMBER       : sNuovoPartNumber,
      SAP_CODE          : sNuovoSapCode,
    };

    this.gestScorteServices.getCatalog(qParamsMaterialiOp).subscribe(( data )=>{
      console.debug('cambiaMaterialeRicerca -> getCatalog -> data: ',data);
      if (data.data.results.length === 1) { /* se trovato */
        this.reactiveFormCercaMateriale.controls.materialeCheck.setValue(data.data.results[0], {emitEvent:false}); /* sostituisco esistente */
        this.cercaMateriale(sCriterioAttuale); /* eseguo la ricerca da ZERO */
      } else {
         this.loaderSearch = false;
      }

    });

  }

  /* proseguiRicercaMateriale */
  proseguiRicercaMateriale(sCriterioAttuale:string): void {
    this.loaderSearch = true;
    console.debug('proseguiRicercaMateriale -> sCriterioAttuale: ',sCriterioAttuale);
    /* in base al criterio attuale, decido quello successivo da applicare alla ricerca */
    if ( sCriterioAttuale.toUpperCase() === 'START') {
      console.debug('proseguiRicercaMateriale -> Nuovo Criterio: EQUIVALENTE ALTRI PDP');
      this.cercaMateriale('EQUIVALENTE ALTRI PDP');
    } else if ( sCriterioAttuale.toUpperCase() === 'EQUIVALENTE ALTRI PDP') {
      console.debug('proseguiRicercaMateriale -> Nuovo Criterio: RICERCA IN SIRTI	');
      this.cercaMateriale('RICERCA IN SIRTI');
    } else if ( sCriterioAttuale.toUpperCase() === 'RICERCA IN SIRTI') {
      console.debug('proseguiRicercaMateriale -> Nuovo Criterio: RICERCA COMPATIBILI');
      this.cercaMateriale('RICERCA COMPATIBILI');
    } else if ( sCriterioAttuale.toUpperCase() === 'RICERCA COMPATIBILI') {
      console.debug('proseguiRicercaMateriale -> Nuovo Criterio: RICERCA SAP_CODE');
      this.cercaMateriale('RICERCA SAP_CODE');
    } else {
      console.debug('proseguiRicercaMateriale -> Nessun Materiale trovato in tutte le ricerche');
    }

  }

  /* gestisciSezioniCustom */
  gestisciSezioniCustom(sChiamante:string): void {

    const sSla:string           = this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR;
    const sTipoMagazzino:string = this.resultVerificaGiacenza[0].TIPO_MAGAZZINO_SIRTI;

    console.debug('gestisciSezioniCustom -> sSla',sSla);
    console.debug('gestisciSezioniCustom -> sTipoMagazzino',sTipoMagazzino);

    this.creaRmaSection.mostraSezioneCustom             = true;
    /* resetto sempre tutto e a fronte delle varie selezioni riabilito */
    this.creaRmaSection.mostraButtonRilassa             = false;
    this.creaRmaSection.mostraCampiRilassaSla           = false;
    this.creaRmaSection.mostraButtonAccelera            = false;
    this.creaRmaSection.mostraCampiAcceleraSla          = false;
    this.creaRmaSection.mostraOtrAccelera               = false;
    this.creaRmaSection.mostraCampiOtrAccelera          = false;
    this.creaRmaSection.mostraOtr                       = false;
    this.creaRmaSection.mostraCampiOtr                  = false;
    this.creaRmaSection.mostraTipoConsegna              = false;
    this.creaRmaSection.mostraCampiTipoConsegna         = false;
    this.reactiveFormCreaRma.controls['sedeGlsCheck'].reset('');
    this.resetCampiSedeGls(); /* invoco l'apposita procedura */
    this.reactiveFormCreaRma.controls['indirizzoConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['telefonoConsegnaCheck'].reset('');
    this.reactiveFormCreaRma.controls['referenteConsegnaCheck'].reset('');
    this.resetCampiAltroIndirizzo(); /* invoco l'apposita procedura */
    this.reactiveFormCreaRma.controls['IndirizzoOtrCheck'].reset('');
    this.resetCampiOtr(); /* invoco l'apposita procedura */
    this.resetCampiAcceleraSla(); /* invoco l'apposita procedura */
    this.resetCampiRilassaSla(); /* invoco l'apposita procedura */



    /* gestisco la data appuntamento consegna MAX */
    console.debug('Split MAX: ', this.resultVerificaGiacenza[0].DATA_APPUNTAMENTO_CONSEG_MAX.split(' '));
    const aDataAppConsMaxFull = this.resultVerificaGiacenza[0].DATA_APPUNTAMENTO_CONSEG_MAX.split(' ');
    const aDataAppConsMaxDate = aDataAppConsMaxFull[0].split('/');
    console.debug('Split MAX: ', aDataAppConsMaxDate);
    const aDataAppConsMaxTime = aDataAppConsMaxFull[1].split(':');
    console.debug('Split MAX: ', aDataAppConsMaxTime);


    this.creaRmaSection.dataAppConsMaxDate      = {year: parseInt(aDataAppConsMaxDate[2]), month: parseInt(aDataAppConsMaxDate[1]), day: parseInt(aDataAppConsMaxDate[0])};
    this.creaRmaSection.sDataAppConsMaxDateTime = this.resultVerificaGiacenza[0].DATA_APPUNTAMENTO_CONSEG_MAX;



    console.debug('Split MAX: ', this.creaRmaSection.dataAppConsMaxDate);

    if (sSla==='4') { /* se lo sla 4 ore nativo */
      if (sTipoMagazzino.toUpperCase() === 'CORRISPONDENZA') { /* sono in sirti corrispondenza */

        if (sChiamante.toUpperCase() === 'STANDARD') { /* di default */
          /* il tipo consegna e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});
          this.creaRmaSection.mostraOtr = true; /* mostro OTR */
          this.creaRmaSection.mostraButtonRilassa = true; /* mostro il rilassa sla*/

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate      = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);


        } else if (sChiamante.toUpperCase() === 'RILASSA SLA') {
          this.creaRmaSection.mostraCampiRilassaSla  = true; /* mostro solo i suo campi da editare */

          this.reactiveFormCreaRma.get("slaRilassaCheck").setValue('24', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaRilassaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO, {onlySelf: true});

          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro il tipo consegna */


          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);



        } else if (sChiamante.toUpperCase() === 'TIPO CONSEGNA') {
          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro il tipo consegna */
          this.creaRmaSection.mostraCampiRilassaSla  = true; /* mostro i campi del rilassa sla */

          this.reactiveFormCreaRma.get("slaRilassaCheck").setValue('24', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaRilassaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO, {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO;

          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

          if (this.reactiveFormCreaRma.value.tipoConsegnaCheck.toUpperCase() !=='PDP') { /* solo nei casi != PDP */
            this.creaRmaSection.mostraCampiTipoConsegna = true; /* mostro solo i suo campi da editare */
          }

        } else if (sChiamante.toUpperCase() === 'OTR') { /* sono dentro otr */
          /* il tipo consegna e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});
          this.creaRmaSection.mostraCampiOtr  = true; /* mostro solo i suo campi da editare */

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        }
      }
      else if (sTipoMagazzino.toUpperCase() === 'ALTRI_SIRTI') { /* sono in altri sirti */
        if (sChiamante.toUpperCase() === 'STANDARD') { /* di default */
          this.creaRmaSection.mostraButtonRilassa = true;
          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        } else if (sChiamante.toUpperCase() === 'RILASSA SLA') {
          this.creaRmaSection.mostraCampiRilassaSla  = true; /* mostro solo i suo campi da editare */

          this.reactiveFormCreaRma.get("slaRilassaCheck").setValue('24', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaRilassaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO, {onlySelf: true});

          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro il tipo consegna */

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        } else if (sChiamante.toUpperCase() === 'TIPO CONSEGNA') {
          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro il tipo consegna */
          this.creaRmaSection.mostraCampiRilassaSla  = true; /* mostro i campi del rilassa sla */

          this.reactiveFormCreaRma.get("slaRilassaCheck").setValue('24', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaRilassaCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO, {onlySelf: true});

          if (this.reactiveFormCreaRma.value.tipoConsegnaCheck.toUpperCase() !=='PDP') { /* solo nei casi != PDP */
            this.creaRmaSection.mostraCampiTipoConsegna = true; /* mostro solo i suo campi da editare */
          }

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};

          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_RILASSATO;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        }


      }
    } else if (sSla==='24') { /* se lo sla 24 ore nativo */
      if (sTipoMagazzino.toUpperCase() === 'CORRISPONDENZA') { /* in corrispondenza */
         if (sChiamante.toUpperCase() === 'STANDARD') { /* di default */
          this.creaRmaSection.mostraButtonAccelera  = true; /* mostro accelera */
          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro tipo consegna */
          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        } else if (sChiamante.toUpperCase() === 'ACCELERA SLA') { /* dentro accelera SLA */
          this.creaRmaSection.mostraCampiAcceleraSla  = true; /* mostro i suoi campi edit */
          this.creaRmaSection.mostraOtrAccelera = true; /* mostro otr acellerato */

          this.reactiveFormCreaRma.get("slaAcceleraCheck").setValue('4', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaAcceleraCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO, {onlySelf: true});

          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});


          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO;



          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);


        } else if (sChiamante.toUpperCase() === 'OTR ACCELERA SLA') {
          //this.creaRmaSection.mostraOtrAccelera = true; /* mostro otr acellerato */
          this.creaRmaSection.mostraCampiAcceleraSla  = true; /* mostro i campi edit di accelera sla */
          this.reactiveFormCreaRma.get("slaAcceleraCheck").setValue('4', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaAcceleraCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO, {onlySelf: true});
          this.creaRmaSection.mostraCampiOtrAccelera  = true; /* mostro i suoi campi edit */
          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);


        } else if (sChiamante.toUpperCase() === 'TIPO CONSEGNA') { /* se uso il tipo consegna */
          this.creaRmaSection.mostraTipoConsegna    = true /* continuo a mostrarlo */
          this.creaRmaSection.mostraButtonAccelera  = true; /* mostro accelera */
          if (this.reactiveFormCreaRma.value.tipoConsegnaCheck.toUpperCase() !=='PDP') {
            this.creaRmaSection.mostraCampiTipoConsegna = true; /* mostro solo i suo campi da editare */
          }

        }
      }
      else if (sTipoMagazzino.toUpperCase() === 'ALTRI_SIRTI') { /* in altri sirti  */
        if (sChiamante.toUpperCase() === 'STANDARD') {
          this.creaRmaSection.mostraButtonAccelera  = true; /* mostro accelera */
          this.creaRmaSection.mostraTipoConsegna    = true; /* mostro tipo consegna */
          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};

          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;

          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);


        } else if (sChiamante.toUpperCase() === 'ACCELERA SLA') { /* dentro accelera SLA */
          this.creaRmaSection.mostraCampiAcceleraSla  = true;  /* mostro i suoi campi edit */
          this.reactiveFormCreaRma.get("slaAcceleraCheck").setValue('4', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaAcceleraCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO, {onlySelf: true});


          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});
        } else if (sChiamante.toUpperCase() === 'TIPO CONSEGNA') {/* se uso il tipo consegna */
          this.creaRmaSection.mostraTipoConsegna    = true /* continuo a mostrarlo */
          this.creaRmaSection.mostraButtonAccelera  = true; /* mostro accelera */
          if (this.reactiveFormCreaRma.value.tipoConsegnaCheck.toUpperCase() !=='PDP') { /* solo nei casi != PDP */
            this.creaRmaSection.mostraCampiTipoConsegna = true; /* mostro solo i suo campi da editare */
          }

        }
      } else if (sTipoMagazzino.toUpperCase() === 'PDP') {
        console.debug('DEVO GESTIRE QUESTO CASO E FAR USCIRE ACCELERA');
         if (sChiamante.toUpperCase() === 'STANDARD') { /* di default */
          this.creaRmaSection.mostraButtonAccelera  = true; /* mostro accelera */

          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});

          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREVISTA_CONSEGNA_SLA;


          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);

        } else if (sChiamante.toUpperCase() === 'ACCELERA SLA') { /* dentro accelera SLA */
          this.creaRmaSection.mostraCampiAcceleraSla  = true; /* mostro i suoi campi edit */


          this.reactiveFormCreaRma.get("slaAcceleraCheck").setValue('4', {onlySelf: true});
          this.reactiveFormCreaRma.get("dataPrevistaConsegnaAcceleraCheck").setValue(this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO, {onlySelf: true});

          /* il tipo consegna di default e' sempre PDP */
          this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
          this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});


          /* gestisco la data appuntamento consegna  MIN in base al chiamante */
          console.debug('Split MIN: ', this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' '));
          const aDataAppConsMinFull = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO.split(' ');
          const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
          console.debug('Split MIN: ', aDataAppConsMinDate);
          const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
          console.debug('Split MIN: ', aDataAppConsMinTime);

          this.creaRmaSection.dataAppConsMinDate = {year: parseInt(aDataAppConsMinDate[2]), month: parseInt(aDataAppConsMinDate[1]), day: parseInt(aDataAppConsMinDate[0])};
          this.creaRmaSection.sDataAppConsMinDateTime = this.resultVerificaGiacenza[0].DATA_PREV_CONS_SLA_ACCELERATO;



          console.debug('Split MIN: ', this.creaRmaSection.dataAppConsMinDate);


        }
      }
    } else {
      /* il tipo consegna di default e' sempre PDP */
      this.aOptionValueTipoConsegna               = ['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO'];
      this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(this.aOptionValueTipoConsegna[0], {onlySelf: true});



    }
  }
  
  
  chiudiSessioneGeocall()  : void {
    console.debug('chiudiSessioneGeocall');

    
    let qParamsGeocall = {
        RICHIEDENTE : this.richiedente,
      };
    
    this.gestScorteServices.checkValiditaJwtGeocall(qParamsGeocall).subscribe(( dataCheck )=>{
      
      if (dataCheck.data.ESITO === 'KO') {
        this.creaRmaSection.geocallScaduta = true;
        
      } else {
        this.gestScorteServices.terminaValiditaJwtGeocall(qParamsGeocall).subscribe(( dataChiudi )=>{
          
        this.creaRmaSection.geocallScaduta = true;         
          
          
        });      
      }

      
    });
    
  }

  /* creaRmaAttesa */
  creaRmaAttesa(sChiamante): void {
    console.debug('creaRmaAttesa -> value: ',this.reactiveFormCreaRma.value);
    this.creaRmaSection.loaderCreaRma           = true;
    this.creaRmaSection.mostraEsitoCreaRma      = false;
    this.creaRmaSection.tipoEsitoCreaRma        = '';
    this.creaRmaSection.msgEsitoCreaRma         = '';
    this.creaRmaSection.msgUltimaCreaRmaGeocall = false;
    let qParamsCreaRma = {};

    let paramsCreaRmaRichiesteCompletate;
    let paramsJwtTokenGeocall;
    let paramsTTRemedy;

    if (this.richiedente === 'GEOCALL') {
      paramsCreaRmaRichiesteCompletate = sChiamante === 'ULTIMA' ? 'SI':'NO';
      paramsJwtTokenGeocall = this.jwtTokenGeocall;
      paramsTTRemedy        = this.workOrderKey;
      
      /* checkValiditaJwtGeocall */
      
      
    } else {
      paramsCreaRmaRichiesteCompletate = 'SI';
    }


    qParamsCreaRma = {
      SLA                       : this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR,
      SLA_CATALOGO              : this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR,
      MAGAZZINO_PARTENZA        : '',
      MAGAZZINO_DESTINAZIONE    : this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE,
      ID_SIRTI                  : '',
      PART_NUMBER               : this.reactiveFormCercaMateriale.value.materialeCheck.PART_NUMBER,
      SAP_CODE                  : this.reactiveFormCercaMateriale.value.materialeCheck.SAP_CODE,
      SERIAL_NUMBER             : '',
      QUANTITA                  : this.reactiveFormCercaMateriale.value.qtaCheck,
      TIPO_RICHIESTA            : 'RMA',
      DATA_PREVISTA_CONSEGNA    : '',
      DATA_APP_CONSEGNA         : '',
      TT_REMEDY                 : paramsTTRemedy,
      LATITUDINE_OTR            : '',
      LONGITUDINE_OTR           : '',
      INDIRIZZO_OTR             : '',
      TIPO_CONSEGNA             : '',
      SEDE_GLS                  : '',
      CAP_GLS                   : '',
      INDIRIZZO_GLS             : '',
      LOCALITA_GLS              : '',
      PROVINCIA_GLS             : '',
      REGIONE_GLS               : '',
      TELEFONO_GLS              : '',
      FAX_GLS                   : '',
      REFERENTE_CONSEGNA        : '',
      TELEFONO_CONSEGNA         : '',
      INDIRIZZO_CONSEGNA        : '',
      CAP_CONSEGNA              : '',
      COMUNE_CONSEGNA           : '',
      PROVINCIA_CONSEGNA        : '',
      REGIONE_CONSEGNA          : '',
      NAZIONE_CONSEGNA          : '',
      STATO_DEST_RMA            : 'ATTESA MATERIALE',
      RESO_PRESSO_GLS           : '',
      RICHIEDENTE               : this.richiedente,
      EXTERNAL_ID               : paramsJwtTokenGeocall,
      RICHIESTE_COMPLETATE      : paramsCreaRmaRichiesteCompletate,
      NOTE                      : ''
    }

    console.debug('creaRma => qParamsCreaRma',qParamsCreaRma);


    this.gestScorteServices.createRmaWind(qParamsCreaRma).subscribe(( data )=>{
      console.debug('creaRma => data',data);
      this.creaRmaSection.loaderCreaRma = false;
      if (data.sessioneValida == 'OK') {
        if (data.data.ESITO === 'KO') {
          this.creaRmaSection.mostraEsitoCreaRma  = true;
          this.creaRmaSection.tipoEsitoCreaRma    = 'ERROR';
          this.creaRmaSection.msgEsitoCreaRma     = 'Crea RMA ATTESA fallito: '+data.data.DESC_ESITO;
        } else {
          this.creaRmaSection.mostraEsitoCreaRma    = true;
          this.creaRmaSection.tipoEsitoCreaRma      = 'SUCCESS';
          const descEsitoOk =  (data.data.DESC_ESITO && data.data.DESC_ESITO !== '') ? ' - '+data.data.DESC_ESITO : '';
          this.creaRmaSection.msgEsitoCreaRma       = 'Crea RMA ATTESA eseguito con successo: '+data.data.ID_ATT_RMA+' '+descEsitoOk;
          this.creaRmaSection.mostraSezioneCreaRma  = false; /*nascondo la sezione crea rma */
          this.creaRmaSection.mostraMsgEsitoRicercaMateriale = false; /*nascondo la sezione crea rma */
          if ((this.richiedente === 'GEOCALL') && (sChiamante === 'ULTIMA')) {  
            this.creaRmaSection.msgUltimaCreaRmaGeocall = true;
          } 
          setTimeout(()=>{
            if ((this.richiedente === 'GEOCALL') && (sChiamante === 'ULTIMA')) {          
              //this.creaRmaSection.geocallScaduta = true;
            }    
          }, 10000);
    
          
          
        }

        this.getRichiesteRmaCreate();
      } else {
        this.creaRmaSection.mostraEsitoCreaRma  = true;
        this.creaRmaSection.tipoEsitoCreaRma    = 'ERROR';
        this.creaRmaSection.msgEsitoCreaRma     = 'Crea RMA ATTESA fallito: '+data.data.DESC_ESITO;
        this.creaRmaSection.geocallScaduta = true;
      }
      
    });



  }

  /* creaRma */
  creaRma(sChiamante): void {
    console.debug('creaRma -> value: ',this.reactiveFormCreaRma.value);
    this.creaRmaSection.loaderCreaRma       = true;
    this.creaRmaSection.mostraEsitoCreaRma  = false;
    this.creaRmaSection.tipoEsitoCreaRma    = '';
    this.creaRmaSection.msgEsitoCreaRma     = '';
    this.creaRmaSection.msgUltimaCreaRmaGeocall = false;
    let qParamsCreaRma = {};
    let mappingDatiEseguito = false;
    if (this.abilitaCreaRmaButton()) {
      console.debug('creaRma => tipoOperazione',this.creaRmaSection.tipoOperazione);

      /* common value */

      let paramsCreaRmaSLa;
      let paramsCreaRmaDataPrevistaConsegna;
      let paramsCreaRmaNote;
      let paramsCreaRmaStatoDestRma;
      let paramsCreaRmaRichiesteCompletate;
      let paramsJwtTokenGeocall;

      if (this.richiedente === 'GEOCALL') {
        paramsCreaRmaRichiesteCompletate = sChiamante === 'ULTIMA' ? 'SI':'NO';
        paramsJwtTokenGeocall = this.jwtTokenGeocall;
        
        /* checkValiditaJwtGeocall */
        
      } else {
        paramsCreaRmaRichiesteCompletate = 'SI';
      }

      if (this.creaRmaSection.tipoOperazione === 'PRENOTAZIONE') {

        paramsCreaRmaSLa                  = this.reactiveFormCreaRma.value.slaOriginaleCheck
        paramsCreaRmaDataPrevistaConsegna = this.reactiveFormCreaRma.value.dataPrevistaConsegnaCheck;
        paramsCreaRmaNote                 = this.reactiveFormCreaRma.value.noteStandardCheck;
        paramsCreaRmaStatoDestRma         = 'RITIRO MATERIALE';

        mappingDatiEseguito               = true;

      } else if (this.creaRmaSection.tipoOperazione === 'SPEDIZIONE') {

        paramsCreaRmaStatoDestRma         = 'APPROVATO';

        /* SPEDIZIONE STANDARD */
        if ((!this.creaRmaSection.mostraCampiRilassaSla) && (!this.creaRmaSection.mostraCampiAcceleraSla)) {
          console.debug('creaRma => STANDARD');
          paramsCreaRmaSLa                  = this.reactiveFormCreaRma.value.slaOriginaleCheck;
          paramsCreaRmaDataPrevistaConsegna = this.reactiveFormCreaRma.value.dataPrevistaConsegnaCheck;
          paramsCreaRmaNote                 = this.reactiveFormCreaRma.value.noteStandardCheck;
          mappingDatiEseguito               = true;

        } else if ((this.creaRmaSection.mostraCampiRilassaSla) && (!this.creaRmaSection.mostraCampiAcceleraSla)) { /* SPEDIZIONE RILASSA SLA */
          console.debug('creaRma => RILASSA SLA');
          paramsCreaRmaSLa                  = this.reactiveFormCreaRma.value.slaRilassaCheck;
          paramsCreaRmaDataPrevistaConsegna = this.reactiveFormCreaRma.value.dataPrevistaConsegnaRilassaCheck;
          paramsCreaRmaNote                 = this.reactiveFormCreaRma.value.noteRilassaCheck;
          mappingDatiEseguito               = true;
        } else if ((!this.creaRmaSection.mostraCampiRilassaSla) && (this.creaRmaSection.mostraCampiAcceleraSla)) { /* SPEDIZIONE ACCELERA SLA */
          console.debug('creaRma => RILASSA ACCELERA SLA');
          paramsCreaRmaSLa                  = this.reactiveFormCreaRma.value.slaAcceleraCheck;
          paramsCreaRmaDataPrevistaConsegna = this.reactiveFormCreaRma.value.dataPrevistaConsegnaAcceleraCheck;
          paramsCreaRmaNote                 = this.reactiveFormCreaRma.value.noteAcceleraCheck;
          mappingDatiEseguito               = true;
        }



      } else {
        console.debug('creaRma => caso non gestito');
        this.creaRmaSection.loaderCreaRma = false;
        mappingDatiEseguito               = false;
      }

      if (mappingDatiEseguito) {

        qParamsCreaRma = {
          SLA                       : paramsCreaRmaSLa,
          SLA_CATALOGO              : this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR,
          MAGAZZINO_PARTENZA        : this.resultVerificaGiacenza[0].SIGLA_MAGAZZINO_DISPONIBILE,
          MAGAZZINO_DESTINAZIONE    : this.reactiveFormCercaMateriale.value.magazzinoCheck.SEDE,
          ID_SIRTI                  : '',
          PART_NUMBER               : this.resultVerificaGiacenza[0].PART_NUMBER_DISPONIBILE,
          SAP_CODE                  : this.resultVerificaGiacenza[0].SAP_CODE_DISPONIBILE,
          SERIAL_NUMBER             : '',
          QUANTITA                  : this.reactiveFormCercaMateriale.value.qtaCheck,
          TIPO_RICHIESTA            : 'RMA',
          DATA_PREVISTA_CONSEGNA    : paramsCreaRmaDataPrevistaConsegna.replace(/\//g, '-'),
          DATA_APP_CONSEGNA         : this.reactiveFormCreaRma.value.dataConsegnaSuAppuntamentoStdCheck +' '+this.reactiveFormCreaRma.value.oraConsegnaSuAppuntamentoStdCheck,
          TT_REMEDY                 : this.reactiveFormCreaRma.value.ttRemedyCheck,
          LATITUDINE_OTR            : this.reactiveFormCreaRma.value.latitudineOtrCheck,
          LONGITUDINE_OTR           : this.reactiveFormCreaRma.value.longitudineOtrCheck,
          INDIRIZZO_OTR             : this.reactiveFormCreaRma.value.IndirizzoOtrCheck,
          TIPO_CONSEGNA             : this.reactiveFormCreaRma.value.tipoConsegnaCheck,
          SEDE_GLS                  : this.reactiveFormCreaRma.value.sedeGlsCheck.SEDE,
          CAP_GLS                   : this.reactiveFormCreaRma.value.capGlsCheck,
          INDIRIZZO_GLS             : this.reactiveFormCreaRma.value.indirizzoGlsCheck,
          LOCALITA_GLS              : this.reactiveFormCreaRma.value.localitaGlsCheck,
          PROVINCIA_GLS             : this.reactiveFormCreaRma.value.provinciaGlsCheck,
          REGIONE_GLS               : this.reactiveFormCreaRma.value.regioneGlsCheck,
          TELEFONO_GLS              : this.reactiveFormCreaRma.value.telefonoGlsCheck,
          FAX_GLS                   : this.reactiveFormCreaRma.value.faxGlsCheck,
          REFERENTE_CONSEGNA        : this.reactiveFormCreaRma.value.referenteConsegnaCheck,
          TELEFONO_CONSEGNA         : this.reactiveFormCreaRma.value.telefonoConsegnaCheck,
          INDIRIZZO_CONSEGNA        : this.reactiveFormCreaRma.value.indirizzoConsegnaCheck,
          CAP_CONSEGNA              : this.reactiveFormCreaRma.value.capConsegnaCheck,
          COMUNE_CONSEGNA           : this.reactiveFormCreaRma.value.comuneConsegnaCheck,
          PROVINCIA_CONSEGNA        : this.reactiveFormCreaRma.value.provinciaConsegnaCheck,
          REGIONE_CONSEGNA          : this.reactiveFormCreaRma.value.regioneConsegnaCheck,
          NAZIONE_CONSEGNA          : this.reactiveFormCreaRma.value.nazioneConsegnaCheck,
          STATO_DEST_RMA            : paramsCreaRmaStatoDestRma,
          RESO_PRESSO_GLS           : this.reactiveFormCreaRma.value.restituzioneResoPressoCheck,
          RICHIEDENTE               : this.richiedente,
          EXTERNAL_ID               : paramsJwtTokenGeocall,
          RICHIESTE_COMPLETATE      : paramsCreaRmaRichiesteCompletate,
          NOTE                      : paramsCreaRmaNote
        }

        console.debug('creaRma => qParamsCreaRma',qParamsCreaRma);


        this.gestScorteServices.createRmaWind(qParamsCreaRma).subscribe(( data )=>{
          console.debug('creaRma => data',data);
          this.creaRmaSection.loaderCreaRma = false;
          if (data.sessioneValida == 'OK') {
            if (data.data.ESITO === 'KO') {           
              this.creaRmaSection.mostraEsitoCreaRma  = true;
              this.creaRmaSection.tipoEsitoCreaRma    = 'ERROR';
              this.creaRmaSection.msgEsitoCreaRma     = 'Crea RMA fallito: '+data.data.DESC_ESITO;
            } else {
              this.creaRmaSection.mostraEsitoCreaRma    = true;
              this.creaRmaSection.tipoEsitoCreaRma      = 'SUCCESS';
              const descEsitoOk =  (data.data.DESC_ESITO && data.data.DESC_ESITO !== '') ? ' - '+data.data.DESC_ESITO : '';
              this.creaRmaSection.msgEsitoCreaRma       = 'Crea RMA ATTESA eseguito con successo: '+data.data.ID_ATT_RMA+' '+descEsitoOk;
              this.creaRmaSection.mostraSezioneCreaRma  = false; /*nascondo la sezione crea rma */
              this.creaRmaSection.mostraMsgEsitoRicercaMateriale = false; /*nascondo la sezione crea rma */
              if ((this.richiedente === 'GEOCALL') && (sChiamante === 'ULTIMA')) {  
                this.creaRmaSection.msgUltimaCreaRmaGeocall = true;
              } 
         
                setTimeout(()=>{
                  if ((this.richiedente === 'GEOCALL') && (sChiamante === 'ULTIMA')) {          
                    //this.creaRmaSection.geocallScaduta = true;
                  }    
                }, 10000);
            
              
            }
  
            this.getRichiesteRmaCreate();

          } else {
            this.creaRmaSection.mostraEsitoCreaRma  = true;
            this.creaRmaSection.tipoEsitoCreaRma    = 'ERROR';
            this.creaRmaSection.msgEsitoCreaRma     = 'Crea RMA fallito: '+data.data.DESC_ESITO;
            this.creaRmaSection.geocallScaduta = true;
          }
          
        });

      } else {
        console.debug('creaRma => mapping dei dati incompleto');
        this.creaRmaSection.mostraEsitoCreaRma  = true;
        this.creaRmaSection.tipoEsitoCreaRma    = 'WARNING';
        this.creaRmaSection.msgEsitoCreaRma     = 'mapping dei dati incompleto';
      }

    } else {
      /* errore succesivo al check */
      this.creaRmaSection.loaderCreaRma       = false;
      this.creaRmaSection.mostraEsitoCreaRma  = true;
      this.creaRmaSection.tipoEsitoCreaRma    = 'WARNING';
      this.creaRmaSection.msgEsitoCreaRma     = 'Compilare / Selezionare correttamente tutti i campi per eseguire il Crea RMA';
      //this.creaRmaSection.abilitaCreaRma  = false;
    }


  }

  verificaBottoneRma(e):void {

    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    //e.stopPropagation();
  }

  /* funzione che gestisce l'abilita del pulsante crea RMA in base all'operazione,le custom e ai campi compilati */
  abilitaCreaRmaButton():boolean {
    console.debug('abilitaCreaRmaButton');
    if (!this.creaRmaSection.mostraSezioneCustom) {
      console.debug('abilitaCreaRmaButton => sezione custom non visibile, verifico solo i dati principali');

      /* forzo PDP - tipoConsegnaCheck */
      this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue('PDP', {onlySelf: false});

      /* verifico la validita degli edit base */
      if (
          (this.reactiveFormCreaRma.controls.ttRemedyCheck.valid) &&
          (this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.valid) &&
          (this.reactiveFormCreaRma.controls.slaOriginaleCheck.valid) &&
          (this.reactiveFormCreaRma.controls.dataPrevistaConsegnaCheck.valid) &&
          (this.reactiveFormCreaRma.controls.tipoConsegnaCheck.valid)
         ) {
        console.debug('abilitaCreaRmaButton =>  VALID');
        return true;
      } else {
        console.debug('abilitaCreaRmaButton =>  INVALID');
        return false;

      }

    } else {
      console.debug('abilitaCreaRmaButton => sezione custom visibile, verifico in base al contenuto attivato');

      let checkError = 0;

      /* gestisco il check sulla data e ora Consegna Su Appuntamento - Inizio */
      if (
          ((this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR === '4') && (!this.creaRmaSection.mostraCampiRilassaSla)) ||
          ((this.reactiveFormCercaMateriale.value.materialeCheck.SLA_DELIVERY_HOUR === '24') && (this.creaRmaSection.mostraCampiAcceleraSla))
         ) {
        console.debug('abilitaCreaRmaButton => Esiste DATA E ORA Consegna su Appuntamento => Verifico');

        const dataCa  = this.reactiveFormCreaRma.value.dataConsegnaSuAppuntamentoStdCheck;
        const oraCA   = this.reactiveFormCreaRma.value.oraConsegnaSuAppuntamentoStdCheck;

        console.debug('abilitaCreaRmaButton => Valori',dataCa,oraCA);

        /* se i campi sono entrambi valorizzati */
        if (((dataCa)&&(dataCa !== '')) && ((oraCA)&&(oraCA !== ''))) {

            console.debug('abilitaCreaRmaButton => 01 => i campi sono entrambi valorizzati');

          /* controllo se rispetta il min e max */
          if ((this.reactiveFormCreaRma.controls.oraConsegnaSuAppuntamentoStdCheck.errors) &&
              (this.reactiveFormCreaRma.controls.oraConsegnaSuAppuntamentoStdCheck.errors.errorMinMax)) {
            console.debug('abilitaCreaRmaButton => 02 => errore min max');
            checkError++;

          } else {
            console.debug('abilitaCreaRmaButton => 03 => data valida');
          }

        }
        /* se e' stato valorizzato solo 1 dei due campi errore*/
        else if (
                ((dataCa !== '') && ((oraCA == '') || (!oraCA))) ||
                (((dataCa == '') || (!dataCa)) && (oraCA != ''))
                ) {
          console.debug('abilitaCreaRmaButton => 04 => errore solo 1 dei 2 campi valorizzati');
          checkError++;
        } else { /* se sono entrambi nulli, allora e' valido perche non e' stato selezionata la data e ora */
            console.debug('abilitaCreaRmaButton => 05 => valido nessuno valorizzato');
        }
      } /* gestisco il check sulla data e ora Consegna Su Appuntamento - Fine */

      /* gestisco il check su Rilassa SLA - Inizio */
      if (this.creaRmaSection.mostraCampiRilassaSla) {
        console.debug('abilitaCreaRmaButton => Esiste RILASSA SLA => Verifico');
        if (
            (this.reactiveFormCreaRma.controls.ttRemedyCheck.valid) &&
            (this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.valid) &&
            (this.reactiveFormCreaRma.controls.slaRilassaCheck.valid) &&
            (this.reactiveFormCreaRma.controls.dataPrevistaConsegnaRilassaCheck.valid)
           ) {
          console.debug('abilitaCreaRmaButton => 01 => i campi sono tutti valorizzati');

        } else {
          console.debug('abilitaCreaRmaButton => 02 => uno dei campi non valorizzato');
          checkError++;
        }
      } /* gestisco il check su Rilassa SLA - Fine */

      /* gestisco il check su Accelera SLA - Inizio */
      if (this.creaRmaSection.mostraCampiAcceleraSla) {
        console.debug('abilitaCreaRmaButton => Esiste ACCELERA SLA => Verifico');
        if (
            (this.reactiveFormCreaRma.controls.ttRemedyCheck.valid) &&
            (this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.valid) &&
            (this.reactiveFormCreaRma.controls.slaAcceleraCheck.valid) &&
            (this.reactiveFormCreaRma.controls.dataPrevistaConsegnaAcceleraCheck.valid)
           ) {
          console.debug('abilitaCreaRmaButton => 01 => i campi sono tutti valorizzati');

        } else {
          console.debug('abilitaCreaRmaButton => 02 => uno dei campi non valorizzato');
          checkError++;
        }
      } /* gestisco il check su Rilassa SLA - Fine */

      /* gestisco il check su OTR O OTR ACCELERA - Inizio */
      if ((this.creaRmaSection.mostraCampiOtr) ||(this.creaRmaSection.mostraCampiOtrAccelera)) {
        console.debug('abilitaCreaRmaButton => Esiste OTR O OTR ACCELERA => Verifico');
        if (
            (this.reactiveFormCreaRma.controls.latitudineOtrCheck.valid) &&
            (this.reactiveFormCreaRma.controls.longitudineOtrCheck.valid) &&
            (this.reactiveFormCreaRma.controls.IndirizzoOtrCheck.valid)
           ) {
          if (this.esitoValidaOtr === 'OK') {
            console.debug('abilitaCreaRmaButton => 01 => i campi sono tutti valorizzati con indirizzo OTR => VALIDATO');
          } else {
            console.debug('abilitaCreaRmaButton => 02 => i campi sono tutti valorizzati, ma con indirizzo OTR => NON VALIDATO');
            checkError++;
          }


        } else {
          console.debug('abilitaCreaRmaButton => 03 => uno dei campi non valorizzato');
          checkError++;
        }
      } /* gestisco il check su OTR O OTR ACCELERA - Fine */

      /* gestisco il check su TIPO CONSEGNA - Inizio */
      if (this.creaRmaSection.mostraTipoConsegna) {
        console.debug('abilitaCreaRmaButton => Esiste TIPO CONSEGNA => Verifico');
        /*controllo il valore selezionato */

        const tipoConsSel = this.reactiveFormCreaRma.value.tipoConsegnaCheck;
        console.debug('abilitaCreaRmaButton => 01 => tipo consegna selezionato:',tipoConsSel);
        /*['PDP','FERMO DEPOSITO','ALTRO INDIRIZZO']*/
        if ((tipoConsSel === 'PDP') && (!this.creaRmaSection.mostraCampiTipoConsegna)) {
          console.debug('abilitaCreaRmaButton => 02 => valido per PDP');
        } else if ((tipoConsSel === 'FERMO DEPOSITO') && (this.creaRmaSection.mostraCampiTipoConsegna))  {
          /* constrollo se i campi sono stati visualizzati e verifico la correttezza */
          if (
              (this.reactiveFormCreaRma.controls.sedeGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.indirizzoGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.capGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.localitaGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.provinciaGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.regioneGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.telefonoGlsCheck.valid) &&
              (this.reactiveFormCreaRma.controls.faxGlsCheck.valid)
            ) {
            console.debug('abilitaCreaRmaButton => 03 => tutti campi validi per FERMO DEPOSITO');
          } else {
            console.debug('abilitaCreaRmaButton => 04 => uno dei campi non validi FERMO DEPOSITO');
            checkError++;
          }


        } else if ((tipoConsSel === 'ALTRO INDIRIZZO') && (this.creaRmaSection.mostraCampiTipoConsegna)) {
          /* constrollo se i campi sono stati visualizzati e verifico la correttezza */
          if (
              (this.reactiveFormCreaRma.controls.indirizzoConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.capConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.comuneConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.provinciaConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.regioneConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.nazioneConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.telefonoConsegnaCheck.valid) &&
              (this.reactiveFormCreaRma.controls.referenteConsegnaCheck.valid)
            ) {
            console.debug('abilitaCreaRmaButton => 05 => tutti campi validi per ALTRO INDIRIZZO');
          } else {
            console.debug('abilitaCreaRmaButton => 06 => uno dei campi non validi ALTRO INDIRIZZO');
            checkError++;
          }
        }


      } else {
        console.debug('abilitaCreaRmaButton => NON Esiste TIPO CONSEGNA => Forzo il valore a PDP');
        this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue('PDP', {onlySelf: false});
      }


      /* nei casi di SPEDIZIONE NO RILASSA e NO ACCELERA */
      if (this.creaRmaSection.tipoOperazione === 'SPEDIZIONE') {

        if ((!this.creaRmaSection.mostraCampiAcceleraSla) && (!this.creaRmaSection.mostraCampiRilassaSla)) {

          console.debug('abilitaCreaRmaButton => Esiste SPEDIZIONE (NO RILASSA e NO ACCELERA) => Verifico');

          /* verifico la validita degli edit base */
          if (
              (this.reactiveFormCreaRma.controls.ttRemedyCheck.valid) &&
              (this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.valid) &&
              (this.reactiveFormCreaRma.controls.slaOriginaleCheck.valid) &&
              (this.reactiveFormCreaRma.controls.dataPrevistaConsegnaCheck.valid)
            ) {
              console.debug('abilitaCreaRmaButton => 01 => tutti campi validi della SPEDIZIONE (NO RILASSA e NO ACCELERA)');
            } else {
              console.debug('abilitaCreaRmaButton => 02 => uno dei campi non validi della SPEDIZIONE (NO RILASSA e NO ACCELERA)');
              checkError++;
            }

        }


      }



      if (this.creaRmaSection.tipoOperazione === 'PRENOTAZIONE') { /* nei casi di prenotazione */

        console.debug('abilitaCreaRmaButton => Esiste PRENOTAZIONE => Verifico');

        /* forzo PDP - tipoConsegnaCheck */
        this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue('PDP', {onlySelf: false});

        /* verifico la validita degli edit base */
        if (
            (this.reactiveFormCreaRma.controls.ttRemedyCheck.valid) &&
            (this.reactiveFormCreaRma.controls.restituzioneResoPressoCheck.valid) &&
            (this.reactiveFormCreaRma.controls.slaOriginaleCheck.valid) &&
            (this.reactiveFormCreaRma.controls.dataPrevistaConsegnaCheck.valid) &&
            (this.reactiveFormCreaRma.controls.tipoConsegnaCheck.valid)
           ) {
            console.debug('abilitaCreaRmaButton => 01 => tutti campi validi della PRENOTAZIONE');
          } else {
            console.debug('abilitaCreaRmaButton => 02 => uno dei campi non validi della PRENOTAZIONE');
            checkError++;
          }

      }

      /* gestisco il check su TIPO CONSEGNA - Fine */

      /* controllo eventuali errori */
      if (checkError > 0) {
        console.debug('abilitaCreaRmaButton =>  INVALID');
        return false;
      } else {
        console.debug('abilitaCreaRmaButton =>  VALID');
        return true;
      }

    }

  }

  /* acceleraSla */
  acceleraSla(): void {
    console.debug('acceleraSla -> value: ',this.reactiveFormCreaRma.value);
    this.gestisciSezioniCustom('ACCELERA SLA');
  }

  /* rilassaSla */
  rilassaSla(): void {
    console.debug('rilassaSla -> value: ',this.reactiveFormCreaRma.value);
    this.gestisciSezioniCustom('RILASSA SLA');
  }

  /* otrStandard */
  otrStandard(): void {
    console.debug('otrStandard -> value: ',this.reactiveFormCreaRma.value);
    /* verifico se e' OTR o OTR ACCELERA SLA */
    if (!this.creaRmaSection.mostraCampiAcceleraSla) { /* se i campi di ACCELERA SLA sono nascosti, allora siamo OTR */
      this.gestisciSezioniCustom('OTR'); /* vado a OTR */
            setTimeout(()=>{
                  this.initAutocomplete();
             }, 500);
    } else { /* se i campi di ACCELERA SLA sono visibili, allora siamo OTR ACCELERA SLA */
      this.gestisciSezioniCustom('OTR ACCELERA SLA'); /* vado a OTR ACCELERA SLA */
            setTimeout(()=>{
                  this.initAutocomplete();
             }, 500);
    }

  }

  chiudiCampiOtr(): void {
    console.debug('chiudiCampiOtr');
    if (!this.creaRmaSection.mostraCampiAcceleraSla) { /* se i campi di ACCELERA SLA sono nascosti, allora siamo OTR */
      this.gestisciSezioniCustom('STANDARD'); /* torno a STANDARD */
            setTimeout(()=>{
                  this.initAutocomplete();
             }, 500);
    } else { /* se i campi di ACCELERA SLA sono visibili, allora siamo OTR ACCELERA SLA */
      this.gestisciSezioniCustom('ACCELERA SLA'); /* torno a ACCELERA SLA */
            setTimeout(()=>{
                  this.initAutocomplete();
             }, 500);
    }

  }



 // Choose city using select dropdown
  changeResoPressoValue(e) {
    e.stopPropagation();
    this.reactiveFormCreaRma.get("restituzioneResoPressoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('changeResoPressoValue',this.reactiveFormCreaRma.value.restituzioneResoPressoCheck);

   }

  changeTipoConsegnaValue(e) {
    e.stopPropagation();
    this.reactiveFormCreaRma.get("tipoConsegnaCheck").setValue(e.target.value, {onlySelf: false});
    console.debug('changeTipoConsegnaValue',this.reactiveFormCreaRma.value.tipoConsegnaCheck);
    this.gestisciSezioniCustom('TIPO CONSEGNA');
          setTimeout(()=>{
                this.initAutocomplete();
           }, 500);

   }

  changeTime(timeSelected) {

     console.debug('changeTime',timeSelected);
     if (timeSelected.value) {
      const aTimeSelected = timeSelected.value.split(':');
      const sDataSelected = this.reactiveFormCreaRma.value.dataConsegnaSuAppuntamentoStdCheck+' '+timeSelected.value;

      let dateParts = sDataSelected.substring(0, 10).split("-");
      let oreMinParts = sDataSelected.substring(11, 19).split(":");


      console.debug('changeTime => DATA SELECTED',sDataSelected);

      const dateNew = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));

      const sDataMin = this.creaRmaSection.sDataAppConsMinDateTime;

      dateParts = sDataMin.substring(0, 10).split("/");
      oreMinParts = sDataMin.substring(11, 19).split(":");

      console.debug('changeTime => DATA MIN',sDataMin);

      const dateMin = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));

      const sDataMax = this.creaRmaSection.sDataAppConsMaxDateTime;

      dateParts = sDataMax.substring(0, 10).split("/");
      oreMinParts = sDataMax.substring(11, 19).split(":");

      console.debug('changeTime => DATA MAX',sDataMax);

      const dateMax = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));

      if (dateNew < dateMin) {
       console.debug('changeTime => ERROR A < MIN ',dateNew,dateMin);
       //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
       return {errorMinMax:true};
       //this.reactiveFormCreaRma.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMin.substring(11, 19), {onlySelf: false});
      } else {
        if (dateNew > dateMax) {
         console.debug('changeTime => ERROR A > MAX ',dateNew,dateMax);
         //this.reactiveFormCreaRma.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMax.substring(11, 19), {onlySelf: false});
         //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
         return {errorMinMax:true};
        } else {
          console.debug('changeTime => VALORE ACCETTATO:  MIN <= A <= MAX ',dateNew,dateMin,dateMax);
          //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
          return {errorMinMax:false};
        }
      }
     }



  }

  onDateSelect(dateSelected) {
    console.debug('onDateSelect',dateSelected);


    /* conrtrollo se la data selezionata e' la MAX */
    console.debug('onDateSelect => Split: ', this.creaRmaSection.sDataAppConsMaxDateTime.split(' '));
    const aDataAppConsMaxFull = this.creaRmaSection.sDataAppConsMaxDateTime.split(' ');
    const aDataAppConsMaxDate = aDataAppConsMaxFull[0].split('/');
    console.debug('onDateSelect => Split: ', aDataAppConsMaxDate);
    const aDataAppConsMaxTime = aDataAppConsMaxFull[1].split(':');
    console.debug('onDateSelect => Split: ', aDataAppConsMaxTime);

    let dateCompare: NgbDate = new NgbDate(parseInt(aDataAppConsMaxDate[2]), parseInt(aDataAppConsMaxDate[1]), parseInt(aDataAppConsMaxDate[0]));

    if (dateCompare.equals(NgbDate.from(dateSelected))) {

      console.debug('onDateSelect => DATA MAX SELEZIONATA');

      this.reactiveFormCreaRma.get("oraConsegnaSuAppuntamentoStdCheck").setValue(aDataAppConsMaxFull[1], {onlySelf: false});

    } else {

      /* conrtrollo se la data selezionata e' la MIN */
      console.debug('onDateSelect => Split: ', this.creaRmaSection.sDataAppConsMinDateTime.split(' '));
      const aDataAppConsMinFull = this.creaRmaSection.sDataAppConsMinDateTime.split(' ');
      const aDataAppConsMinDate = aDataAppConsMinFull[0].split('/');
      console.debug('onDateSelect => Split: ', aDataAppConsMinDate);
      const aDataAppConsMinTime = aDataAppConsMinFull[1].split(':');
      console.debug('onDateSelect => Split: ', aDataAppConsMinTime);

      dateCompare = new NgbDate(parseInt(aDataAppConsMinDate[2]), parseInt(aDataAppConsMinDate[1]), parseInt(aDataAppConsMinDate[0]));

      if (dateCompare.equals(NgbDate.from(dateSelected))) {

        console.debug('onDateSelect => DATA MIN SELEZIONATA');

        this.reactiveFormCreaRma.get("oraConsegnaSuAppuntamentoStdCheck").setValue(aDataAppConsMinFull[1], {onlySelf: false});

      } else {
        console.debug('onDateSelect => NESSUNA DATA RICONOSCIUTA');
      }

    }
   //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
  }

  /* procedura che controlla la validita della sessione */
  controllaSessioneGeocall()  : void {
    console.debug('NuovaRichiestaComponent => controllaSessioneGeocall');
    
    this.showToast      =  false; // toast
    
    let minutiSessioneMax = 0;

    
    let qParamsGeocall = {
        RICHIEDENTE : this.richiedente,
        NOME_GUI    : 'GUI_WIND3',
        MODULO_GUI  : 'MOD_NUOVA_RICHIESTA_RMA',
      };
      
    console.debug('NuovaRichiestaComponent => controllaSessioneGeocall => qParamsGeocall',qParamsGeocall);  
    
    this.gestScorteServices.getMinSessGeocall(qParamsGeocall).subscribe(( dataCheck )=>{
      
      console.debug('NuovaRichiestaComponent => controllaSessioneGeocall => getMinSessGeocall => dataCheck',dataCheck);
      
     

      
      if (dataCheck.data.ESITO === 'KO') {
        this.creaRmaSection.loaderSessionGeocall = false;
        //this.router.navigateByUrl("404");
        this.creaRmaSection.geocallScaduta = true;
        this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
        
      } else {
          
        
        this.delayToast     = 10000; // toast
        this.autohideToast  = true; // toast
        this.tempoRestanteSessione  = parseInt(dataCheck.data.MINUTI_RESIDUI);
        minutiSessioneMax           = parseInt(dataCheck.data.MINUTI_SESSIONE);
        this.messaggioToast         = dataCheck.data.MESSAGGIO_GUI.replace('[VALORE]', dataCheck.data.MINUTI_RESIDUI);
        
        console.debug('NuovaRichiestaComponent => controllaSessioneGeocall => getMinSessGeocall => tempoRestanteSessione',this.tempoRestanteSessione);
        
        if (this.tempoRestanteSessione == 0) {
          this.chiudiSessioneGeocall();
        } else if ((this.tempoRestanteSessione == minutiSessioneMax) ||(this.tempoRestanteSessione == (minutiSessioneMax-1)))  {
          this.showToast      = true; // toast
        } else if ((minutiSessioneMax % 2) >= this.tempoRestanteSessione) {
            this.showToast      = true;
        } else if ((this.tempoRestanteSessione >= 1) && (this.tempoRestanteSessione <= 5)) {
            this.showToast      = true;
        } else {
            this.closeToast();
        }
        
        
    
      }

      
    }, ( err )=>{
      console.debug('NuovaRichiestaComponent => controllaSessioneGeocall => KO =>err: ',err);
      this.creaRmaSection.loaderSessionGeocall            = false;
      //this.router.navigateByUrl("404");
      this.creaRmaSection.geocallScaduta = true;
      this.router.navigateByUrl('gestioneScorte/nuovaRichiesta/'+this.richiedente+'?esterno=SI');
    }); 
    
  }
  
  closeToast() : void {
    this.showToast = false;
    let scarto = 1;
    setTimeout(() => {
                  this.controllaSessioneGeocall();
               },15000);
    
  }




}
