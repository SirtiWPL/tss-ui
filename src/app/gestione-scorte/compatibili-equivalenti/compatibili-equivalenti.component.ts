import { Component, OnInit,Input,OnChanges,ViewChild  } from '@angular/core';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import { MenuCfgModule } from './../../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { UiService } from './../../ui-service.service';
import { GestioneScorteService } from './../../gestione-scorte.service';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
/*per gestire i filtri autocomplete */
import {Observable, of ,OperatorFunction,Subject, merge,forkJoin} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';

@Component({
  selector: 'app-compatibili-equivalenti',
  templateUrl: './compatibili-equivalenti.component.html',
  styleUrls: ['./compatibili-equivalenti.component.css']
})
export class CompatibiliEquivalentiComponent implements OnInit {
  
  public loaderGrid:boolean;
  public carica:boolean;
  public richiedente:string;
  public mostraBoxImportExcel:boolean;
  public mostraBoxMaterialeB:boolean;
  public mostraBoxTipoCompatibilita:boolean;
  public mostraBottoniTipoCompatibilita:boolean;
  public tipoCompatibilitaSelected:string;
  public msgConfermaTipoCompatibilitaSelezionata:string;
  public gridConfig:any           = {};
  public guiChiamante:any         = {};
  public msgAzioneObj: any           = {};

  public materialePrincipaleSection:any = {
    loaderAutocompleteMat:              false,
    loaderAutocompleteQuadrupla:        false,
    zeroResultMateriale:                false,
    zeroResultQuadrupla:                false,
    quadrupleMateriale:                 [],
    abilitaFiltroQuadrupla:             false,
  };
  
  public materialeCompatibileEqSection:any = {
    loaderAutocompleteMat:              false,
    zeroResultMateriale:                false,
    errorCompatibilitaEsistente:        false,
  };

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  
  


  
  /* definisco il reactive form */
  public reactiveFormCercaMaterialePrincipale     : FormGroup;
  public reactiveFormCercaMaterialeCompatibileEq  : FormGroup;
  
  /* formatter degli autocomplete */
  formatterMaterialePrincipale        = (materialeObj: any) => materialeObj.PART_NUMBER;
  formatterQuadruplaMatPrincipale     = (quadruplaObj: any) => quadruplaObj.QUADRUPLA;
  formatterMaterialeCompatibileEq     = (materialeObj: any) => materialeObj.PART_NUMBER;
  //formatterQuadruplaMatCompatibileEq  = (quadruplaObj: any) => quadruplaObj.QUADRUPLA;

  constructor(private uiService: UiService,
              private gestScorteServices: GestioneScorteService,
              private formBuilderCercaMaterialePrincipale: FormBuilder,
              private formBuilderCercaMaterialeCompatibileEq: FormBuilder
              ) {
    
    
    /* formBuilderCercaMaterialePrincipale */

    this.reactiveFormCercaMaterialePrincipale = this.formBuilderCercaMaterialePrincipale.group({
      materialePrincipaleCheck    : [null,[Validators.required]],
      quadruplaMatPrincipaleCheck : [null,[Validators.required]],
    });
    
    /* formBuilderCercaMaterialeCompatibileEq */

    this.reactiveFormCercaMaterialeCompatibileEq= this.formBuilderCercaMaterialeCompatibileEq.group({
      materialeCompatibileEqCheck     : [null,[Validators.required]],
      //quadruplaMatCompatibileEqCheck  : [null,[Validators.required]],
    });
    
    this.guiChiamante = {
      NOME_GUI          : 'MOD_COMPATIBILI_EQUIVALENTI',
      TIPO_RICHIESTA    : 'COMPATIBILI EQUIVALENTI',
      NOME_SHEET        : 'COMPATIBILI_EQUIVALENTI'
    };  
    
    /* cfg della griglia */
    this.gridConfig = {
      nColonneInput     : null,
      abilitaDettaglio  : false,
      abilitaSelectAll  : true,
      abilitaClick      : false,
      abilitaExport     : true,
      nomeSheet         : this.guiChiamante.NOME_SHEET,
      colonne           : [],
      record            : [],
      customFilter      : [],
      recordSelected    : [],
      mostraConfermaElimina : false
    };
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
    
  
    
    
    
     this.resetGUI();
    
  }

  ngOnInit(): void {
    //this.resetGUI();

  }
  
  public resetGUI(): void {
    

    this.carica                                   = false;
    this.mostraBoxImportExcel                     = false;  /* gestione futura importa da excel */
    this.mostraBoxMaterialeB                      = false;   /* il default e' false - verra' poi gestito in fase di crea nuova compatibilita / equivalenza */
    this.mostraBoxTipoCompatibilita               = false;   /* il default e' false - verra' poi gestito in fase di crea nuova compatibilita / equivalenza */
    this.mostraBottoniTipoCompatibilita           = false;   /* il default e' false - verra' poi gestito in fase di crea nuova compatibilita / equivalenza */
    this.tipoCompatibilitaSelected                = null;
    this.msgConfermaTipoCompatibilitaSelezionata  = '';
    
    this.richiedente                              = 'INTERNO';
    
    
    this.materialePrincipaleSection = {
      loaderAutocompleteMat:              false,
      loaderAutocompleteQuadrupla:        false,
      zeroResultMateriale:                false,
      zeroResultQuadrupla:                false,
      quadrupleMateriale:                 [],
      abilitaFiltroQuadrupla:             false,
    };
    
    this.reactiveFormCercaMaterialePrincipale.get("quadruplaMatPrincipaleCheck").setValue(null, {onlySelf: false});
    
    
    this.materialeCompatibileEqSection = {
      loaderAutocompleteMat:              false,
      zeroResultMateriale:                false,
      errorCompatibilitaEsistente:        false,
    };    
    
    
    this.reactiveFormCercaMaterialeCompatibileEq.get("materialeCompatibileEqCheck").setValue(null, {onlySelf: false});
    
    this.gridConfig.colonne      = [];
    this.gridConfig.record       = [];
    this.gridConfig.customFilter = [];
    this.gridConfig.recordSelected = [];

    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
    
  
  }
  
  public resetCompatibileEq(): void {
    
    console.debug('sono CompatibiliEquivalentiComponent => resetCompatibileEq');
 
    this.mostraBoxTipoCompatibilita					      = false;      
    this.mostraBottoniTipoCompatibilita			      = false;
    this.msgConfermaTipoCompatibilitaSelezionata	= '';
    this.tipoCompatibilitaSelected				        = null;
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
    
  }
  
  /* procedura che gestisce il click del pulsante di tipo compatibilita */
  public clickButtonTipoCompatibilita(sOperativita:string): void {
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };    
    
		console.debug('sono CompatibiliEquivalentiComponent => clickButtonTipoCompatibilita',sOperativita);
		
		this.mostraBottoniTipoCompatibilita			      = false;
		this.msgConfermaTipoCompatibilitaSelezionata	= '';
		this.tipoCompatibilitaSelected				        = sOperativita;
		
		if (sOperativita === 'COMPATIBILE') {
			
			this.msgConfermaTipoCompatibilitaSelezionata = 'Si conferma la scelta che A e\' compatibile con B ?';
		
			
		} else if (sOperativita === 'DUALE') {
			
			this.msgConfermaTipoCompatibilitaSelezionata = 'Si conferma la scelta che A e\' compatibile con B e B e\' compatibile con A ?';
		
			
		} else if (sOperativita === 'EQUIVALENTE') {
			
			this.msgConfermaTipoCompatibilitaSelezionata = 'Si conferma la scelta che A e B sono equivalenti ?';
		
			
		}  
    
  }
  
  
	/* annulla tipo compatibilita */
	public clickAnnullaTipoCompatibilita():void {
   
    console.debug('sono CompatibiliEquivalentiComponent => clickAnnullaTipoCompatibilita',this.tipoCompatibilitaSelected);
		
		this.mostraBottoniTipoCompatibilita			      = true;
		this.msgConfermaTipoCompatibilitaSelezionata	= '';
		this.tipoCompatibilitaSelected				        = null;

    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
		
	};
  
  /*conferma tipo compatibilita */
  public clickConfermaTipoCompatibilita():void {
    console.debug('sono CompatibiliEquivalentiComponent => clickConfermaTipoCompatibilita',this.tipoCompatibilitaSelected);
    this.carica = true;
    
		let paramChiamataAZ = {};
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
		
		if (this.tipoCompatibilitaSelected === 'COMPATIBILE') {
						
			/* eseguo una sola chiamata x creare da A a B */
			
			
			paramChiamataAZ = {
				
				CODICE_SIRTI					        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI : null,
				PART_NUMBER						        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER : null,
				SAP_CODE 						          :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE : null,
				NOME_PART_NUMBER				      :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER : null,
				
				CODICE_SIRTI_COMPATIBILE    	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI : null,
				PART_NUMBER_COMPATIBILE     	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER : null,
				SAP_CODE_COMPATIBILE        	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE : null,
				NOME_PART_NUMBER_COMPATIBILE	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER : null,				
				
				EQUIVALENTE						        :	0,
				
				PROGETTO                      :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
				SOTTOPROGETTO                 :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
				PROPRIETA                     :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
				CLIENTE_FINALE                :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE,
				RICHIEDENTE                   : this.richiedente,
        ID_MCM                        : null,
				
				
			};
			
			console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente',paramChiamataAZ);
			
			this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ).subscribe(( resultAZ )=> {
        
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => esito',resultAZ);
			
				
				
				//console.log(resultAZ);
				
				if (resultAZ.data.ESITO === 'OK'){
					//this.alert.success('A compatibile con B Creata con Successo', { ttl: 10000 });
					console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => A compatibile con B Creata con Successo');
					
					
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'SUCCESS',        
            msgEsitoAzione       : 'A compatibile con B Creata con Successo'      
            
          };
          
          this.mostraBottoniTipoCompatibilita			= true;
          this.msgConfermaTipoCompatibilitaSelezionata	= '';
          this.tipoCompatibilitaSelected				= null;
          
          this.clickAnnullaNuovaCompatibilitaButton();
          
          /* ricarico la grid  */
          this.getGridCompatibiliEquivalenti();

					
				}
				else{
					
					//this.alert.error('Fallita Creazione A compatibile con B: '+resultAZ.data.DESC_ESITO, { ttl: 10000 });
          console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A compatibile con B',resultAZ.data.DESC_ESITO);
      
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'ERROR',        
            msgEsitoAzione       : 'Fallita Creazione A compatibile con B: '+resultAZ.data.DESC_ESITO      
            
          };	          
      
				}
        
        this.carica = false;
				

				
				
			}, ( err )=>{
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A compatibile con B => KO =>err: ',err);
        this.msgAzioneObj = {
          
          mostraEsitoAzione    : true,      
          tipoEsitoEsitoAzione : 'ERROR',        
          msgEsitoAzione       : 'Fallita Creazione A compatibile con B: '+err      
          
        };
        this.carica = false;
      }); 
			

		
			
		} else if (this.tipoCompatibilitaSelected === 'DUALE') {
			
			/* eseguo prima la chiamata x creare da A a B */
			
			
			paramChiamataAZ = {
				
				CODICE_SIRTI					        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI : null,
				PART_NUMBER						        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER : null,
				SAP_CODE 						          :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE : null,
				NOME_PART_NUMBER				      :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER : null,
				
				CODICE_SIRTI_COMPATIBILE    	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI : null,
				PART_NUMBER_COMPATIBILE     	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER : null,
				SAP_CODE_COMPATIBILE        	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE : null,
				NOME_PART_NUMBER_COMPATIBILE	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER : null,				
				
				EQUIVALENTE						        :	0,
				
				PROGETTO                      :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
				SOTTOPROGETTO                 :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
				PROPRIETA                     :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
				CLIENTE_FINALE                :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE,
				RICHIEDENTE                   : this.richiedente,
        ID_MCM                        : null,

				
				
			};
			
			console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente',paramChiamataAZ);
			
      this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ).subscribe(( resultAZ_A )=> {   
        
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => esito',resultAZ_A);
			
				
				
				//console.log(resultAZ_A);
				
				if (resultAZ_A.data.ESITO === 'OK'){

				
					/* eseguo dopo la chiamata x creare da B a A */
		
					
					paramChiamataAZ = {
						
						CODICE_SIRTI					        :	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI : null,
						PART_NUMBER						        :	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER : null,
						SAP_CODE 						          :	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE : null,
						NOME_PART_NUMBER				      :	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER : null,
						
						CODICE_SIRTI_COMPATIBILE    	:	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI : null,
						PART_NUMBER_COMPATIBILE     	:	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER : null,
						SAP_CODE_COMPATIBILE        	:	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE : null,
						NOME_PART_NUMBER_COMPATIBILE	:	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER : null,				
						
						EQUIVALENTE						        :	0,
						
						PROGETTO                      :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
						SOTTOPROGETTO                 :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
						PROPRIETA                     :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
						CLIENTE_FINALE                :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE,
            RICHIEDENTE                   : this.richiedente,
            ID_MCM                        : null,
		
						
						
					};
					
					console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente',paramChiamataAZ);
					
          this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ).subscribe(( resultAZ_B )=> {
            
            console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => esito',resultAZ_B);
					
						
						
						//console.log(resultAZ_B);
						
						if (resultAZ_B.data.ESITO === 'OK'){
							//this.alert.success('A compatibile con B e B compatibile con A create con successo ', { ttl: 10000 });
              console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => A compatibile con B e B compatibile con A create con successo');
              this.msgAzioneObj = {
                
                mostraEsitoAzione    : true,      
                tipoEsitoEsitoAzione : 'SUCCESS',        
                msgEsitoAzione       : 'A compatibile con B e B compatibile con A create con successo'      
                
              };
              
              this.mostraBottoniTipoCompatibilita			= true;
              this.msgConfermaTipoCompatibilitaSelezionata	= '';
              this.tipoCompatibilitaSelected				= null;
              
              this.clickAnnullaNuovaCompatibilitaButton();
              
              /* ricarico la grid  */
              this.getGridCompatibiliEquivalenti();   
              
              
						}
						else{
							//this.alert.success('A compatibile con B create con successo ', { ttl: 10000 });
							//this.alert.error('Fallita Creazione B compatibile con A:  '+resultAZ_B.data.DESC_ESITO, { ttl: 10000 });
              console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione B compatibile con A');
              this.msgAzioneObj = {
                
                mostraEsitoAzione    : true,      
                tipoEsitoEsitoAzione : 'ERROR',        
                msgEsitoAzione       : 'Fallita Creazione B compatibile con A: '+resultAZ_B.data.DESC_ESITO        
                
              };
						}
						
          this.carica = false;
						
						
					}, ( err )=>{
            console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione B compatibile con A => KO =>err: ',err);
            this.carica = false;

              this.msgAzioneObj = {
                
                mostraEsitoAzione    : true,      
                tipoEsitoEsitoAzione : 'ERROR',        
                msgEsitoAzione       : 'Fallita Creazione B compatibile con A: '+err      
                
              };            
            
          }); /* eseguo dopo la chiamata x creare da B a A */
					
				}
				else{
					
					//this.alert.error('Fallita Creazione A compatibile con B e B compatibile con A: '+resultAZ_A.data.DESC_ESITO, { ttl: 10000 });
          console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A compatibile con B e B compatibile con A');
          
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'ERROR',        
            msgEsitoAzione       : 'Fallita Creazione A compatibile con B e B compatibile con A: '+resultAZ_A.data.DESC_ESITO        
            
          };
              
          this.carica = false;    
				}

			}, ( err )=>{
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A compatibile con B e B compatibile con A => KO =>err: ',err);
        this.carica = false;
          console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A compatibile con B e B compatibile con A');
              this.msgAzioneObj = {
                
                mostraEsitoAzione    : true,      
                tipoEsitoEsitoAzione : 'ERROR',        
                msgEsitoAzione       : 'Fallita Creazione A compatibile con B e B compatibile con A: '+err       
                
              };    
      });
		
			
		} else if (this.tipoCompatibilitaSelected === 'EQUIVALENTE') {
			
			/* eseguo una sola chiamata x creare A e B equivalenti */
			
			
			paramChiamataAZ = {
				
				CODICE_SIRTI					        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI : null,
				PART_NUMBER						        :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER : null,
				SAP_CODE 						          :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE : null,
				NOME_PART_NUMBER				      :	this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.NOME_PART_NUMBER : null,
				
				CODICE_SIRTI_COMPATIBILE    	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.CODICE_SIRTI : null,
				PART_NUMBER_COMPATIBILE     	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.PART_NUMBER : null,
				SAP_CODE_COMPATIBILE        	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.SAP_CODE : null,
				NOME_PART_NUMBER_COMPATIBILE	:	this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER ? this.reactiveFormCercaMaterialeCompatibileEq.value.materialeCompatibileEqCheck.NOME_PART_NUMBER : null,				
				
				EQUIVALENTE						        :	1,
				
				PROGETTO                      :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
				SOTTOPROGETTO                 :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
				PROPRIETA                     :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
				CLIENTE_FINALE                :	this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE,
        RICHIEDENTE                   : this.richiedente,
        ID_MCM                        : null,				

				
				
			};
			
			console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente',paramChiamataAZ);
			
			this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ).subscribe(( resultAZ )=> {
        
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => esito',resultAZ);
			
				
				
				//console.log(resultAZ);
				
				if (resultAZ.data.ESITO === 'OK'){
					//this.alert.success('A equivalete B Creata con Successo', { ttl: 10000 });
					console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => A equivalete B Creata con Successo');
					
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'SUCCESS',        
            msgEsitoAzione       : 'A equivalete B Creata con Successo'      
            
          };  
					
          this.mostraBottoniTipoCompatibilita			= true;
          this.msgConfermaTipoCompatibilitaSelezionata	= '';
          this.tipoCompatibilitaSelected				= null;
          
          this.clickAnnullaNuovaCompatibilitaButton();
          
          /* ricarico la grid  */
          this.getGridCompatibiliEquivalenti();
					
				}
				else{
					
					//this.alert.error('Fallita Creazione A equivalete B: '+resultAZ.data.DESC_ESITO, { ttl: 10000 });
          console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A equivalete B',resultAZ.data.DESC_ESITO);
          
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'ERROR',        
            msgEsitoAzione       : 'Fallita Creazione A equivalete B: '+resultAZ.data.DESC_ESITO      
            
          };     
          
				}
				
      this.carica = false;
				
				
			}, ( err )=>{
        console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita Creazione A equivalete B => KO =>err: ',err);
        this.carica = false;
        
        this.msgAzioneObj = {
          
          mostraEsitoAzione    : true,      
          tipoEsitoEsitoAzione : 'ERROR',        
          msgEsitoAzione       : 'Fallita Creazione A equivalete B: '+err      
          
        };          
        
      });
			
		
			
		}
    
    
  }
  
  /* click button crea/modifica */
  public clickButtonCreaModifica(buttonFunction:string):void {
    console.debug('sono CompatibiliEquivalentiComponent => clickButtonCreaModifica => buttonFunction',buttonFunction);
    
    this.mostraBoxImportExcel				= false;
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
    
    if (buttonFunction === 'CREA') {
      
			this.mostraBoxMaterialeB        = true;
			this.mostraBoxTipoCompatibilita = false;      
      
    } else if (buttonFunction === 'CERCA') {
      
			this.getGridCompatibiliEquivalenti();     
      
    } else {
      
    }
    
  }
  
	/* Annulla nuova compatibilita' / equivalenza  */
	public clickAnnullaNuovaCompatibilitaButton(): void {
  
    this.reactiveFormCercaMaterialeCompatibileEq.get("materialeCompatibileEqCheck").setValue(null, {onlySelf: false});
    this.mostraBoxMaterialeB							  = false;
    this.mostraBoxTipoCompatibilita					= false;
    
    this.materialeCompatibileEqSection      = {
      loaderAutocompleteMat:              false,
      zeroResultMateriale:                false,
      errorCompatibilitaEsistente:        false,
    };
    


  }
  
  
  /* ricerca materiale invocata dall'autocomplete typehead */
  searchMaterialePrincipale: any = (text$: Observable<any>) =>
    text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    tap(() => this.materialePrincipaleSection.loaderAutocompleteMat = true),
    switchMap(term =>
      this.trovaMateriale(term,'MATERIALE_PRINCIPALE').pipe(
        tap(() => this.materialePrincipaleSection.zeroResultMateriale = false),
        catchError(() => {
          this.materialePrincipaleSection.zeroResultMateriale = true;
          return of([]);
        }))
    ),
    tap(() => this.materialePrincipaleSection.loaderAutocompleteMat = false)
  )
    
    
  /* ricerca materiale invocata dall'autocomplete typehead */
  searchMaterialeCompatibileEq: any = (text$: Observable<any>) =>
    text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    tap(() => this.materialeCompatibileEqSection.loaderAutocompleteMat = true),
    switchMap(term =>
      this.trovaMateriale(term,'MATERIALE_COMPATIBILE').pipe(
        tap(() => this.materialeCompatibileEqSection.zeroResultMateriale = false),
        catchError(() => {
          this.materialeCompatibileEqSection.zeroResultMateriale = true;
          return of([]);
        }))
    ),
    tap(() => this.materialeCompatibileEqSection.loaderAutocompleteMat = false)
  )
    
    
 /* trova materiale - invoca il service per gestire l'autocomplete */
 trovaMateriale(term,sChiamante): any {
    
    
    
    let qParamsMateriali = {};
    
    /* creo l'obj dei parametri */
    
    if (sChiamante === 'MATERIALE_PRINCIPALE') {
      
      this.resetGUI(); /* resetto tutto */
      
      qParamsMateriali = {
        RICHIEDENTE       : this.richiedente,
        /*
        CODICE_SIRTI      : term,
        PART_NUMBER       : term,
        SAP_CODE          : term,
        NOME_PART_NUMBER  : term,
        */
        MULTI_FILTER      : term
      };
    } else {
      
      this.resetCompatibileEq();
      
      qParamsMateriali = {
        RICHIEDENTE               : this.richiedente,
        MULTI_FILTER              : term,
        CODICE_SIRTI_ESCLUDI      : this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI,
        PART_NUMBER_ESCLUDI       : this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER,
        PROGETTO                  : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
        SOTTOPROGETTO             : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
        PROPRIETA                 : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
        CLIENTE_FINALE            : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE   
      };
    }
    

    
    
    /* invoco il servizio che mi restituisce i materiali */
    if (term.length >= 3) {
      if (sChiamante === 'MATERIALE_PRINCIPALE') {
        this.materialePrincipaleSection.loaderAutocompleteMat = true;
      } else {
        this.materialeCompatibileEqSection.loaderAutocompleteMat = true;
      }
      
      return this.gestScorteServices.getAnagCompatibili(qParamsMateriali).pipe(
          map(response => {
            if (response.data.results.length > 0) {
              
              if (sChiamante === 'MATERIALE_PRINCIPALE') {
                this.materialePrincipaleSection.zeroResultMateriale = false;  
              } else {
                this.materialeCompatibileEqSection.zeroResultMateriale = false;  
              }
              
             
              
              const arrayUniqueByKey = [...new Map(response.data.results.map(item =>[item.CODICE_SIRTI, item])).values()]; /* ritorno sempre i valori diversi*/
              
              if (sChiamante === 'MATERIALE_PRINCIPALE') {
                this.materialePrincipaleSection.loaderAutocompleteMat = false;
              } else {
                this.materialeCompatibileEqSection.loaderAutocompleteMat = false;  
              }              
              
              
              
              return arrayUniqueByKey;
            } else {
              
              if (sChiamante === 'MATERIALE_PRINCIPALE') {
                this.materialePrincipaleSection.zeroResultMateriale = true;
                this.materialePrincipaleSection.loaderAutocompleteMat = false; 
              } else {
                this.materialeCompatibileEqSection.zeroResultMateriale = true;
                this.materialeCompatibileEqSection.loaderAutocompleteMat = false; 
              }

              return of([]);
            }
          })
        );
      }
    else {
      if (sChiamante === 'MATERIALE_PRINCIPALE') {
        this.materialePrincipaleSection.zeroResultMateriale = true;
        this.materialePrincipaleSection.loaderAutocompleteMat = false; 
      } else {
        this.materialeCompatibileEqSection.zeroResultMateriale = true;
        this.materialeCompatibileEqSection.loaderAutocompleteMat = false; 
      }
      return of([]);
    }
  }
  
  
  /* seleziona il materiale */
  selectedItemMateriale(itemSelected,sPadreFiltroChiamante,sNomeFiltroChiamante): void {
    console.debug('sono CompatibiliEquivalentiComponent => selectedItemMateriale',itemSelected,sPadreFiltroChiamante,sNomeFiltroChiamante);
    
    
    
    /* se la selezione viene fatta sul filtro del materiale principale */
    if (sPadreFiltroChiamante === 'MATERIALE_PRINCIPALE') {
      
      this.materialePrincipaleSection.loaderAutocompleteMat = true;
  
      if (sNomeFiltroChiamante === 'MULTI_FILTER') {
        
        this.materialePrincipaleSection.abilitaQuadrupla    = false;
        this.materialePrincipaleSection.quadrupleMateriale  = [];
        
        /* devo controllare se il materiale selezionato e' su piu' quadruple */
        const qParamsMateriali = {
          RICHIEDENTE       : this.richiedente,
          CODICE_SIRTI      : itemSelected.item.CODICE_SIRTI,
          PART_NUMBER       : itemSelected.item.PART_NUMBER,
        };
        
        this.gestScorteServices.getAnagCompatibili(qParamsMateriali).subscribe(( result )=>{
          console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili',result);
          //console.debug('sono CompatibiliEquivalentiComponent => selectedItemMateriale',this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck);
          
          /* popolo il filtro delle quadruple */
          
          let resMat:Array<any> = result.data.results;
          
          Object.entries(resMat).forEach(entry => {
            const [key, value] = entry;
            console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili => CICLO QUADRUPLE',entry);
            
            this.materialePrincipaleSection.quadrupleMateriale.push({
              PROGETTO        : value.PROGETTO      ,
              SOTTOPROGETTO   : value.SOTTOPROGETTO ,
              PROPRIETA       : value.PROPRIETA     ,
              CLIENTE_FINALE  : value.CLIENTE_FINALE,
              QUADRUPLA       : value.PROGETTO+' - '+value.SOTTOPROGETTO+' - '+value.PROPRIETA+' - '+value.CLIENTE_FINALE,
            });

          });
          
          console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili => quadrupleMateriale',this.materialePrincipaleSection.quadrupleMateriale);
          
          
          
          /* se esiste il selezionato e' solo 1 ed e' associato a 1 sola quadrupla tra i progetti dell'operatore, allora lo passo alla rotta di check per farlo selezionare */
          if (result.data.results.length === 1 ) {
            console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili => SINGOLA QUADRUPLA');
            
            this.reactiveFormCercaMaterialePrincipale.get("quadruplaMatPrincipaleCheck").setValue(this.materialePrincipaleSection.quadrupleMateriale[0], {onlySelf: false});
            this.materialePrincipaleSection.abilitaQuadrupla = false;
            this.clickButtonCreaModifica('CERCA');
            
          } else { /* se il materiale e' su piu' quadruple tra quelle visibili dall'operatore le devo proporre, propongo solo quelle trovate */
            console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili => MULTIPLA QUADRUPLA');
            
            this.materialePrincipaleSection.abilitaQuadrupla = true;

            
          }
          
        });
      } else if (sNomeFiltroChiamante === 'QUADRUPLA') {
        
        console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_PRINCIPALE => selectedItemMateriale => getAnagCompatibili => DA GESTIRE');
        this.clickButtonCreaModifica('CERCA');
        
      }
      
      this.materialePrincipaleSection.loaderAutocompleteMat = false;
      
      
    } /* MATERIALE_PRINCIPALE - fine */
    else if (sPadreFiltroChiamante === 'MATERIALE_COMPATIBILE') {  /* se la selezione viene fatta sul filtro del MATERIALE_COMPATIBILE */
    
      this.materialeCompatibileEqSection.errorCompatibilitaEsistente =      false;
      this.mostraBoxTipoCompatibilita							  = true;
      this.mostraBottoniTipoCompatibilita 				  = true;
      this.msgConfermaTipoCompatibilitaSelezionata  = '';
      this.tipoCompatibilitaSelected 							  = null;         

      
  
      
    } /* MATERIALE_COMPATIBILE - fine */
    


  }
  

  
  /* ricerca quadrupla materiale */
  searchQuadruplaMateriale: OperatorFunction<string, readonly {QUADRUPLA}[]> = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    //filter(term => term.length >= 2),
    map(term => {
        console.debug('sono CompatibiliEquivalentiComponent => searchQuadruplaMateriale => sono qui',term);

        if (!term || term ==='') {
          this.materialePrincipaleSection.zeroResultQuadrupla    = false;
          this.materialePrincipaleSection.loaderAutocompleteQuadrupla  = false;
          return this.materialePrincipaleSection.quadrupleMateriale;

        } else {
        
          this.materialePrincipaleSection.zeroResultQuadrupla    = false;
          this.materialePrincipaleSection.loaderAutocompleteQuadrupla  = true;
          if (term.length >= 0) {
           const result = this.materialePrincipaleSection.quadrupleMateriale.filter(quadruplaObj => new RegExp('^'+term, 'mi').test(quadruplaObj.QUADRUPLA)).slice(0, 10);
           if (result.length > 0) {
            this.materialePrincipaleSection.zeroResultQuadrupla = false;
           } else {
            this.materialePrincipaleSection.zeroResultQuadrupla = true;
           }
          this.materialePrincipaleSection.loaderAutocompleteQuadrupla  = false;
           return result;
          } else {
            if (term.length == 1) {
              this.materialePrincipaleSection.loaderAutocompleteQuadrupla   = true;
            } else {
              this.materialePrincipaleSection.loaderAutocompleteQuadrupla   = false;
              this.materialePrincipaleSection.zeroResultQuadrupla = true;
            }
            return [];
          }
        }
      }
    ),
  )
  
  searchQuadruplaOpenFocus: OperatorFunction<string, readonly {QUADRUPLA}[]> = (text$: Observable<string>) => {
    
    this.gridConfig.colonne      = [];
    this.gridConfig.record       = [];
    this.gridConfig.customFilter = [];
    this.gridConfig.recordSelected = [];
    
    
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    //const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$).pipe(
      map(term => (term === '' ? this.materialePrincipaleSection.quadrupleMateriale
        : this.materialePrincipaleSection.quadrupleMateriale.filter(quadruplaObj => quadruplaObj.QUADRUPLA.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }
  
  /* procedura che popola la griglia dei compatibili equivalenti in base al materiale selezionato */
  getGridCompatibiliEquivalenti():void {
    
    this.loaderGrid = true; 
    
    this.gridConfig.colonne      = [];
    this.gridConfig.record       = [];
    this.gridConfig.customFilter = [];
    this.gridConfig.recordSelected = [];
    
    
    /* recupero le colonne della grid e i relativi filter */
  
    const qParamsGridColumns = {
      RICHIEDENTE     : this.richiedente,
      CHIAMANTE       : this.guiChiamante.NOME_GUI,
      MODULO          : this.guiChiamante.NOME_GUI,
      TIPO_RICHIESTA  : this.guiChiamante.TIPO_RICHIESTA
    };
    
    this.gestScorteServices.getGridColumns(qParamsGridColumns).subscribe(( data )=>{
      //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
      this.gridConfig.colonne = data.data.results;
    
      console.debug('sono CompatibiliEquivalentiComponent => apriReport => cfg colonne: ', this.gridConfig.colonne);
    
      /*for (let col in this.gridConfig.colonne) {
        
        this.infoElement[this.gridConfig.colonne[col].NOME_COLONNA] = {
          ETICHETTA : this.gridConfig.colonne[col].LABEL,
          TIPO      : this.gridConfig.colonne[col].TIPO,
          POSIZIONE : this.gridConfig.colonne[col].POSIZIONE,        
        };
                                                                
        if (parseInt(this.gridConfig.colonne[col].POSIZIONE) == 1 ) {
          colonnaOrderBy = this.gridConfig.colonne[col].NOME_COLONNA;
        }
    
      } */
      
      if (this.gridConfig.colonne.length > 0 ) {
        
        for (let col in this.gridConfig.colonne) {
          if (this.gridConfig.colonne[col].NOME_COLONNA === 'EQUIVALENTE') {
            this.gridConfig.colonne[col].IS_TIPO_ICONA      = 1;
            this.gridConfig.colonne[col].CUSTOM_ICON_NO     = 'fas fa-long-arrow-alt-right fa-w-14 fa-2x text-danger';
            this.gridConfig.colonne[col].CUSTOM_ICON_SI     = 'fas fa-arrows-alt-h fa-w-14 fa-2x text-success';
            this.gridConfig.colonne[col].COLORE_CUSTOM      = 'bg-primary bg-color-palette';
            
            this.gridConfig.customFilter[this.gridConfig.colonne[col].NOME_COLONNA+'-filter'] = {
              TIPO    : this.gridConfig.colonne[col].TIPO,
              INPUT   : 'SELECT',
              VALORI  : ['SI','NO'],
              COLORE_FILTRO : 'bg-primary bg-color-palette'
              };            
            
          } else if (
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'PART_NUMBER') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'CODICE_SIRTI') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'NOME_PART_NUMBER') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'SAP_CODE')
                    ) {
            this.gridConfig.colonne[col].IS_TIPO_ICONA      = 0;
            this.gridConfig.colonne[col].CUSTOM_ICON_NO     = '';
            this.gridConfig.colonne[col].CUSTOM_ICON_SI     = '';
            this.gridConfig.colonne[col].COLORE_CUSTOM      = 'bg-green bg-color-palette';
            
            this.gridConfig.customFilter[this.gridConfig.colonne[col].NOME_COLONNA+'-filter'] = {
              COLORE_FILTRO : 'bg-green bg-color-palette',
              INPUT   : 'INPUT',
              };            
            
          } else if (
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'PART_NUMBER_COMPATIBILE') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'CODICE_SIRTI_COMPATIBILE') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'NOME_PART_NUMBER_COMPATIBILE') ||
                     (this.gridConfig.colonne[col].NOME_COLONNA === 'SAP_CODE_COMPATIBILE')
                    ) {
            this.gridConfig.colonne[col].IS_TIPO_ICONA      = 0;
            this.gridConfig.colonne[col].CUSTOM_ICON_NO     = '';
            this.gridConfig.colonne[col].CUSTOM_ICON_SI     = '';
            this.gridConfig.colonne[col].COLORE_CUSTOM      = 'bg-danger bg-color-palette';
            
            this.gridConfig.customFilter[this.gridConfig.colonne[col].NOME_COLONNA+'-filter'] = {
              COLORE_FILTRO : 'bg-danger bg-color-palette',
              INPUT   : 'INPUT',
              };            
            
          }
        }
        
        /* leggo i record delle equivalenze / compatibili */
        
        this.loaderGrid = true;
        
        /* richiamo la rotta dei compatibili */
        const qParamsMateriali = {
            RICHIEDENTE       : this.richiedente,
            CODICE_SIRTI      : this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.CODICE_SIRTI,
            PART_NUMBER       : this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.PART_NUMBER,
            SAP_CODE          : this.reactiveFormCercaMaterialePrincipale.value.materialePrincipaleCheck.SAP_CODE,
            PROGETTO          : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROGETTO,
            SOTTOPROGETTO     : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.SOTTOPROGETTO,
            PROPRIETA         : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.PROPRIETA,
            CLIENTE_FINALE    : this.reactiveFormCercaMaterialePrincipale.value.quadruplaMatPrincipaleCheck.CLIENTE_FINALE             
          };
          
        this.gestScorteServices.getCompatibiliEquivalenti(qParamsMateriali).subscribe(( result )=>{
          
          console.debug('sono CompatibiliEquivalentiComponent => MATERIALE_COMPATIBILE => selectedItemMateriale => getCompatibiliEquivalenti',result);
          
          this.gridConfig.record       = result.data.results;
         
          this.gridConfig.recordSelected = [];
          
          this.loaderGrid = false; 
          
        });
        
        
        
      } else {
        this.loaderGrid = false; 
      }
      
    });
    
  }
  
  
  public reloadOut(event: any):void {
    
    console.debug('sono CompatibiliEquivalentiComponent => reloadOut ', event );

    this.clickButtonCreaModifica('CERCA');
  }
  
  public getRecordSelezionato(rowSelected: any):void {
    this.gridConfig.recordSelected = [];
    this.gridConfig.mostraConfermaElimina = false;
    //console.debug('sono CompatibiliEquivalentiComponent => getRecordSelezionato ', rowSelected );
    if ((rowSelected.selected) && (rowSelected.selected.length > 0) ) {
      console.debug('sono CompatibiliEquivalentiComponent => getRecordSelezionato => record selezionati: ', rowSelected.selected );
      this.gridConfig.recordSelected = rowSelected.selected;
      /* abilito il bottono di cancellazione */
    } else {
      console.debug('sono CompatibiliEquivalentiComponent => getRecordSelezionato => nessun record selezionato');
    }
    
  }
  
  public focusOut(event: any):void {
    //console.debug('sono CompatibiliEquivalentiComponent => focusOut: ', event);

  }
  
	/* click del bottone che elimina le compatibilita selezionate nella grid */
	public clickEliminaCompatibilita():void {
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
		
		/* chiedo la conferma definitiva */
		
		this.gridConfig.mostraConfermaElimina = true;
		
	};
  
	/* click del bottone che elimina le compatibilita selezionate nella grid */
	public clickAnnullaEliminaCompatibilita():void {
		
		/* chiedo la conferma definitiva */
    
    this.msgAzioneObj = {
      
      mostraEsitoAzione    : false,      
      tipoEsitoEsitoAzione : '',        
      msgEsitoAzione       : ''      
      
    };
		
		this.gridConfig.mostraConfermaElimina = false;
    
    
		
	};
  
	/* click del bottone che elimina le compatibilita selezionate nella grid */
	public clickConfermaEliminaCompatibilita():void {
		
    this.loaderGrid = true;
    let obsevableResponseArray:any = [];
    
    if ((this.gridConfig.recordSelected) && (this.gridConfig.recordSelected.length > 0 ) ) {
      
      for (let colRs in this.gridConfig.recordSelected) {
        
        let recordSelected = this.gridConfig.recordSelected[colRs];
        
        let paramChiamataAZ = {
          
          CODICE_SIRTI					        :	null,
          PART_NUMBER						        :	null,
          SAP_CODE 						          :	null,
          NOME_PART_NUMBER				      :	null,
          CODICE_SIRTI_COMPATIBILE    	:	null,
          PART_NUMBER_COMPATIBILE     	:	null,
          SAP_CODE_COMPATIBILE        	:	null,
          NOME_PART_NUMBER_COMPATIBILE	:	null,				
          EQUIVALENTE						        :	null,
          PROGETTO                      :	null,
          SOTTOPROGETTO                 :	null,
          PROPRIETA                     :	null,
          CLIENTE_FINALE                :	null,
          RICHIEDENTE                   : this.richiedente,
          ID_MCM                        : recordSelected.ID_MCM,				
        };
        
        console.debug('sono CompatibiliEquivalentiComponent => clickConfermaEliminaCompatibilita',paramChiamataAZ);
        obsevableResponseArray.push(this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ));
        
        /*this.gestScorteServices.creaCompatibileEquivalente(paramChiamataAZ).subscribe(( resultAZ )=> {
          
          console.debug('sono CompatibiliEquivalentiComponent => eliminaCompatibileEquivalente => esito',resultAZ);
                 
          obsevableResponseArray.push({
            ESITO       : resultAZ.data.ESITO,
            DESC_ESITO  : resultAZ.data.DESC_ESITO,
            RECORD      : recordSelected
          });

        }, ( err )=>{
          console.debug('sono CompatibiliEquivalentiComponent => creaCompatibileEquivalente => Fallita elimina => KO =>err: ',err);

          obsevableResponseArray.push({
            ESITO       : 'KO',
            DESC_ESITO  : 'Fallita elimina: '+err,
            RECORD      : recordSelected
          });
        
          
        });*/
      }
      
      
      forkJoin(obsevableResponseArray).subscribe(resultFj => {
        
        console.debug('sono CompatibiliEquivalentiComponent => clickConfermaEliminaCompatibilita => forkJoin',resultFj);
        
        let contaKo = 0;
        let contaOk = 0;
        
        for (let colResp in resultFj) {

          let esitoObj = resultFj[colResp]; 
          
          if ((esitoObj as any).data.ESITO === 'KO') {
            contaKo = contaKo + 1;
          } else {
            contaOk = contaOk + 1;
          }
          
        }
       
        if (resultFj.length == contaOk) {
          
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'SUCCESS',        
            msgEsitoAzione       : contaOk+'/'+resultFj.length+' compatibilita / equivalenze selezionate sono state eliminate con successo'      
            
          };
          
        } else if (resultFj.length == contaKo) {
          
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'ERROR',        
            msgEsitoAzione       : 'Impossibile eliminare '+contaKo+'/'+resultFj.length+' compatibilita / equivalenze selezionate'      
            
          };
          
        } else {
          
          this.msgAzioneObj = {
            
            mostraEsitoAzione    : true,      
            tipoEsitoEsitoAzione : 'WARNING',        
            msgEsitoAzione       : 'Solo '+contaOk+'/'+resultFj.length+' compatibilita / equivalenze selezionate sono state eliminate con successo. Le restanti '+contaKo+' sono fallite'      
            
          };
          
        }
        
        //this.loaderGrid = false;
        this.mostraBottoniTipoCompatibilita			= true;
        this.msgConfermaTipoCompatibilitaSelezionata	= '';
        this.tipoCompatibilitaSelected				= null;
        
        this.clickAnnullaNuovaCompatibilitaButton();
        

        this.getGridCompatibiliEquivalenti();
        
      });      
     

            
    }

	};
  


}
