import { Component, OnInit ,Input,Output,EventEmitter,Injectable} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';


/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'app-azione-rientro-a-magazzino',
  templateUrl: './azione-rientro-a-magazzino.component.html',
  styleUrls: ['./azione-rientro-a-magazzino.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider
})
export class AzioneRientroAMagazzinoComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzRientroAMagazzino: FormGroup;
  public aOptionValueResoPresso:any = [];
  public aOptionDescGuasto:any = [];
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;
  
  public markDisabled;
  public calendar: NgbCalendar;
  


  constructor(private formBuilderAzRientroAMagazzino: FormBuilder,
              calendar: NgbCalendar,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) {
    this.calendar = calendar;
    
    }
    
  disableDays() {
      this.markDisabled = (date: NgbDate) => {
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.datiAzioneObj.presidioMagazWind);
        let ritorno = (this.calendar.getWeekday(date) >= 6);
        if(this.datiAzioneObj.presidioMagazWind) {
          if(this.datiAzioneObj.presidioMagazWind.LUNEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 1);
          }
          if(this.datiAzioneObj.presidioMagazWind.MARTEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 2);
          }
          if(this.datiAzioneObj.presidioMagazWind.MERCOLEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 3);
          }
          if(this.datiAzioneObj.presidioMagazWind.GIOVEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 4);
          }
          if(this.datiAzioneObj.presidioMagazWind.VENERDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 5);
          }
        }
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',ritorno);
        return ritorno;
      };
       //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.markDisabled);
    
  }

  ngOnInit(): void {
    
    console.debug('sono AzioneRientroAMagazzinoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzRientroAMagazzino  in base al tipo materiale */
    
    if (this.datiAzioneObj.materiale.SERIAL_NUMBER && this.datiAzioneObj.stato.toUpperCase() !== 'MATERIALE_SU_MOBILE') { /* SERIAL_NUMBER  e stano != MATERIALE_SU_MOBILE */
      
      this.reactiveFormAzRientroAMagazzino = this.formBuilderAzRientroAMagazzino.group({
        nQuantitaCheck                : [1,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1),Validators.max(parseInt(this.datiAzioneObj.materiale.QUANTITA))]],
        nNoteGuastoCheck              : [null],
        sRestituzioneResoPressoCheck  : [null,[Validators.required]],
        sDataRitiroAppuntamentoCheck  : [null,[Validators.required]],
        sOraRitiroAppuntamentoCheck   : [null,[Validators.required,this.changeTime.bind(this)]],
        sStatoMaterialeCheck          : ['GUASTO'],
        sDescGuastoCheck              : [null,[Validators.required]],
      });
      
      
      this.aOptionValueResoPresso = [];
      
      /* se SEDE WIND allora forzo a SEDE WIND come unico valore */
      if (this.datiAzioneObj.materiale.RESO_PRESSO_GLS.toUpperCase() === 'SEDE WIND') {
        this.aOptionValueResoPresso = ['SEDE WIND'];
        this.reactiveFormAzRientroAMagazzino.get("sRestituzioneResoPressoCheck").setValue('SEDE WIND', {onlySelf: false});
      } else {
        /* else allora forzo a PUNTO GLS ma permetto la selezine */
        this.aOptionValueResoPresso = ['PUNTO GLS','SEDE WIND'];
        this.reactiveFormAzRientroAMagazzino.get("sRestituzioneResoPressoCheck").setValue('PUNTO GLS', {onlySelf: false});
        
      }
      
      
      this.aOptionDescGuasto = this.datiAzioneObj.descrizioneGuasto;
      
    
      
  
      
      this.reactiveFormAzRientroAMagazzino.get("sStatoMaterialeCheck").valueChanges.subscribe(value => {
        if (this.datiAzioneObj.materiale.SERIAL_NUMBER && value.toUpperCase() === 'GUASTO') {
          this.reactiveFormAzRientroAMagazzino.get("sDescGuastoCheck").setValidators([Validators.required]);
          this.reactiveFormAzRientroAMagazzino.get("sDescGuastoCheck").setValue('', {onlySelf: false});
        } else {
          this.reactiveFormAzRientroAMagazzino.get("sDescGuastoCheck").clearValidators();
        }
        this.reactiveFormAzRientroAMagazzino.get("sDescGuastoCheck").updateValueAndValidity({ emitEvent : false });
        //console.log(this.reactiveFormAzRientroAMagazzino.valid);
      });
      
      this.disableDays();

    } else { /* CONSUMABILE  o stato == MATERIALE_SU_MOBILE*/
      this.reactiveFormAzRientroAMagazzino = this.formBuilderAzRientroAMagazzino.group({
        nQuantitaCheck                : [parseInt(this.datiAzioneObj.materiale.QUANTITA),[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1),Validators.max(parseInt(this.datiAzioneObj.materiale.QUANTITA))]],
        nNoteGuastoCheck              : [null],
      });
      
      
      
    }
    
    
    /* gestisco il multistep */
    if (this.datiAzioneObj.multiStep) {
      this.reactiveFormAzRientroAMagazzino.addControl('sEsitoConsegnaCheck', this.formBuilderAzRientroAMagazzino.control(null, [Validators.required]));
      this.reactiveFormAzRientroAMagazzino.addControl('sDataConsegnaCheck', this.formBuilderAzRientroAMagazzino.control(null, [Validators.required]));
      this.reactiveFormAzRientroAMagazzino.addControl('sOraConsegnaCheck', this.formBuilderAzRientroAMagazzino.control(null, [Validators.required]));
      
    }

    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    

    
  }  
 
  public eseguiAzione():void {
    console.debug('sono AzioneRientroAMagazzinoComponent => eseguiAzione: ',this.reactiveFormAzRientroAMagazzino.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente,
      ID_ATTIVITA             : this.datiAzioneObj.materiale.ID,
      QUANTITA             	  : this.reactiveFormAzRientroAMagazzino.value.nQuantitaCheck               ? this.reactiveFormAzRientroAMagazzino.value.nQuantitaCheck : null,    
      AZIONE             		  : 'RIENTRO A MAGAZZINO',    
      CAUSALE_RIFIUTO         : null,      
      MOTIVO_RIFIUTO          :	null,      
      TIPOLOGIA_GUASTO        : null,      
      NOTE_GUASTO        		  : this.reactiveFormAzRientroAMagazzino.value.nNoteGuastoCheck             ? this.reactiveFormAzRientroAMagazzino.value.nNoteGuastoCheck : null,    
      TIPO_RICHIESTA_RESO     : 'RESO',      
      CAUSALE_RESO     		    : this.reactiveFormAzRientroAMagazzino.value.sStatoMaterialeCheck         ? this.reactiveFormAzRientroAMagazzino.value.sStatoMaterialeCheck : null,
      TIPOLOGIA_GUASTO_RESO   : this.reactiveFormAzRientroAMagazzino.value.sDescGuastoCheck             ? this.reactiveFormAzRientroAMagazzino.value.sDescGuastoCheck : null,     
      DDT_RESO   				      : null,
      SENZA_RESTIT_RESO   	  : null,    
      NODO_PROVENIENZA_RESO   : null,      
      DATA_APPRIT_RESO        : this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck ? this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck+' '+this.reactiveFormAzRientroAMagazzino.value.sOraRitiroAppuntamentoCheck : null,   
      VINC_CONS_CONC_RESO     : 'NO',      
      RESO_PRESSO_GLS         : this.reactiveFormAzRientroAMagazzino.value.sRestituzioneResoPressoCheck ? this.reactiveFormAzRientroAMagazzino.value.sRestituzioneResoPressoCheck : null,    
      MULTI_STEP              : this.datiAzioneObj.multiStep                                              ? 'SI' : 'NO',
      ESITO_CONSEGNA          : this.reactiveFormAzRientroAMagazzino.value.sEsitoConsegnaCheck          ? this.reactiveFormAzRientroAMagazzino.value.sEsitoConsegnaCheck : null,
      DATA_CONSEGNA           : this.reactiveFormAzRientroAMagazzino.value.sDataConsegnaCheck           ? this.reactiveFormAzRientroAMagazzino.value.sDataConsegnaCheck+' '+this.reactiveFormAzRientroAMagazzino.value.sOraConsegnaCheck : null,
    };
    
    this.gestScorteServices.materialeUtilizzatoRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneRientroAMagazzinoComponent => materialeUtilizzatoRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo. Creato il Reso: '+data.data.ID_ATT_RESO;
        this.outReload.emit(true);
        
      }

    });
    
  }
  
  changeResoPressoValue(e) {
    e.stopPropagation();
    this.reactiveFormAzRientroAMagazzino.get("sRestituzioneResoPressoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneRientroAMagazzinoComponent => changeResoPressoValue',this.reactiveFormAzRientroAMagazzino.value.sRestituzioneResoPressoCheck);

   }
   
  changeDescGuastoValue(e) {
    e.stopPropagation();
    this.reactiveFormAzRientroAMagazzino.get("sDescGuastoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneRientroAMagazzinoComponent => changeDescGuastoValue',this.reactiveFormAzRientroAMagazzino.value.sDescGuastoCheck);

   }
   
   

   
  public onDateSelect(dateSelected,sChiamante) {
    console.debug('sono AzioneRientroAMagazzinoComponent => onDateSelect',dateSelected);
    if (sChiamante.toUpperCase() === 'DATA_CONSEGNA') { /* DATA_CONSEGNA */ 
      this.reactiveFormAzRientroAMagazzino.get("sOraConsegnaCheck").setValue('00:00:00', {onlySelf: false});
    } else { /* DATA_RITIRO_APPUNTAMENTO */ 
      this.reactiveFormAzRientroAMagazzino.get("sOraRitiroAppuntamentoCheck").setValue(this.datiAzioneObj.orario.min, {onlySelf: false});
    }
    
  }
  
  public changeTime(timeSelected) {
    console.debug('sono AzioneRientroAMagazzinoComponent => changeTime',timeSelected);
    /*
     if (timeSelected.touched && !this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
     */ 
     if (timeSelected.touched && !timeSelected.value && this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
      
     if (timeSelected.touched && timeSelected.value && !this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
    
    
     if (timeSelected.value) {
      const aTimeSelected = timeSelected.value.split(':');
      const dataRipApp = this.reactiveFormAzRientroAMagazzino.value.sDataRitiroAppuntamentoCheck;
      

      
      const sDataSelected = dataRipApp+' '+timeSelected.value;

      let dateParts = sDataSelected.substring(0, 10).split("-");
      let oreMinParts = sDataSelected.substring(11, 19).split(":");


      

      const dateNew = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => DATA SELECTED',sDataSelected,dateNew);
      
      
      const sDataMin = dataRipApp+' '+this.datiAzioneObj.orario.min;

      dateParts = sDataMin.substring(0, 10).split("-");
      oreMinParts = sDataMin.substring(11, 19).split(":");

     

      const dateMin = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
       console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => DATA MIN',sDataMin,dateMin);
      
      const sDataMax = dataRipApp+' '+this.datiAzioneObj.orario.max;

      dateParts = sDataMax.substring(0, 10).split("-");
      oreMinParts = sDataMax.substring(11, 19).split(":");

     

      const dateMax = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => DATA MAX',sDataMax,dateMax);
      
      
      if (dateNew < dateMin) {
       console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => ERROR A < MIN ',dateNew,dateMin);
       //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
       return {errorMinMax:true};
       //this.reactiveFormAzRientroAMagazzino.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMin.substring(11, 19), {onlySelf: false});
      } else {
        if (dateNew > dateMax) {
         console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => ERROR A > MAX ',dateNew,dateMax);
         //this.reactiveFormAzRientroAMagazzino.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMax.substring(11, 19), {onlySelf: false});
         //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
         return {errorMinMax:true};
        } else {
          console.debug('sono AzioneRientroAMagazzinoComponent => changeTime => VALORE ACCETTATO:  MIN <= A <= MAX ',dateNew,dateMin,dateMax);
          //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
          return false;
        }
      }    
 
   }
  }

}

