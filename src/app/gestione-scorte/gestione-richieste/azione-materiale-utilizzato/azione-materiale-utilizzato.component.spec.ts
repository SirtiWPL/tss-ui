import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneMaterialeUtilizzatoComponent } from './azione-materiale-utilizzato.component';

describe('AzioneMaterialeUtilizzatoComponent', () => {
  let component: AzioneMaterialeUtilizzatoComponent;
  let fixture: ComponentFixture<AzioneMaterialeUtilizzatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneMaterialeUtilizzatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneMaterialeUtilizzatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
