import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-materiale-rifiutato-logistica-wind',
  templateUrl: './azione-materiale-rifiutato-logistica-wind.component.html',
  styleUrls: ['./azione-materiale-rifiutato-logistica-wind.component.css']
})
export class AzioneMaterialeRifiutatoLogisticaWindComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzMaterialeRifiutatoLogisticaWind: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzMaterialeRifiutatoLogisticaWind: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService  
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneMaterialeRifiutatoLogisticaWindComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzMaterialeRifiutatoLogisticaWind */

    this.reactiveFormAzMaterialeRifiutatoLogisticaWind = this.formBuilderAzMaterialeRifiutatoLogisticaWind.group({
      sMotivoCheck       : [null,[Validators.required]],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneMaterialeRifiutatoLogisticaWindComponent => eseguiAzione: ',this.reactiveFormAzMaterialeRifiutatoLogisticaWind.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
      PART_NUMBER     : this.datiAzioneObj.materiale.PART_NUMBER,
      ID_SIRTI        : this.datiAzioneObj.materiale.ID_SIRTI,
      MOTIVO          : this.reactiveFormAzMaterialeRifiutatoLogisticaWind.value.sMotivoCheck ? this.reactiveFormAzMaterialeRifiutatoLogisticaWind.value.sMotivoCheck : null
    };
    
    this.gestScorteServices.fornituraMaterialeRifiutatoLogisticaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneMaterialeRifiutatoLogisticaWindComponent => fornituraMaterialeRifiutatoLogisticaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}