import { Component, OnInit ,Input,Output,EventEmitter,Injectable} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}


@Component({
  selector: 'app-azione-approvato',
  templateUrl: './azione-approvato.component.html',
  styleUrls: ['./azione-approvato.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider
})
export class AzioneApprovatoComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzApprovato: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;
  

  constructor(private formBuilderAzApprovato: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneApprovatoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzApprovato */

    this.reactiveFormAzApprovato = this.formBuilderAzApprovato.group({
      nIdAttRmaCheck           : [null,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1)]],
      nIdSirtiCheck            : [null,[Validators.required]],
      nQuantitaCheck           : [1,[Validators.required,Validators.pattern('^[1-9]+[0-9]*$'),Validators.min(1),Validators.max(parseInt(this.datiAzioneObj.materiale.QUANTITA))]],
      sDataPrevistaConsCheck   : [null],
      sOraPrevistaConsCheck    : [null,[this.changeTime.bind(this)]],
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    /* pre-compilo i campi edit previsti */
    
    if (this.datiAzioneObj.materiale.ID_SIRTI) { /* se ID_SIRTI valorizzato allora prendo il valore */
      this.reactiveFormAzApprovato.get("nIdSirtiCheck").setValue(this.datiAzioneObj.materiale.ID_SIRTI, {onlySelf: false});  
    } else {
      this.reactiveFormAzApprovato.get("nIdSirtiCheck").setValue(0, {onlySelf: false}); /* lo forzo a zero */
    }
    
    if (this.datiAzioneObj.tipo === 'APPROVAZIONE_HQ') { /* se il chiamante e'  APPROVAZIONE HQ valorizzo l'edit con ID_ATTIVITA_RMA */
      this.reactiveFormAzApprovato.get("nIdAttRmaCheck").setValue(this.datiAzioneObj.materiale.ID_ATTIVITA_RMA, {onlySelf: false});  
    }
    
    
    if (this.datiAzioneObj.materiale.SERIAL_NUMBER) {
      this.reactiveFormAzApprovato.get("nQuantitaCheck").setValue(parseInt(this.datiAzioneObj.materiale.QUANTITA), {onlySelf: false});
    } else {
      this.reactiveFormAzApprovato.get("nQuantitaCheck").setValue(0, {onlySelf: false});
    }
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneApprovatoComponent => eseguiAzione: ',this.reactiveFormAzApprovato.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente                            ? this.datiAzioneObj.richiedente                    : null,
      ID_ATT_RMA              : this.reactiveFormAzApprovato.value.nIdAttRmaCheck         ? this.reactiveFormAzApprovato.value.nIdAttRmaCheck : null,
      ID_SIRTI                : this.reactiveFormAzApprovato.value.nIdSirtiCheck          ? this.reactiveFormAzApprovato.value.nIdSirtiCheck  : null,
      QUANTITA                : this.reactiveFormAzApprovato.value.nQuantitaCheck         ? this.reactiveFormAzApprovato.value.nQuantitaCheck : null,
      DATA_PREVISTA_CONSEGNA  : this.reactiveFormAzApprovato.value.sDataPrevistaConsCheck ? this.reactiveFormAzApprovato.value.sDataPrevistaConsCheck+' '+this.reactiveFormAzApprovato.value.sOraPrevistaConsCheck : null
    };
    
    this.gestScorteServices.approvatoRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneApprovatoComponent => approvatoRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }
  
  public onDateSelect(dateSelected) {
    console.debug('sono AzioneApprovatoComponent => onDateSelect',dateSelected);
    this.reactiveFormAzApprovato.get("sOraPrevistaConsCheck").setValue('00:00:00', {onlySelf: false});
  }
  
  public changeTime(timeSelected) {
    console.debug('sono AzioneApprovatoComponent => changeTime',timeSelected);
  }
}
