import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneConsegnaAccettataLogisticaWindComponent } from './azione-consegna-accettata-logistica-wind.component';

describe('AzioneConsegnaAccettataLogisticaWindComponent', () => {
  let component: AzioneConsegnaAccettataLogisticaWindComponent;
  let fixture: ComponentFixture<AzioneConsegnaAccettataLogisticaWindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneConsegnaAccettataLogisticaWindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneConsegnaAccettataLogisticaWindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
