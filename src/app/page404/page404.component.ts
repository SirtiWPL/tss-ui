import { Component, OnInit } from '@angular/core';
import { UiService } from './../ui-service.service';
import { RouterModule, Routes,ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.css']
})
export class Page404Component implements OnInit {
  isAuthenticationLoader : boolean;
  error404:boolean;
  constructor(private uiService: UiService,private route:ActivatedRoute,private router: Router) {




    }

  ngOnInit(): void {
    this.isAuthenticationLoader = true;
    this.error404 = false;
    console.log('canActivate - Page404Component 1');
    //this.uiService.configService().subscribe(( data )=>{
      this.uiService.getUserInfo().subscribe(( data )=>{
        console.log('canActivate - Page404Component 2',data);
        this.isAuthenticationLoader = false;
        if (this.uiService) {
          this.router.navigateByUrl('home');
        } else {
          this.error404 = true;
        }


        }, ( err )=>{
            console.debug('canActivate - Page404Component 3');
            this.router.navigateByUrl('login');
            });
    //});


  }

}
