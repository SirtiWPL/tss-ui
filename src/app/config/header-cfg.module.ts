import { iHeaderDataModule } from '../admin-lte/header-page/header-data.interface'

export class HeaderCfgModule {
  
		public headerCfg: iHeaderDataModule = {
      cssStyleColorNavBar : 'navbar-orange',
      cssStyleTextNavBar  : 'navbar-dark',
      nomeOperatore       : '',
      cognomeOperatore    : '',
      nascondiMenu        : false,
      mostraLogo          : false
		};
  
};  
