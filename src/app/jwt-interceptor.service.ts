import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UiService } from './ui-service.service';

import { CookieService } from 'ngx-cookie-service';

const JWT                = environment.JWT;
const injectXWebAuthUser = environment.injectXWebAuthUser;
const xWebAuthUser       = environment.xWebAuthUser;
const authType           = environment.authType; /* per distinguere tra JWT O AUTH_PROXY */

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  private jwtToken           = JWT;
  private injectXWebAuthUser = injectXWebAuthUser;
  private xWebAuthUser       = xWebAuthUser;
  private authType           = authType; /* per distinguere tra JWT O AUTH_PROXY */

  //private jwtToken = this.uiService.getJwtToken;

  private cookieValue : string;

  constructor(
    private uiService: UiService,
    private cookieService: CookieService
  ) {

    //console.log( this.uiService.getJwtToken.getTokenJWT );
    //this.jwtToken = this.uiService.getJwtToken.jwtToken;

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = this.uiService.getJwtToken;

    const skipIntercept = request.headers.has('skipAuthorization');



    console.debug('JwtInterceptorService');
    //console.debug(currentUser);
    /*if (currentUser) {*/
      //request = request.clone({
      //    setHeaders: {
              //Authorization: `JWT ${currentUser.jwtToken}`
              //Authorization: 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiUk9PVCIsImV4cCI6MTYzMzM2NjUyOCwibmJmIjoxNjIyNzExMjk4LCJpYXQiOjE2MjI3MTEyOTgsIndvX3Npc3RlbWFfZXN0ZXJubyI6IlRLX1NFXzEyMyIsIndvX2dlb2NhbGwiOiJUS19HRU9DQUxMXzEyMyJ9.2YO2B8YwsjbrGuw2tL1mp52dNU32JDVurnx-Q7Q4l20vdj4-PbFh5ayjcic0au2yioV3xHz74zooGkA8GFkMWw'
       //   }
     // });
    /*}*/

    //
    //if (skipIntercept) {
    //  request = request.clone({
    //      headers: request.headers.delete('Authorization').delete('skipAuthorization')
    //  });
    //}


    // add authorization header with jwt token if available

    if ((injectXWebAuthUser) && (this.authType === 'AUTH_PROXY')) {
      request = request.clone({
        setHeaders: {
          //Authorization: `JWT ${this.jwtToken}`,
          "x-webauth-user" : xWebAuthUser,
          "Content-Type":  "application/json"
        }
      });
    }

    if (this.authType === 'AUTH_PROXY') { /* modificato solo per AUTH_PROXY */
      this.cookieValue = this.cookieService.get('geocall');
      console.log(this.cookieService.get('geocall'));
      console.log('JWT INTERCEPTOR Cookie GEOCALL: '+ this.cookieValue);
  
      if (this.cookieValue != null) {
        request = request.clone({
          setHeaders: {
            'x-wind-jwt'  : this.cookieValue
          }
        });
      }      
    }
    
    if (this.authType === 'JWT') { /* modificato solo per JWT */

    if (currentUser) {
      request = request.clone({
          setHeaders: {
              Authorization: `JWT ${currentUser.jwtToken}`
              //Authorization: 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiUk9PVCIsImV4cCI6MTYzMzM2NjUyOCwibmJmIjoxNjIyNzExMjk4LCJpYXQiOjE2MjI3MTEyOTgsIndvX3Npc3RlbWFfZXN0ZXJubyI6IlRLX1NFXzEyMyIsIndvX2dlb2NhbGwiOiJUS19HRU9DQUxMXzEyMyJ9.2YO2B8YwsjbrGuw2tL1mp52dNU32JDVurnx-Q7Q4l20vdj4-PbFh5ayjcic0au2yioV3xHz74zooGkA8GFkMWw'
          }
     });
    }
      
    }
    


    return next.handle(request);
  }

}
