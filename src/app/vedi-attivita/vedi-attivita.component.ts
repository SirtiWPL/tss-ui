
import {AfterViewInit, Component,ViewEncapsulation, ViewChild,ChangeDetectorRef,Input,OnInit,Output,EventEmitter,TemplateRef } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { UiService } from './../ui-service.service';
import { VediAttivitaService } from './../vedi-attivita.service';
//import * as fileSaver from 'file-saver';
import {saveAs as importedSaveAs} from "file-saver";



@Component({
  selector: 'app-vedi-attivita',
  templateUrl: './vedi-attivita.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./vedi-attivita.component.css']
})
export class VediAttivitaComponent implements OnInit {
  
  /* input params grid */
  @Input() idAttivita: string;
  @Input() cssStyle:any;
  
  @Output() outClose = new EventEmitter<any>();
  
  public closeResult:string = '';
  public activeTabStoriaActRelazioni:number = 1;
  public activeTabDatiTencniciAllegati:number = 1;
  public richiedente:string = 'INTERNO';
  public vediAttivitaObj:any = {
    loader            : false,
    loaderDtaAllegati : false,
    loaderStoriaAct   : false,
    infoPrincipali    : [],
    storiaAttivita    : [],
    relazioni         : [],
    dta               : [],
    allegati          : [],
    azioni            : [],
    isSelected        : false
    }; 
  
  
  @ViewChild('content') modalContent: TemplateRef<any>;

  constructor(private modalService: NgbModal,
              private uiService: UiService,
              private vediAttivitaService: VediAttivitaService) {

      

  }
  
  ngOnInit(): void {
    setTimeout(() => {
      this.showModal();
    }, 0);
    
    this.caricaVediAttivita();
    
  }
  
  
  public caricaVediAttivita():void {
    
    //this.idAttivita = '24599148';
    
    this.vediAttivitaObj.loader         = true;
    
    /*reset di tutti o box */
    this.vediAttivitaObj.infoPrincipali = [];
    this.vediAttivitaObj.storiaAttivita = [];
    this.vediAttivitaObj.relazioni      = [];
    this.vediAttivitaObj.azioni         = [];
    this.vediAttivitaObj.dta            = [];
    this.vediAttivitaObj.allegati       = [];
    
    const qParamsGetVediAttAttivita = {
      RICHIEDENTE     : this.richiedente,
      ID_ATTIVITA     : this.idAttivita,
    };
    
    console.debug('sono VediAttivitaComponent => caricaVediAttivita => qParamsGetVediAttAttivita: ', qParamsGetVediAttAttivita);
    
    this.vediAttivitaService.getVediAttAttivita(qParamsGetVediAttAttivita).subscribe(( data )=>{
      
      console.debug('sono VediAttivitaComponent => caricaVediAttivita => getVediAttAttivita: ', data);
      //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
      this.vediAttivitaObj.infoPrincipali = data.data.results ? data.data.results : [];

      
      //console.debug('sono VediAttivitaComponent => showModal => qParamsGetVediAttAttivita: ', qParamsGetVediAttAttivita);
      
      this.vediAttivitaService.getVediAttStoria(qParamsGetVediAttAttivita).subscribe(( data )=>{
        
        console.debug('sono VediAttivitaComponent => caricaVediAttivita => getVediAttStoria: ', data);
        //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
        this.vediAttivitaObj.storiaAttivita = data.data.results ? data.data.results : [];
        
        for (let cSa in this.vediAttivitaObj.storiaAttivita) {
          this.vediAttivitaObj.storiaAttivita[cSa].IS_SELECTED = false;
        }

        this.vediAttivitaService.getVediAttRelazioni(qParamsGetVediAttAttivita).subscribe(( data )=>{
          
          console.debug('sono VediAttivitaComponent => caricaVediAttivita => getVediAttRelazioni: ', data);
          //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
          this.vediAttivitaObj.relazioni = data.data.results ? data.data.results : [];

          this.vediAttivitaService.getVediAttAzioni(qParamsGetVediAttAttivita).subscribe(( data )=>{
            
            console.debug('sono VediAttivitaComponent => caricaVediAttivita => getVediAttAzioni: ', data);
            //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
            this.vediAttivitaObj.azioni = data.data.results ? data.data.results : [];
  
            this.activeTabStoriaActRelazioni    = 1;
            this.activeTabDatiTencniciAllegati  = 1;
            this.vediAttivitaObj.isSelected     = false;
            this.vediAttivitaObj.loader         = false;

          });  
          
          
          
        });          
        
        
      });       
      
    });      
  }

  showModal() {   
    
    
    
    this.modalService.open(this.modalContent,{
                            size              : this.cssStyle.size,
                            scrollable        : true
                            }).result.then((result) => {
      
      

      
      this.closeResult = `Closed with: ${result}`;
      this.outClose.emit(this.closeResult);
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.outClose.emit(this.closeResult);
    });
    
    
    
    
    
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
  public apriDtaTransazione(itemStoriaAttivita:any):void {
    this.vediAttivitaObj.loaderDtaAllegati = true;
    console.debug('sono VediAttivitaComponent => apriDtaTransazione => itemStoriaAttivita: ', itemStoriaAttivita);
    
    /* riprristino tutti IS_SELECTED  a false */
    
    for (let cSa in this.vediAttivitaObj.storiaAttivita) {
      this.vediAttivitaObj.storiaAttivita[cSa].IS_SELECTED = false;
    }
    
    itemStoriaAttivita.IS_SELECTED = true; /* metto a true solo il selezionato */
    this.vediAttivitaObj.isSelected = true;

    const qParamsGetVediDTA = {
      RICHIEDENTE       : this.richiedente,
      ID_ATTIVITA       : this.idAttivita,
      DATA_ESECUZIONE   : itemStoriaAttivita.DATA_ESECUZIONE,
    };
    
    this.vediAttivitaService.getVediAttDTA(qParamsGetVediDTA).subscribe(( data )=>{
      
      console.debug('sono VediAttivitaComponent => showModal => getVediAttDTA: ', data);
      //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
      this.vediAttivitaObj.dta = data.data.results ? data.data.results : [];

      this.vediAttivitaService.getVediAttAllegati(qParamsGetVediDTA).subscribe(( data )=>{
        
        console.debug('sono VediAttivitaComponent => showModal => getVediAttAllegati: ', data);
        //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
        this.vediAttivitaObj.allegati = data.data.results ? data.data.results : [];
        this.vediAttivitaObj.loaderDtaAllegati = false;
      });      
      
      
    });      
    
    
  }
  
  public resetStoriaAttivita():void {
    
    this.vediAttivitaObj.loaderStoriaAct    = true;
    this.vediAttivitaObj.loaderDtaAllegati  = true;
    
    const qParamsGetVediAttAttivita = {
      RICHIEDENTE     : this.richiedente,
      ID_ATTIVITA     : this.idAttivita,
    };
    
    this.vediAttivitaService.getVediAttStoria(qParamsGetVediAttAttivita).subscribe(( data )=>{
      
      console.debug('sono VediAttivitaComponent => caricaVediAttivita => getVediAttStoria: ', data);
      //console.debug('GestioneRichiesteComponent => Richieste Operatore: ', data );
      this.vediAttivitaObj.storiaAttivita = data.data.results ? data.data.results : [];
    
      /* riprristino tutti IS_SELECTED  a false */
      
      for (let cSa in this.vediAttivitaObj.storiaAttivita) {
        this.vediAttivitaObj.storiaAttivita[cSa].IS_SELECTED = false;
      }
      
      this.vediAttivitaObj.dta            = [];
      this.vediAttivitaObj.allegati       = [];
      this.vediAttivitaObj.isSelected     = false;
      this.activeTabDatiTencniciAllegati  = 1;
      
      this.vediAttivitaObj.loaderStoriaAct    = false;
      this.vediAttivitaObj.loaderDtaAllegati  = false;
      
    });
    
  }
  
  
  public apriRelazione(itemRelazioni:any):void {
    console.debug('sono VediAttivitaComponent => apriRelazione => itemRelazioni: ', itemRelazioni);
    this.idAttivita = itemRelazioni.ID_ATTIVITA;
    this.caricaVediAttivita();
  }
  
  
  public downloadFileAllegato(itemAllegato:any):void {
    //this.fileService.downloadFile().subscribe(response => {
    const qParamsDlFile = {
      file_s : itemAllegato.NOME_FILE_SERVIZIO,
      file_c : itemAllegato.NOME_FILE_CLIENT
    };
    
		this.vediAttivitaService.downloadFileRest(qParamsDlFile).subscribe((response: any) => { //when you use stricter type checking
      console.debug('sono VediAttivitaComponent => downloadFileAllegato => downloadFileRest: ', response);
      
      //importedSaveAs(response.data, itemAllegato.NOME_FILE_CLIENT);
			let blob:any = new Blob([response]);
      importedSaveAs(blob, itemAllegato.NOME_FILE_CLIENT);
			//const url = URL.createObjectURL(blob);
			/*window.open(url);*/

			//window.location.href = response.url;
			//fileSaver.saveAs(blob);
		//}), error => console.log('Error downloading the file'),
		}), (error: any) => console.log('Error downloading the file'), //when you use stricter type checking
                 () => console.info('File downloaded successfully');
  }

}
