import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneCambioPrioritaComponent } from './azione-cambio-priorita.component';

describe('AzioneCambioPrioritaComponent', () => {
  let component: AzioneCambioPrioritaComponent;
  let fixture: ComponentFixture<AzioneCambioPrioritaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneCambioPrioritaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneCambioPrioritaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
