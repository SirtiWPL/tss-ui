import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneScorteComponent } from './gestione-scorte.component';

describe('GestioneScorteComponent', () => {
  let component: GestioneScorteComponent;
  let fixture: ComponentFixture<GestioneScorteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestioneScorteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneScorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
