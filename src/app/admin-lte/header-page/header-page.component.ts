import { Component, OnInit,Input,OnChanges } from '@angular/core';

import { iHeaderDataModule } from './header-data.interface';
import { iSidebarDataModule } from '../sidebar-page/sidebar-data.interface';
import { UiService } from './../../ui-service.service';


@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
  styleUrls: ['./header-page.component.css']
})
export class HeaderPageComponent implements OnInit,OnChanges {
  
  /* istanzio l'oggetto HeaderDataModule e lo rendo visibile */
  
  @Input() headerDataObj: iHeaderDataModule;
  @Input() sidebarDataObj: iSidebarDataModule;
  
  
  constructor(public uiService: UiService) {
    console.debug(this.uiService.getCurrentUser); /* profilo */
     
    }
    
  /* metodo per leggere la versione */
  getCssStyleColorNavBar(): string {
    return this.headerDataObj.cssStyleColorNavBar;
  }
  
  /* metodo per leggere la versione */
  getCssStyleTextNavBar(): string {
    return this.headerDataObj.cssStyleTextNavBar;
  }
  
  /* metodo per leggere la versione */
  getNascondiMenuNavBar(): boolean {
    return this.headerDataObj.nascondiMenu;
  }
  
  /* metodo per leggere la versione */
  getMostraLogoNavBar(): boolean {
    return this.headerDataObj.mostraLogo;
  }
  
  
  /* metodo per leggere la colore */
  getCssStyleColorSideBar(): string {
    return this.sidebarDataObj.cssStyleColorSideBar;
  }
  
  /* metodo per leggere la versione */
  getCssStyleLogoSideBar(): string {
    return this.sidebarDataObj.cssStyleLogoSideBar;
  }
  
  /* metodo per leggere la versione */
  getLogoImageClienteSideBar(): string {
    return this.sidebarDataObj.logoImageCliente;
  }
  
  /* metodo per leggere la versione */
  getLogoImageSirtiSideBar(): string {
    return this.sidebarDataObj.logoImageSirti;
  }
  
  /* metodo per leggere la versione */
  getNomeAppSideBar(): string {
    return this.sidebarDataObj.nomeApp;
  }
  
  /* metodo per leggere la versione */
  getMostraLogoCustomSideBar(): boolean {
    return this.sidebarDataObj.mostraLogoCustom;
  }

  ngOnInit(): void {
    
    this.headerDataObj.cssStyleColorNavBar    = (( this.headerDataObj.cssStyleColorNavBar === '') ? 'navbar-indigo' : this.headerDataObj.cssStyleColorNavBar );
    this.headerDataObj.cssStyleTextNavBar     = (( this.headerDataObj.cssStyleTextNavBar === '') ? 'navbar-dark' : this.headerDataObj.cssStyleTextNavBar );
    this.sidebarDataObj.cssStyleColorSideBar  = (( this.sidebarDataObj.cssStyleColorSideBar === '') ? 'sidebar-dark-indigo' : this.sidebarDataObj.cssStyleColorSideBar );
    this.sidebarDataObj.cssStyleLogoSideBar   = (( this.sidebarDataObj.cssStyleLogoSideBar === '') ? 'navbar-indigo' : this.sidebarDataObj.cssStyleLogoSideBar );
    this.sidebarDataObj.logoImageCliente      = (( this.sidebarDataObj.logoImageCliente === '') ? '' : this.sidebarDataObj.logoImageCliente );
    this.sidebarDataObj.logoImageSirti        = (( this.sidebarDataObj.logoImageSirti === '') ? '' : this.sidebarDataObj.logoImageSirti );
    this.sidebarDataObj.nomeApp               = (( this.sidebarDataObj.nomeApp === '') ? '' : this.sidebarDataObj.nomeApp );
    this.sidebarDataObj.mostraLogoCustom      = (( this.sidebarDataObj.mostraLogoCustom) ? true : false );
    console.log(this.uiService.getCurrentUser); /* profilo */
    

    
  }
  
  ngOnChanges(): void {
  }

}
