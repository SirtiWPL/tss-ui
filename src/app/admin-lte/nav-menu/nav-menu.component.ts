import { Component, OnInit,Input,OnChanges } from '@angular/core';

import { NavItemModule } from './nav-item.module' /* classe NavItemModule */
declare var $: any;                  //INSERT THIS LINE

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit,OnChanges {
  
  /* istanzio l'oggetto FooterDataModule e lo rendo visibile */
  
  @Input() navMenuCfg: Array<NavItemModule>;   
  

  constructor() { }

  ngOnInit(): void {
    
    console.debug('sono NavMenuComponent: ',this.navMenuCfg);
    $('[data-widget="treeview"]').Treeview('init');
    
  }
  
  ngOnChanges(): void {

    
  }

}
