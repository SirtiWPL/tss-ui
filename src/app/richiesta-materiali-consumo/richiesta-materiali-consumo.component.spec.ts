import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RichiestaMaterialiConsumoComponent } from './richiesta-materiali-consumo.component';

describe('RichiestaMaterialiConsumoComponent', () => {
  let component: RichiestaMaterialiConsumoComponent;
  let fixture: ComponentFixture<RichiestaMaterialiConsumoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RichiestaMaterialiConsumoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RichiestaMaterialiConsumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
