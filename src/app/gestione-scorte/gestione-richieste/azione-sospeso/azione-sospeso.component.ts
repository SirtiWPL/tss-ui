import { Component, OnInit ,Input,Output,EventEmitter,Injectable} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}

@Component({
  selector: 'app-azione-sospeso',
  templateUrl: './azione-sospeso.component.html',
  styleUrls: ['./azione-sospeso.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider
})
export class AzioneSospesoComponent implements OnInit {
  
  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzSospeso: FormGroup;
  public aOptionValueResoPresso:any = [];
  public aOptionCausaleSospensione:any = [];
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;
  
  public markDisabled;
  public calendar: NgbCalendar;
  


  constructor(private formBuilderAzSospeso: FormBuilder,
              calendar: NgbCalendar,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) {
    this.calendar = calendar;
    
    }
    

  ngOnInit(): void {
    
    console.debug('sono AzioneSospesoComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzSospeso  in base al tipo materiale */
    

    this.reactiveFormAzSospeso = this.formBuilderAzSospeso.group({      
      sCausaleSospensioneCheck      : [null,[Validators.required]],
      sDataRitiroResoPropostaCheck  : [null,[Validators.required]], 
      sOraRitiroResoPropostaCheck   : [null,[Validators.required]],
      sNoteSospensioneCheck         : [null],
    });
    
    this.aOptionCausaleSospensione = this.datiAzioneObj.causaleSospensione;
      
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    

    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneSospesoComponent => eseguiAzione: ',this.reactiveFormAzSospeso.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE             : this.datiAzioneObj.richiedente,
      ID_ATT_RESO             : this.datiAzioneObj.materiale.ID,
      DATA_RIT_RESO_PROP      : this.reactiveFormAzSospeso.value.sDataRitiroResoPropostaCheck  ? this.reactiveFormAzSospeso.value.sDataRitiroResoPropostaCheck+' '+this.reactiveFormAzSospeso.value.sOraRitiroResoPropostaCheck : null,
      CAUSALE                 : this.reactiveFormAzSospeso.value.sCausaleSospensioneCheck ? this.reactiveFormAzSospeso.value.sCausaleSospensioneCheck : null,
      NOTE                    : this.reactiveFormAzSospeso.value.sNoteSospensioneCheck    ? this.reactiveFormAzSospeso.value.sNoteSospensioneCheck    : null,
    };
    
    this.gestScorteServices.sospesoResoWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneSospesoComponent => sospesoResoWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

   
  changeCausaleSospensioneValue(e) {
    e.stopPropagation();
    this.reactiveFormAzSospeso.get("sCausaleSospensioneCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneSospesoComponent => changeCausaleSospensioneValue',this.reactiveFormAzSospeso.value.sCausaleSospensioneCheck);

   }
   
   

   
  public onDateSelect(dateSelected) {
    console.debug('sono AzioneSospesoComponent => onDateSelect',dateSelected);

    this.reactiveFormAzSospeso.get("sOraRitiroResoPropostaCheck").setValue('00:00:00', {onlySelf: false});

    
  }
  

}

