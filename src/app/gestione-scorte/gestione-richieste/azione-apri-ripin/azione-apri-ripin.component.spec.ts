import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneApriRipinComponent } from './azione-apri-ripin.component';

describe('AzioneApriRipinComponent', () => {
  let component: AzioneApriRipinComponent;
  let fixture: ComponentFixture<AzioneApriRipinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneApriRipinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneApriRipinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
