import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaProvaComponent } from './pagina-prova.component';

describe('PaginaProvaComponent', () => {
  let component: PaginaProvaComponent;
  let fixture: ComponentFixture<PaginaProvaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaProvaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaProvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
