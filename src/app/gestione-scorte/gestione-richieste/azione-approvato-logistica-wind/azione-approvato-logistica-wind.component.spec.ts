import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneApprovatoLogisticaWindComponent } from './azione-approvato-logistica-wind.component';

describe('AzioneApprovatoLogisticaWindComponent', () => {
  let component: AzioneApprovatoLogisticaWindComponent;
  let fixture: ComponentFixture<AzioneApprovatoLogisticaWindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneApprovatoLogisticaWindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneApprovatoLogisticaWindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
