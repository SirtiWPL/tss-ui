import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneAnnullamentoSospensioneComponent } from './azione-annullamento-sospensione.component';

describe('AzioneAnnullamentoSospensioneComponent', () => {
  let component: AzioneAnnullamentoSospensioneComponent;
  let fixture: ComponentFixture<AzioneAnnullamentoSospensioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneAnnullamentoSospensioneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneAnnullamentoSospensioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
