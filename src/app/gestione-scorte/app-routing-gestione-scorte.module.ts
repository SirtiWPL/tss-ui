import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GestioneScorteComponent } from './gestione-scorte.component';
import { PaginaProvaComponent } from '../admin-lte/common-component/pagina-prova/pagina-prova.component';
import { Page404GestioneScorteComponent } from './page404-gestione-scorte/page404-gestione-scorte.component';
import { NuovaRichiestaComponent } from './nuova-richiesta/nuova-richiesta.component';
import { AuthGuard } from './../auth.guard';
import { GestioneRichiesteComponent } from './gestione-richieste/gestione-richieste.component';
import { PresidioMagazziniComponent } from './presidio-magazzini/presidio-magazzini.component';
import { CompatibiliEquivalentiComponent } from './compatibili-equivalenti/compatibili-equivalenti.component';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: GestioneScorteComponent,
    data : {livello : 'gestioneScorte',icon : 'fas fa-archive',titolo:'Gestione Scorte'},
    children: [
      { path: 'nuovaRichiesta/:richiedente',
        canActivate: [AuthGuard],
        component: NuovaRichiestaComponent,
        data : {livello : 'nuovaRichiesta',icon : 'far fa-edit',titolo:'Nuova Richiesta RMA'}
      },
      { path: 'nuovaRichiestaReso',
        canActivate: [AuthGuard],
        component: GestioneRichiesteComponent,
        data : {livello : 'nuovaRichiestaReso',icon : 'far fa-edit',titolo:'Nuova Richiesta RESO', sModuloChiamante: 'MOD_NUOVA_RICHIESTA', sTipoRichiesta: 'RESO'}
      },
      { path: 'nuovaRichiestaRipin',
        canActivate: [AuthGuard],
        component: GestioneRichiesteComponent,
        data : {livello : 'nuovaRichiestaRipin',icon : 'far fa-edit',titolo:'Nuova Richiesta RIPIN', sModuloChiamante: 'MOD_NUOVA_RICHIESTA', sTipoRichiesta: 'RIPIN'}
      },
      { /*routing - gestioneRichieste  - Inizio */
        path: 'gestioneRichieste',
        component: GestioneScorteComponent,
        canActivate: [AuthGuard],
        data : {livello : 'gestioneRichieste',icon : 'far fa-list-alt',titolo:'Le Mie Richieste'},
        children: [
          { path: 'rmaWind',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'rmaWind',icon : 'far fa-edit',titolo:'RMA WIND',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'RMA WIND'}
          },
          { path: 'reso',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'reso',icon : 'far fa-edit',titolo:'RESO',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'RESO'}
          },
          { path: 'reintegro',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'reintegro',icon : 'far fa-edit',titolo:'REINTEGRO',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'REINTEGRO'}
          },
          { path: 'trasferimento',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'trasferimento',icon : 'far fa-edit',titolo:'TRASFERIMENTO',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'TRASFERIMENTO'}
          },
          { path: 'caricoMateriali',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'caricoMateriali',icon : 'far fa-edit',titolo:'CARICO MATERIALI',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'CARICO MATERIALI'}
          },
          { path: 'logisticaWind',
            component: GestioneRichiesteComponent,
            canActivate: [AuthGuard],
            data : {livello : 'logisticaWind',icon : 'far fa-edit',titolo:'LOGISTICA WIND',sModuloChiamante: 'MOD_GEST_RICHIESTE',sTipoRichiesta: 'LOGISTICA WIND'}
          },
        ]
      }, /*routing - gestioneRichieste  - Fine */
      { path: 'ricercaIdSirti',
        component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : 'ricercaIdSirti',icon : 'fas fa-search',titolo:'Ricerca ID Sirti'}
      },
      { path: 'approvazioneHq',
        canActivate: [AuthGuard],
        component: GestioneRichiesteComponent,
        data : {livello : 'approvazioneHq',icon : 'fas fa-tasks',titolo:'Approvazione HQ',sModuloChiamante: 'MOD_APPROVAZIONE_HQ',sTipoRichiesta: 'APPROVAZIONE HQ'}
      },
      { path: 'trasferimentoWind',
        component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : 'trasferimentoWind',icon : 'fas fa-exchange-alt',titolo:'Trasferimento Wind'}
      },
      { path: 'gestCompMat',
        component: CompatibiliEquivalentiComponent,
        canActivate: [AuthGuard],
        data : {livello : 'gestCompMat',icon : 'far fa-copy',titolo:'Gestione Compatibilita\' Materiali'}
      },
      { path: 'presidiMagazzini',
        component: PresidioMagazziniComponent,
        canActivate: [AuthGuard],
        data : {livello : 'presidiMagazzini',icon : 'far fa-clock',titolo:'Presidi Magazzini'}
      },
    ]
  },
  { path: '404', component: Page404GestioneScorteComponent },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingGestioneScorteModule { }
