import { Component, OnInit ,Input,Output,EventEmitter} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

@Component({
  selector: 'app-azione-annullamento-attesa-materiale',
  templateUrl: './azione-annullamento-attesa-materiale.component.html',
  styleUrls: ['./azione-annullamento-attesa-materiale.component.css']
})
export class AzioneAnnullamentoAttesaMaterialeComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzAnnullamentoAttesaMateriale: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;

  constructor(private formBuilderAzAnnullamentoAttesaMateriale: FormBuilder,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService     
              ) { }

  ngOnInit(): void {
    
    console.debug('sono AzioneAnnullamentoAttesaMaterialeComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzAnnullamentoAttesaMateriale */

    this.reactiveFormAzAnnullamentoAttesaMateriale = this.formBuilderAzAnnullamentoAttesaMateriale.group({
    });
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
  }
  
  public eseguiAzione():void {
    console.debug('sono AzioneAnnullamentoAttesaMaterialeComponent => eseguiAzione: ',this.reactiveFormAzAnnullamentoAttesaMateriale.value);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE     : this.datiAzioneObj.richiedente,
      ID_ATTIVITA     : this.datiAzioneObj.materiale.ID,
    };
    
    this.gestScorteServices.annullaAttMatRmaWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneAnnullamentoAttesaMaterialeComponent => annullaAttMatRmaWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
  
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }

    });
    
  }

}
