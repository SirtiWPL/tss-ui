import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingGestioneScorteModule } from './app-routing-gestione-scorte.module';
import { GestioneScorteComponent } from './gestione-scorte.component';
import { CommonComponentModule } from '../admin-lte/common-component/common-component.module';
import { Page404GestioneScorteComponent } from './page404-gestione-scorte/page404-gestione-scorte.component';
import { NuovaRichiestaComponent } from './nuova-richiesta/nuova-richiesta.component';

/* per gestire ngb */
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GestioneRichiesteComponent } from './gestione-richieste/gestione-richieste.component';

import { AzioneRifiutatoComponent } from './gestione-richieste/azione-rifiutato/azione-rifiutato.component';
import { AzioneApprovatoComponent } from './gestione-richieste/azione-approvato/azione-approvato.component';
import { AzioneAnnullamentoAttesaMaterialeComponent } from './gestione-richieste/azione-annullamento-attesa-materiale/azione-annullamento-attesa-materiale.component';
import { AzioneCambioPrioritaComponent } from './gestione-richieste/azione-cambio-priorita/azione-cambio-priorita.component';
import { AzioneApriRipinComponent } from './gestione-richieste/azione-apri-ripin/azione-apri-ripin.component';
import { AzioneMaterialeUtilizzatoComponent } from './gestione-richieste/azione-materiale-utilizzato/azione-materiale-utilizzato.component';
import { AzioneRientroAMagazzinoComponent } from './gestione-richieste/azione-rientro-a-magazzino/azione-rientro-a-magazzino.component';
import { AzioneRifiutoMaterialeAccettatoComponent } from './gestione-richieste/azione-rifiuto-materiale-accettato/azione-rifiuto-materiale-accettato.component';
import { AzioneAnnullamentoPrenotazioneComponent } from './gestione-richieste/azione-annullamento-prenotazione/azione-annullamento-prenotazione.component';
import { AzioneMaterialeSuMobileComponent } from './gestione-richieste/azione-materiale-su-mobile/azione-materiale-su-mobile.component';
import { AzioneAnnullamentoResoComponent } from './gestione-richieste/azione-annullamento-reso/azione-annullamento-reso.component';
import { AzioneConfermaSospensioneComponent } from './gestione-richieste/azione-conferma-sospensione/azione-conferma-sospensione.component';
import { AzioneSospesoComponent } from './gestione-richieste/azione-sospeso/azione-sospeso.component';
import { AzioneAnnullamentoSospensioneComponent } from './gestione-richieste/azione-annullamento-sospensione/azione-annullamento-sospensione.component';
import { AzioneApprovatoLogisticaWindComponent } from './gestione-richieste/azione-approvato-logistica-wind/azione-approvato-logistica-wind.component';
import { AzioneAnnullataLogisticaWindComponent } from './gestione-richieste/azione-annullata-logistica-wind/azione-annullata-logistica-wind.component';
import { AzioneConsegnaAccettataLogisticaWindComponent } from './gestione-richieste/azione-consegna-accettata-logistica-wind/azione-consegna-accettata-logistica-wind.component';
import { AzioneMaterialeRifiutatoLogisticaWindComponent } from './gestione-richieste/azione-materiale-rifiutato-logistica-wind/azione-materiale-rifiutato-logistica-wind.component';
import { PresidioMagazziniComponent } from './presidio-magazzini/presidio-magazzini.component';
import { FilterSede } from './presidio-magazzini/filter-sede.pipe';
import { CompatibiliEquivalentiComponent } from './compatibili-equivalenti/compatibili-equivalenti.component';


@NgModule({
  declarations: [FilterSede,GestioneScorteComponent, Page404GestioneScorteComponent, NuovaRichiestaComponent, GestioneRichiesteComponent, AzioneRifiutatoComponent, AzioneApprovatoComponent, AzioneAnnullamentoAttesaMaterialeComponent, AzioneCambioPrioritaComponent, AzioneApriRipinComponent, AzioneMaterialeUtilizzatoComponent, AzioneRientroAMagazzinoComponent, AzioneRifiutoMaterialeAccettatoComponent, AzioneAnnullamentoPrenotazioneComponent, AzioneMaterialeSuMobileComponent, AzioneAnnullamentoResoComponent, AzioneConfermaSospensioneComponent, AzioneSospesoComponent, AzioneAnnullamentoSospensioneComponent, AzioneApprovatoLogisticaWindComponent, AzioneAnnullataLogisticaWindComponent, AzioneConsegnaAccettataLogisticaWindComponent, AzioneMaterialeRifiutatoLogisticaWindComponent, PresidioMagazziniComponent, CompatibiliEquivalentiComponent],
  imports: [
    CommonModule,
    AppRoutingGestioneScorteModule,
    CommonComponentModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    
  ],
  exports: []
})

export class GestioneScorteModule { }
