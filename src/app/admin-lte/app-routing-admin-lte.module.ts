import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './../auth.guard';

import { AdminLteComponent } from './admin-lte.component';
import { Page404AdminLteComponent } from './page404-admin-lte/page404-admin-lte.component';
import { PaginaProvaComponent } from './common-component/pagina-prova/pagina-prova.component';
import { ContattiSpocComponent } from '../contatti-spoc/contatti-spoc.component';
import { EmergencyEscalationComponent } from '../emergency-escalation/emergency-escalation.component';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: AdminLteComponent,
    data : {livello : 'principale',icon : 'fas fa-home',titolo:'Home'},
    children: [
      {
        path: 'gestioneScorte',
        canActivate: [AuthGuard],
        loadChildren: () => import(`../gestione-scorte/gestione-scorte.module`).then(m => m.GestioneScorteModule)
      },
      
      {
        path: 'logistica',
        canActivate: [AuthGuard],
        loadChildren: () => import(`../logistica/logistica.module`).then(m => m.LogisticaModule)
      },
      {
        path: 'richiestaMaterialiConsumo',
        canActivate: [AuthGuard],
        loadChildren: () => import(`../richiesta-materiali-consumo/richiesta-materiali-consumo.module`).then(m => m.RichiestaMaterialiConsumoModule)
      },
      
      {
        path: 'reportAnalisi',
        canActivate: [AuthGuard],
        loadChildren: () => import(`../report-analisi/report-analisi.module`).then(m => m.ReportAnalisiModule)
      },
      {
        path: 'documentazione',
        canActivate: [AuthGuard],
        loadChildren: () => import(`../documentazione/documentazione.module`).then(m => m.DocumentazioneModule)
      },
      { path: 'contattiSpoc', component: ContattiSpocComponent,
        canActivate: [AuthGuard],
        data : {livello : 'contattiSpoc',icon : 'far fa-address-book',titolo:'Contatti SPOC'}
      },
      { path: 'emergencyEscalation', component: EmergencyEscalationComponent,
        canActivate: [AuthGuard],
        data : {livello : 'emergencyEscalation',icon : 'far fa-question-circle',titolo:'Emergency path & Escalation'}
      }

    ]
  },
  { path: '404', component: Page404AdminLteComponent },
  
]; 

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingAdminLteModule { }
