import { Component, OnInit,Input,OnChanges  } from '@angular/core';
import { MenuCfgModule } from './../../config/menu-cfg.module' /* modulo di cfg del menu */
import { Router, ActivatedRoute, NavigationEnd, Event } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { UiService } from './../../ui-service.service';
import { GestioneScorteService } from './../../gestione-scorte.service';


@Component({
  selector: 'app-presidio-magazzini',
  templateUrl: './presidio-magazzini.component.html',
  styleUrls: ['./presidio-magazzini.component.css']
})
export class PresidioMagazziniComponent implements OnInit {

  public listaMagazziniPresidiati = [];
  public loaderGrid:boolean;
  public isModify:boolean;
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;
  
  
  public filterSede:string;
  
  constructor(private uiService: UiService,
              private gestScorteServices: GestioneScorteService) {
    console.debug('funziona PresidioMagazziniComponent');

    }

  ngOnInit(): void {
    
    
    this.isModify = false;
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';    
    
    this.ricaricaGrid();
    
  }
  
  public ricaricaGrid() : void {
    
    /* recupero i magazzini dell'operatore - Inizio */
    
    this.listaMagazziniPresidiati = [];
    this.loaderGrid =true;

    
    const qParamsPresidioMagazzini = {
      RICHIEDENTE : 'INTERNO',
      SEDE        : ''
    };
    
    this.gestScorteServices.getPresidioMagazzini(qParamsPresidioMagazzini).subscribe(( data )=>{
      console.debug( data );
      this.listaMagazziniPresidiati  = data.data.results;
      this.loaderGrid=false;
    });
    
    /* recupero i magazzini dell'operatore - Fine */     
    
  }
  
  public savePresidio(presidio:any):void {
    console.debug('sono PresidioMagazziniComponent => eseguiAzione: ',this.listaMagazziniPresidiati);
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    const qParamsAzione = {
      RICHIEDENTE : 'INTERNO',
      SEDE        : presidio.SEDE     ,  
      LUNEDI      : presidio.LUNEDI   ,   
      MARTEDI     : presidio.MARTEDI  ,   
      MERCOLEDI   : presidio.MERCOLEDI,   
      GIOVEDI     : presidio.GIOVEDI  ,   
      VENERDI     : presidio.VENERDI  ,  
    };
    
    this.gestScorteServices.aggiornaPresidiMagazzini(qParamsAzione).subscribe(( data )=> {
      console.debug('PresidioMagazziniComponent => aggiornaPresidiMagazzini => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Impossibile Eseguire il salvataggio: '+data.data.DESC_ESITO;
        this.ricaricaGrid();
      } else {
  
        //this.mostraEsitoAzione      = true;
        //this.tipoEsitoEsitoAzione   = 'SUCCESS';
        //this.msgEsitoAzione         = 'Salvataggio eseguito con successo';
        this.ricaricaGrid();
        
      }

    });
    
  } 
  
  public clickPresidio(presidio:any,giorno:string,valore:string): void {
    presidio[giorno] = valore === 'SI' ? 'NO' : 'SI';
    console.debug('funziona PresidioMagazziniComponent => clickPresidio => ',presidio.SEDE);
    console.debug('funziona PresidioMagazziniComponent => clickPresidio => ',giorno);
    console.debug('funziona PresidioMagazziniComponent => clickPresidio => ',valore);
    console.debug('funziona PresidioMagazziniComponent => clickPresidio => ',presidio);
    this.isModify = false;
    this.savePresidio(presidio);
  }

  


}
