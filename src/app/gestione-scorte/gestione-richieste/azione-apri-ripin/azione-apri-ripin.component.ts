import { Component, OnInit ,Input,Output,EventEmitter,Injectable} from '@angular/core';
/* gestione del FORM */
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UiService } from './../../../ui-service.service';
import { GestioneScorteService } from './../../../gestione-scorte.service';

import {NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct,NgbTimeStruct, NgbTimeAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Observable, of ,OperatorFunction,} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, filter,tap,switchMap,catchError} from 'rxjs/operators';


/* sezione del timepicker */
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;

/**
 * Example of a String Time adapter
 */
@Injectable()
export class NgbTimeStringAdapter extends NgbTimeAdapter<string> {

  fromModel(value: string| null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }

  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
  }
}

/* sezione del datepicker */


const I18N_VALUES = {
  'it': {
    weekdays: ['Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do'],
    months: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
    weekLabel: 'Settimana'
  }
  // other languages you would support
};

// Define a service holding the language. You probably already have one if your app is i18ned. Or you could also
// use the Angular LOCALE_ID value
@Injectable()
export class I18n {
  language = 'it';
}

// Define custom service providing the months and weekdays translations
@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  constructor(private _i18n: I18n) { super(); }

  getWeekdayShortName(weekday: number): string { return I18N_VALUES[this._i18n.language].weekdays[weekday - 1]; }
  getWeekLabel(): string { return I18N_VALUES[this._i18n.language].weekLabel; }
  getMonthShortName(month: number): string { return I18N_VALUES[this._i18n.language].months[month - 1]; }
  getMonthFullName(month: number): string { return this.getMonthShortName(month); }
  getDayAriaLabel(date: NgbDateStruct): string { return `${date.day}-${date.month}-${date.year}`; }
}


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : null;
  }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '/';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[0], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[2], 10)
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? (date.day < 10 ? '0'+date.day : date.day) + this.DELIMITER + (date.month < 10 ? '0'+date.month : date.month) + this.DELIMITER + date.year : '';
  }
}


@Component({
  selector: 'app-azione-apri-ripin',
  templateUrl: './azione-apri-ripin.component.html',
  styleUrls: ['./azione-apri-ripin.component.css'],
  providers:  [I18n,  {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n},
                      {provide: NgbDateAdapter, useClass: CustomAdapter},
                      {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter},
                      {provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter}


               ]  // define custom NgbDatepickerI18n provider
})
export class AzioneApriRipinComponent implements OnInit {

  /* input params azione */
  @Input() datiAzioneObj:any;
  
  /* output params azione */
  @Output() outReload             = new EventEmitter<any>();
  
  /* definisco il reactive form */
  reactiveFormAzApriRipin: FormGroup;
  
  public loaderEseguiAzione   : boolean;
  public mostraEsitoAzione    : boolean;  
  public tipoEsitoEsitoAzione : string;  
  public msgEsitoAzione       : string;
  public minDate              : any;
  public aOptionDescGuasto    :any = [];
  formatterCompatibiliEq  = (magazzinoWindObj: any) => magazzinoWindObj.PART_NUMBER;
  public zeroResultCompatibiliEq:boolean;
  public loaderAutocompleteCompatibiliEq:boolean;
  public aCompatibiliEqTrovati: any;
  public mostraCercaCompatibiliEq:boolean;
  
  public markDisabled;
  public calendar: NgbCalendar;

  constructor(private formBuilderAzApriRipin: FormBuilder,
              calendar: NgbCalendar,
              private uiService: UiService,
              private gestScorteServices: GestioneScorteService
              ) {
    this.calendar = calendar;
    
    }
    
    
  disableDays() {
      this.markDisabled = (date: NgbDate) => {
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.datiAzioneObj.presidioMagazWind);
        let ritorno = (this.calendar.getWeekday(date) >= 6);
        if(this.datiAzioneObj.presidioMagazWind) {
          if(this.datiAzioneObj.presidioMagazWind.LUNEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 1);
          }
          if(this.datiAzioneObj.presidioMagazWind.MARTEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 2);
          }
          if(this.datiAzioneObj.presidioMagazWind.MERCOLEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 3);
          }
          if(this.datiAzioneObj.presidioMagazWind.GIOVEDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 4);
          }
          if(this.datiAzioneObj.presidioMagazWind.VENERDI.toUpperCase() === 'NO') {
            ritorno = ritorno  || (this.calendar.getWeekday(date) == 5);
          }
        }
        //console.debug('sono AzioneApriRipinComponent => disableDays: ',ritorno);
        return ritorno;
      };
       //console.debug('sono AzioneApriRipinComponent => disableDays: ',this.markDisabled);
    
  }

  ngOnInit(): void {
    
    console.debug('sono AzioneApriRipinComponent => datiAzioneObj: ',this.datiAzioneObj);
    
    /* reactiveFormAzApriRipin */
    
    if (this.datiAzioneObj.tipo === 'RESO') {
      
      this.reactiveFormAzApriRipin = this.formBuilderAzApriRipin.group({
        sMagazzinoDestinazioneCheck   : [null,[Validators.required]],
        sPartNumberCheck              : [null,[Validators.required]],
        sNodoProvenienzaCheck         : [null],
        sStatoMaterialeCheck          : ['GUASTO'],
        sDescGuastoCheck              : [null,[Validators.required]],
        sDataRitiroAppuntamentoCheck  : [null,[Validators.required]],
        sOraRitiroAppuntamentoCheck   : [null,[this.changeTime.bind(this)]],
        sCompatibiliEqCheck           : [null],
      });
      
      this.aOptionDescGuasto = this.datiAzioneObj.descrizioneGuasto;
      
      this.reactiveFormAzApriRipin.get("sStatoMaterialeCheck").valueChanges.subscribe(value => {
        if (this.datiAzioneObj.materiale.SERIAL_NUMBER && value.toUpperCase() === 'GUASTO') {
          this.reactiveFormAzApriRipin.get("sDescGuastoCheck").setValidators([Validators.required]);
          this.reactiveFormAzApriRipin.get("sDescGuastoCheck").setValue('', {onlySelf: false});
        } else {
          this.reactiveFormAzApriRipin.get("sDescGuastoCheck").clearValidators();
        }
        this.reactiveFormAzApriRipin.get("sDescGuastoCheck").updateValueAndValidity({ emitEvent : false });
        //console.log(this.reactiveFormAzApriRipin.valid);
      });
      
    } else {

      this.reactiveFormAzApriRipin = this.formBuilderAzApriRipin.group({
        sMagazzinoDestinazioneCheck   : [null,[Validators.required]],
        sPartNumberCheck              : [null,[Validators.required]],
        sNodoProvenienzaCheck         : [null],
        sDescGuastoCheck              : [null],
        sDataRitiroAppuntamentoCheck  : [null,[Validators.required]],
        sOraRitiroAppuntamentoCheck   : [null,[this.changeTime.bind(this)]],
        sCompatibiliEqCheck           : [null],
      });
      
    }
    
    this.loaderEseguiAzione   = false;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    
    this.aCompatibiliEqTrovati = this.datiAzioneObj.compatibiliEquiv ? this.datiAzioneObj.compatibiliEquiv : [];
    
    let newMinDate = new Date(new Date().getTime()+(2*24*60*60*1000));
    console.debug('sono AzioneApriRipinComponent => newMinDate: ',newMinDate);
    
    
    const minDateStr =  (newMinDate.getDate() < 10 ? '0'+newMinDate.getDate() : newMinDate.getDate()) + '-' + ((newMinDate.getMonth()+1) < 10 ? '0'+(newMinDate.getMonth()+1) : (newMinDate.getMonth()+1)) + '-' + newMinDate.getFullYear();
    
    const arrMinDate = minDateStr.split('-');
    
    this.minDate = {year: parseInt(arrMinDate[2]), month: parseInt(arrMinDate[1]), day: parseInt(arrMinDate[0])};
    
    console.debug('sono AzioneApriRipinComponent => newMinDate: ',this.minDate);

    
    this.reactiveFormAzApriRipin.get("sMagazzinoDestinazioneCheck").setValue(this.datiAzioneObj.materiale.MAGAZZINO_DESTINAZIONE, {onlySelf: false});
    this.reactiveFormAzApriRipin.get("sPartNumberCheck").setValue(this.datiAzioneObj.materiale.PART_NUMBER, {onlySelf: false});
    
    this.mostraCercaCompatibiliEq = false;
    
    this.disableDays();
    
  }
  
  public eseguiAzione():void {
    
    this.loaderEseguiAzione   = true;
    this.mostraEsitoAzione    = false;  
    this.tipoEsitoEsitoAzione = '';  
    this.msgEsitoAzione       = '';
    let qParamsAzione     = {};
    
    if (this.datiAzioneObj.tipo === 'RESO') {
      
      console.debug('sono AzioneApriRipinComponent => eseguiAzione => RESO: ',this.reactiveFormAzApriRipin.value);
      

      
      qParamsAzione = {
        RICHIEDENTE             : this.datiAzioneObj.richiedente,
        ID_ATTIVITA             : this.datiAzioneObj.materiale.ID ?  this.datiAzioneObj.materiale.ID  : this.datiAzioneObj.materiale.ID_ATTIVITA ? this.datiAzioneObj.materiale.ID_ATTIVITA : null,
        PART_NUMBER             : this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck ? this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck.PART_NUMBER : this.datiAzioneObj.materiale.PART_NUMBER,
        SAP_CODE                : this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck ? this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck.SAP_CODE : this.datiAzioneObj.materiale.SAP_CODE,
        TIPO_RICHIESTA     	    : 'RESO',
        CAUSALE     		        : this.reactiveFormAzApriRipin.value.sDescGuastoCheck ? this.reactiveFormAzApriRipin.value.sDescGuastoCheck : null,
        TIPOLOGIA_GUASTO   	    : this.reactiveFormAzApriRipin.value.sStatoMaterialeCheck ? this.reactiveFormAzApriRipin.value.sStatoMaterialeCheck : null,
        DDT_RESO   			        : null,
        SENZA_RESTITUZIONE      : 'NO',
        NODO_PROVENIENZA   	    : this.reactiveFormAzApriRipin.value.sNodoProvenienzaCheck ? this.reactiveFormAzApriRipin.value.sNodoProvenienzaCheck : null,
        DATA_APP_RIT_RESO       : this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck ? this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck+' '+this.reactiveFormAzApriRipin.value.sOraRitiroAppuntamentoCheck : null,
        VINC_CONS_CONCORDATA    : 'NO',
        RESO_PRESSO_GLS         : 'SEDE WIND',
      };
      
      console.debug('sono AzioneApriRipinComponent => eseguiAzione => RESO => qParamsAzione: ',qParamsAzione);      
      
      
    } else {    
    
      console.debug('sono AzioneApriRipinComponent => eseguiAzione => RIPIN: ',this.reactiveFormAzApriRipin.value);
      

      
      qParamsAzione = {
        RICHIEDENTE             : this.datiAzioneObj.richiedente,
        ID_ATTIVITA             : this.datiAzioneObj.materiale.ID ?  this.datiAzioneObj.materiale.ID  : this.datiAzioneObj.materiale.ID_ATTIVITA ? this.datiAzioneObj.materiale.ID_ATTIVITA : null,
        PART_NUMBER             : this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck ? this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck.PART_NUMBER : this.datiAzioneObj.materiale.PART_NUMBER,
        SAP_CODE                : this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck ? this.reactiveFormAzApriRipin.value.sCompatibiliEqCheck.SAP_CODE : this.datiAzioneObj.materiale.SAP_CODE,
        TIPO_RICHIESTA     	    : 'RIPIN',
        CAUSALE     		        : this.reactiveFormAzApriRipin.value.sDescGuastoCheck ? this.reactiveFormAzApriRipin.value.sDescGuastoCheck : null,
        TIPOLOGIA_GUASTO   	    : 'GUASTO',
        DDT_RESO   			        : null,
        SENZA_RESTITUZIONE      : 'NO',
        NODO_PROVENIENZA   	    : this.reactiveFormAzApriRipin.value.sNodoProvenienzaCheck ? this.reactiveFormAzApriRipin.value.sNodoProvenienzaCheck : null,
        DATA_APP_RIT_RESO       : this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck ? this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck+' '+this.reactiveFormAzApriRipin.value.sOraRitiroAppuntamentoCheck : null,
        VINC_CONS_CONCORDATA    : 'NO',
        RESO_PRESSO_GLS         : 'SEDE WIND',
      };
      
      console.debug('sono AzioneApriRipinComponent => eseguiAzione => RIPIN => qParamsAzione: ',qParamsAzione);
      

    }
    
    this.gestScorteServices.createResoRipinWind(qParamsAzione).subscribe(( data )=> {
      console.debug('AzioneApriRipinComponent => createResoRipinWind => data',data);
      this.loaderEseguiAzione = false;
      if (data.data.ESITO === 'KO') {
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'ERROR';
        this.msgEsitoAzione         = 'Movimentazione fallita: '+data.data.DESC_ESITO;
      } else {
    
        this.mostraEsitoAzione      = true;
        this.tipoEsitoEsitoAzione   = 'SUCCESS';
        this.msgEsitoAzione         = 'Movimentazione eseguita con successo';
        this.outReload.emit(true);
        
      }
    
    });
    
  }
  
  changeDescGuastoValue(e) {
    e.stopPropagation();
    this.reactiveFormAzApriRipin.get("sDescGuastoCheck").setValue(e.target.value, {onlySelf: false});
    //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
    console.debug('sono AzioneApriRipinComponent => changeDescGuastoValue',this.reactiveFormAzApriRipin.value.sDescGuastoCheck);

   }
   
  
   
   
  /* ricerca magazzino */
  searchCompatibiliEq: OperatorFunction<string, readonly {PART_NUMBER}[]> = (text$: Observable<string>) => text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    //filter(term => term.length >= 2),
    map(term => {
        //this.resetSezioniRisultatoRicerca(); /* resetto tutto */
        //this.zeroResultMateriale    = false;
        this.zeroResultCompatibiliEq    = false;
        this.loaderAutocompleteCompatibiliEq  = true;
        if (term.length >= 1) {
         const result = this.aCompatibiliEqTrovati.filter(magazzinoWindObj => new RegExp('^'+term, 'mi').test(magazzinoWindObj.PART_NUMBER)).slice(0, 10);
         if (result.length > 0) {
          this.zeroResultCompatibiliEq = false;
         } else {
          this.zeroResultCompatibiliEq = true;
         }
        this.loaderAutocompleteCompatibiliEq  = false;
         return result;
        } else {
          if (term.length == 1) {
            this.loaderAutocompleteCompatibiliEq   = true;
          } else {
            this.loaderAutocompleteCompatibiliEq   = false;
            this.zeroResultCompatibiliEq = true;
          }
          return [];
        }
      }
    ),
  )

  /* seleziona il magazzino  */
  selectedItemCompatibiliEq(itemSelected): void {
    //this.abilitaQtaMatRichiesta = false;
    //this.abilitaQtaMatRichiesta = false;
    console.debug('selectedItemCompatibiliEq: ',itemSelected);

  }
  
  apriChiudiCercaCompatibiliEq(itemSelected): void {
    console.debug('apriChiudiCercaCompatibiliEq: ',itemSelected);    
    this.mostraCercaCompatibiliEq = !this.mostraCercaCompatibiliEq;
    
    this.reactiveFormAzApriRipin.get("sCompatibiliEqCheck").setValue(null, {onlySelf: false});
  }
  
  public onDateSelect(dateSelected) {
    console.debug('sono AzioneApriRipinComponent => onDateSelect',dateSelected);
    this.reactiveFormAzApriRipin.get("sOraRitiroAppuntamentoCheck").setValue(this.datiAzioneObj.orario.min, {onlySelf: false});
  }
  
  public changeTime(timeSelected) {
    console.debug('sono AzioneApriRipinComponent => changeTime',timeSelected);
    /*
     if (timeSelected.touched && !this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
     */ 
     if (timeSelected.touched && !timeSelected.value && this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
      
     if (timeSelected.touched && timeSelected.value && !this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck) {
        return {errorMinMax:true};
      }
    
    
     if (timeSelected.value) {
      const aTimeSelected = timeSelected.value.split(':');
      const dataRipApp = this.reactiveFormAzApriRipin.value.sDataRitiroAppuntamentoCheck;
      

      
      const sDataSelected = dataRipApp+' '+timeSelected.value;

      let dateParts = sDataSelected.substring(0, 10).split("-");
      let oreMinParts = sDataSelected.substring(11, 19).split(":");


      

      const dateNew = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneApriRipinComponent => changeTime => DATA SELECTED',sDataSelected,dateNew);
      
      
      const sDataMin = dataRipApp+' '+this.datiAzioneObj.orario.min;

      dateParts = sDataMin.substring(0, 10).split("-");
      oreMinParts = sDataMin.substring(11, 19).split(":");

     

      const dateMin = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
       console.debug('sono AzioneApriRipinComponent => changeTime => DATA MIN',sDataMin,dateMin);
      
      const sDataMax = dataRipApp+' '+this.datiAzioneObj.orario.max;

      dateParts = sDataMax.substring(0, 10).split("-");
      oreMinParts = sDataMax.substring(11, 19).split(":");

     

      const dateMax = new Date(+parseInt(dateParts[2]), parseInt(dateParts[1]) - 1, +parseInt(dateParts[0]),parseInt(oreMinParts[0]),parseInt(oreMinParts[1]),parseInt(oreMinParts[2]));
      console.debug('sono AzioneApriRipinComponent => changeTime => DATA MAX',sDataMax,dateMax);
      
      
      if (dateNew < dateMin) {
       console.debug('sono AzioneApriRipinComponent => changeTime => ERROR A < MIN ',dateNew,dateMin);
       //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
       return {errorMinMax:true};
       //this.reactiveFormAzApriRipin.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMin.substring(11, 19), {onlySelf: false});
      } else {
        if (dateNew > dateMax) {
         console.debug('sono AzioneApriRipinComponent => changeTime => ERROR A > MAX ',dateNew,dateMax);
         //this.reactiveFormAzApriRipin.get("oraConsegnaSuAppuntamentoStdCheck").setValue(sDataMax.substring(11, 19), {onlySelf: false});
         //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
         return {errorMinMax:true};
        } else {
          console.debug('sono AzioneApriRipinComponent => changeTime => VALORE ACCETTATO:  MIN <= A <= MAX ',dateNew,dateMin,dateMax);
          //this.creaRmaSection.abilitaCreaRma  = this.abilitaCreaRmaButton();
          return false;
        }
      }    
 
   }
  }

}
