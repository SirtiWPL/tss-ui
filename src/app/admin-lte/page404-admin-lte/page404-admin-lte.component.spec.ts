import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Page404AdminLteComponent } from './page404-admin-lte.component';

describe('Page404AdminLteComponent', () => {
  let component: Page404AdminLteComponent;
  let fixture: ComponentFixture<Page404AdminLteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Page404AdminLteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Page404AdminLteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
