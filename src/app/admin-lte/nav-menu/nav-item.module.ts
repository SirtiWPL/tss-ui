export class NavItemModule {
  posizione   : number;
  isOpen      : boolean;
  isParent    : boolean;
  color       : string;
  titolo      : string;
  descrizione : string;
  icon        : string;
  routerLink  : string;
  visible     : boolean;
  disabled    : boolean;
  children    : Array<NavItemModule>;
}
