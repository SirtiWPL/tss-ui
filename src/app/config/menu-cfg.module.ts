import { UiService } from './../ui-service.service';

export interface iMenuCfgModule {
  posizione   : number;
  isOpen      : boolean;
  isParent    : boolean;
  color       : string;
  livello     : string;
  colorTile   : string;
  titolo      : string;
  descrizione : string;
  icon        : string;
  routerLink  : string;
  visible     : boolean;
  disabled    : boolean;
  children    : Array <any>
} 

export class MenuCfgModule {
  
    //this.colorlivello3 = '#ffc107';
  colorlivello3   : string = 'lightgray';
  colorlivello2   : string = 'orange';
  colorlivello1   : string = '#fd7e14';
  
  colorTileLiv1   : string = 'bg-orange color-palette';
  colorTileLiv2   : string = 'bg-warning color-palette';
  colorTileLiv3   : string = 'bg-gray color-palette';
  colorTileFoglia : string = 'bg-light color-palette';
  
  /* custom color by wind */
  
  
  colorGestioneScorte = {
    livello : '#fd7e14',
    tile    : 'bg-orange color-palette'
  }
  
  colorLogistica = {
    livello : '#fd7e14',
    tile    : 'bg-orange color-palette'
  }
  
  
  colorReportAnalisi = {
    livello   : '#28a745',
    tile      : 'bg-success color-palette',
    kpiGrezzi : {
      livello   : '#3d9970',
      tile      : 'bg-olive color-palette'
    },
    datiGrezzi : {
      livello   : '#3d9970',
      tile      : 'bg-olive color-palette'
    }
  }
  
  colorDocumentazione = {
    livello : '#28a745',
    tile    : 'bg-success color-palette'
  }
  
  colorContattiSpoc = {
    livello : 'white',
    tile    : 'bg-light color-palette'
  }
  
  colorEmergencyEscalation = {
    livello : 'white',
    tile    : 'bg-light color-palette'
  }
  
  colorRichiestaMaterialiConsumo = {
    livello : '#fd7e14',
    tile    : 'bg-orange color-palette'
  }
  
  
  
  public mainMenuCfg : Array <iMenuCfgModule> = [];
  public jwtToken:any = null;
  
  
  constructor(private uiService ?: UiService) {
    console.debug('MenuCfgModule => costruttore => service',this.uiService);
    if ((this.uiService) && (this.uiService.getCurrentUser)) {
      console.debug('MenuCfgModule => costruttore => current user',this.uiService.getCurrentUser);
      this.jwtToken = (this.uiService.getJwtToken && this.uiService.getJwtToken.jwtToken) ? this.uiService.getJwtToken.jwtToken : null;
      console.debug('MenuCfgModule => costruttore => jwtToken',this.jwtToken );
      const abilitazioniCustom = {
          ricercercaIdSirti               : !this.uiService.getCurrentUser.isRoot && this.uiService.getCurrentUser.isOperatoreAziendaEsterna ? false : true,
          approvazioneHd                  : this.uiService.getCurrentUser.isRoot || this.uiService.getCurrentUser.isWindHq ? true : false,
          trasferimento                   : false /*this.uiService.getCurrentUser.isAbilitatoTrasfermento*/ ,
          compatibili                     : this.uiService.getCurrentUser.isAbilitatoGestCompatibili,
          presidioMagaz                   : this.uiService.getCurrentUser.isAbilitatoPresidioMagaz,
          menuLogistica                   : this.uiService.getCurrentUser.isAbilitatoMenuLogistica,
          richFornitura                   : this.uiService.getCurrentUser.isAbilitatoRichFornitura, 
          minerva                         : this.uiService.getCurrentUser.isAbilitatoMinerva,    
          vincoloSvincolo                 : this.uiService.getCurrentUser.isAbilitatoVincoloSvincolo,    
          menuReportAnalisi               : !this.uiService.getCurrentUser.isRoot && this.uiService.getCurrentUser.isOperatoreAziendaEsterna ? false : true, 
          subMenuKpiGrezzi                : this.uiService.getCurrentUser.isRoot || (!this.uiService.getCurrentUser.isOperatoreAziendaEsterna  && this.uiService.getCurrentUser.isAbilitatoKpi) ? true : false,
          subMenuDatiGrezzi               : this.uiService.getCurrentUser.isRoot || (!this.uiService.getCurrentUser.isOperatoreAziendaEsterna  && this.uiService.getCurrentUser.isAbilitatoKpi) ? true : false,
          menuDocumentazione              : !this.uiService.getCurrentUser.isRoot && this.uiService.getCurrentUser.isOperatoreAziendaEsterna ? false : true,
          menuRicMatConsumo               : this.uiService.getCurrentUser.isAbilitatoMenuRicMatConsumo,  
          nuovaRichiestaConsumo           : this.uiService.getCurrentUser.isAbilitatoNuovaRicMatConsumo, 
          approvazioneRichiesteConsumo    : this.uiService.getCurrentUser.isAbilitatoAppRicMatConsumo,   
          statoRichiesteConsumo           : this.uiService.getCurrentUser.isAbilitatoStatoRicMatConsumo, 
          
        } 
      
    /* creo la sidebar menu */
    this.mainMenuCfg = [
                          {
                          posizione   : 1,
                          isOpen      : true,
                          isParent    : true,
                          color       : this.colorGestioneScorte.livello,
                          livello     : 'principale',
                          colorTile   : this.colorGestioneScorte.tile, 
                          titolo      : 'Gestione Scorte',
                          descrizione : 'Ingaggio e Gestione delle richieste',
                          icon        : 'fas fa-archive',
                          routerLink  : '/gestioneScorte',
                          visible     : true,
                          disabled    : true,
                          children    : [
                              {
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Nuova Richiesta RMA',
                              descrizione : 'Creazione di un RMA',
                              icon        : 'far fa-edit',
                              routerLink  : '/gestioneScorte/nuovaRichiesta/INTERNO',
                              visible     : true,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 2,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Nuova Richiesta RESO',
                              descrizione : 'Creazione di un RESO',
                              icon        : 'far fa-edit',
                              routerLink  : '/gestioneScorte/nuovaRichiestaReso',
                              visible     : true,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 3,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Nuova Richiesta RIPIN',
                              descrizione : 'Creazione di un RIPIN',
                              icon        : 'far fa-edit',
                              routerLink  : '/gestioneScorte/nuovaRichiestaRipin',
                              visible     : true,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 4,
                              isOpen      : false,
                              isParent    : true,
                              livello     : 'gestioneScorte',
                              color       : this.colorlivello2,
                              colorTile   : this.colorTileLiv2,
                              titolo      : 'Le Mie Richieste',
                              descrizione : 'Gestione di tutte le richieste di WIND',
                              icon        : 'far fa-list-alt',
                              routerLink  : '/gestioneScorte/gestioneRichieste',
                              visible     : true,
                              disabled    : true,
                              children    : [
                                {
                                posizione   : 1,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileLiv2,
                                livello     : 'gestioneRichieste',
                                titolo      : 'RMA WIND',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/rmaWind',
                                visible     : true,
                                disabled    : true,
                                children    : []
                                },
                                {
                                posizione   : 2,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileLiv2,
                                livello     : 'gestioneRichieste',
                                titolo      : 'RESO',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/reso',
                                visible     : true,
                                disabled    : true,
                                children    : []
                                },
                                {
                                posizione   : 3,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileLiv2,
                                livello     : 'gestioneRichieste',
                                titolo      : 'REINTEGRO',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/reintegro',
                                visible     : false,
                                disabled    : true,
                                children    : []
                                },
                                {
                                posizione   : 4,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileLiv2,
                                livello     : 'gestioneRichieste',
                                titolo      : 'TRASFERIMENTO',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/trasferimento',
                                visible     : true,
                                disabled    : true,
                                children    : []
                                },
                                {
                                posizione   : 5,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileLiv2,
                                livello     : 'gestioneRichieste',
                                titolo      : 'CARICO MATERIALI',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/caricoMateriali',
                                visible     : false,
                                disabled    : true,
                                children    : []
                                },
                                {
                                posizione   : 6,
                                isOpen      : false,
                                isParent    : false,
                                color       : this.colorlivello2,
                                colorTile   : this.colorTileFoglia,
                                livello     : 'gestioneRichieste',
                                titolo      : 'LOGISTICA WIND',
                                descrizione : '',
                                icon        : 'far fa-edit',
                                routerLink  : '/gestioneScorte/gestioneRichieste/logisticaWind',
                                visible     : true,
                                disabled    : true,
                                children    : []
                                },
                              ]
                              },
                              /*{
                              posizione   : 5,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Ricerca ID Sirti',
                              descrizione : '',
                              icon        : 'fas fa-search',
                              routerLink  : '/gestioneScorte/ricercaIdSirti',
                              visible     : abilitazioniCustom.ricercercaIdSirti,
                              disabled    : true,
                              children    : []
                              },*/
                              {
                              posizione   : 5,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Approvazione HQ',
                              descrizione : '',
                              icon        : 'fas fa-tasks',
                              routerLink  : '/gestioneScorte/approvazioneHq',
                              visible     : abilitazioniCustom.approvazioneHd,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 6,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Trasferimento Wind',
                              descrizione : '',
                              icon        : 'fas fa-exchange-alt',
                              routerLink  : '/gestioneScorte/trasferimentoWind',
                              visible     : abilitazioniCustom.trasferimento,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 7,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Gestione Compatibilita\' Materiali',
                              descrizione : '',
                              icon        : 'far fa-copy',
                              routerLink  : '/gestioneScorte/gestCompMat',
                              visible     : abilitazioniCustom.compatibili,
                              disabled    : true,
                              children    : []
                              },                             
                              {
                              posizione   : 8,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'gestioneScorte',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Presidi Magazzini',
                              descrizione : '',
                              icon        : 'far fa-clock',
                              routerLink  : '/gestioneScorte/presidiMagazzini',
                              visible     : abilitazioniCustom.presidioMagaz,
                              disabled    : true,
                              children    : []
                              },                                
                              
                              
                            ]
                          },
                          {
                          posizione   : 2,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorLogistica.livello,
                          colorTile   : this.colorLogistica.tile, 
                          livello     : 'principale',
                          titolo      : 'Logistica',
                          descrizione : 'Gestione della fornitura',
                          icon        : 'fas fa-warehouse',
                          routerLink  : '/logistica',
                          visible     : abilitazioniCustom.menuLogistica,
                          disabled    : true,
                          children    : [
                              {
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'logistica',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Prelievi da Magazzino',
                              descrizione : '',
                              icon        : 'fas fa-dolly',
                              routerLink  : '/logistica/prelieviDaMagazzini',
                              visible     : abilitazioniCustom.richFornitura,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 2,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'logistica',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Altre attivita\'',
                              descrizione : '',
                              icon        : 'fas fa-link',
                              //routerLink  : '/logistica/altreAttivita',
                              routerLink  : 'https://services.sirti.net/minerva/#!/home?token='+this.jwtToken,
                              visible     : abilitazioniCustom.minerva,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isToken     : true
                              },
                              {
                              posizione   : 3,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'logistica',
                              colorTile   : this.colorTileFoglia,                              
                              titolo      : 'Vincolo/Svincolo da Piattaforma',
                              descrizione : '',
                              icon        : 'fas fa-unlock-alt',
                              routerLink  : '/logistica/vincoloSvincoloMateriali',
                              visible     : abilitazioniCustom.vincoloSvincolo,
                              disabled    : true,
                              children    : []
                              },
                            ]
                          },
                          {
                          posizione   : 3,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorRichiestaMaterialiConsumo.livello,
                          colorTile   : this.colorRichiestaMaterialiConsumo.tile, 
                          livello     : 'principale',
                          titolo      : 'Reintegro Materiali di Consumo',
                          descrizione : '',
                          icon        : 'fas fa-clipboard-list',
                          routerLink  : '/richiestaMaterialiConsumo',
                          visible     : abilitazioniCustom.menuRicMatConsumo,
                          disabled    : true,
                          children    : [
                              {
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'richiestaMaterialiConsumo',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Nuova Richiesta',
                              descrizione : '',
                              icon        : 'fas fa-edit',
                              routerLink  : '/richiestaMaterialiConsumo/nuovaRichiestaConsumo',
                              visible     : abilitazioniCustom.nuovaRichiestaConsumo,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 2,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'richiestaMaterialiConsumo',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Approvazione Richieste',
                              descrizione : '',
                              icon        : 'fas fa-tasks',
                              routerLink  : '/richiestaMaterialiConsumo/approvazioneRichiesteConsumo',
                              visible     : abilitazioniCustom.approvazioneRichiesteConsumo,
                              disabled    : true,
                              children    : []
                              },
                              {
                              posizione   : 3,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'richiestaMaterialiConsumo',
                              colorTile   : this.colorTileFoglia,                              
                              titolo      : 'Stato Richieste',
                              descrizione : '',
                              icon        : 'fas fa-list-alt',
                              routerLink  : '/richiestaMaterialiConsumo/statoRichiesteConsumo',
                              visible     : abilitazioniCustom.statoRichiesteConsumo,
                              disabled    : true,
                              children    : []
                              },
                            ]
                          },
                          {
                          posizione   : 4,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorReportAnalisi.livello,
                          colorTile   : this.colorReportAnalisi.tile, 
                          livello     : 'principale',
                          titolo      : 'Report e Analisi',
                          descrizione : 'Gui per la gestione dei Report e l\'analisi dei dati',
                          icon        : 'fas fa-chart-bar',
                          routerLink  : '/reportAnalisi',
                          visible     : abilitazioniCustom.menuReportAnalisi,
                          disabled    : true,
                          children    : [
                              /*{
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'reportAnalisi',
                              colorTile   : this.colorTileFoglia,
                              titolo      : 'Mappa Giacenze',
                              descrizione : '',
                              icon        : 'fas fa-map-marked-alt',
                              routerLink  : '/reportAnalisi/mappaGiacenze',
                              visible     : true,
                              disabled    : true,
                              children    : []
                              },*/
                              {
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              livello     : 'reportAnalisi',
                              colorTile   : this.colorTileFoglia,                              
                              titolo      : 'Report',
                              descrizione : '',
                              icon        : 'far fa-file-alt',
                              routerLink  : '/reportAnalisi/report',
                              visible     : true,
                              disabled    : true,
                              children    : []
                              },
                              /*{
                              posizione   : 3,
                              isOpen      : false,
                              isParent    : true,
                              color       : this.colorReportAnalisi.kpiGrezzi.livello,
                              livello     : 'reportAnalisi',
                              colorTile   : this.colorReportAnalisi.kpiGrezzi.tile,
                              titolo      : 'KPI Grezzi',
                              descrizione : '',
                              icon        : 'fas fa-chart-line',
                              routerLink  : '/reportAnalisi/kpiGrezzi',
                              visible     : abilitazioniCustom.subMenuKpiGrezzi,
                              disabled    : true,
                              children    : [
                                  {
                                  posizione   : 1,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',
                                  titolo      : 'KPI Answer Richieste SP',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiAnswerRichiesteSp',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiAnswerRichiesteSp',                                        
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiAnswerRichiesteSp/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiAnswerRichiesteSp',                                       
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiAnswerRichiesteSp/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },
                                  {
                                  posizione   : 2,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',
                                  titolo      : 'KPI Reso',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiReso',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiReso',                                      
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiReso/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiReso',                                      
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiReso/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },
                                  {
                                  posizione   : 3,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',                                
                                  titolo      : 'KPI Riparazione',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiRiparazioneKg',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRiparazioneKg',                                      
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRiparazioneKg/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRiparazioneKg',                                      
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRiparazioneKg/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },
                                  {
                                  posizione   : 4,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',                                  
                                  titolo      : 'KPI Repair Quality - Vendor NR',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorNr',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorNr',                                        
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorNr/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorNr',                                        
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorNr/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },
                                  {
                                  posizione   : 5,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',                                  
                                  titolo      : 'KPI Repair Quality - Vendor RG per dominio tecnologico',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerDm',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorRgPerDm',                                      
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerDm/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorRgPerDm',                                      
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerDm/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },
                                  {
                                  posizione   : 6,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'kpiGrezzi',
                                  titolo      : 'KPI Repair Quality - Vendor RG per riparatore',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerRip',
                                  visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorRgPerRip',                                         
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerRip/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqVendorRgPerRip',                                         
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/kpiGrezzi/kpiRqVendorRgPerRip/storico',
                                      visible     : abilitazioniCustom.subMenuKpiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },  
                                ]
                              },
                              {
                              posizione   : 4,
                              isOpen      : false,
                              isParent    : true,
                              color       : this.colorReportAnalisi.datiGrezzi.livello,
                              livello     : 'reportAnalisi',
                              colorTile   : this.colorReportAnalisi.datiGrezzi.tile,                             
                              titolo      : 'Dati grezzi',
                              descrizione : '',
                              icon        : 'fas fa-chart-area',
                              routerLink  : '/reportAnalisi/datiGrezzi',
                              visible     : abilitazioniCustom.subMenuDatiGrezzi,
                              disabled    : true,
                              children    : [
                                  {
                                  posizione   : 1,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'datiGrezzi',                                  
                                  titolo      : 'KPI Riparazione',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/datiGrezzi/kpiRiparazioneDg',
                                  visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRiparazioneDg',                                      
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRiparazioneDg/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRiparazioneDg',                                        
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRiparazioneDg/storico',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },                                 
                                  {
                                  posizione   : 2,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'datiGrezzi',                                  
                                  titolo      : 'KPI Repair Quality - RMA CHIUSE',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaChiuse',
                                  visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaChiuse',  
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaChiuse/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaChiuse',                                        
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaChiuse/storico',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  },                                 
                                  {
                                  posizione   : 3,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'datiGrezzi',                               
                                  titolo      : 'KPI Repair Quality - RMA Rifiuto Materiali Accettati',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaRifiutoMatAcc',
                                  visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaRifiutoMatAcc',                                       
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaRifiutoMatAcc/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaRifiutoMatAcc',                                       
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaRifiutoMatAcc/storico',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  }, 
                                  {
                                  posizione   : 4,
                                  isOpen      : false,
                                  isParent    : true,
                                  color       : this.colorlivello3,
                                  colorTile   : this.colorTileLiv3,
                                  livello     : 'datiGrezzi',                                  
                                  titolo      : 'KPI Repair Quality - RMA IRRIPARABILI',
                                  descrizione : '',
                                  icon        : 'far fa-folder',
                                  routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaIrriparabili',
                                  visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                  disabled    : true,
                                  children    : [
                                      {
                                      posizione   : 1,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaIrriparabili', 
                                      titolo      : 'Mese Corrente',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar-alt',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaIrriparabili/meseCorrente',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                      {
                                      posizione   : 2,
                                      isOpen      : false,
                                      isParent    : false,
                                      colorTile   : this.colorTileFoglia,
                                      livello     : 'kpiRqRmaIrriparabili',                                       
                                      titolo      : 'Storico',
                                      descrizione : '',
                                      icon        : 'fas fa-calendar',
                                      routerLink  : '/reportAnalisi/datiGrezzi/kpiRqRmaIrriparabili/storico',
                                      visible     : abilitazioniCustom.subMenuDatiGrezzi,
                                      disabled    : true,
                                      children    : []
                                      },
                                    ]
                                  }, 
                                ]
                              }, */
                            ]
                          },
                          {
                          posizione   : 5,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorDocumentazione.livello,
                          colorTile   : this.colorDocumentazione.tile, 
                          livello     : 'principale',
                          titolo      : 'Documentazione',
                          descrizione : 'Guide e Manuali',
                          icon        : 'fas fa-file',
                          routerLink  : '/documentazione',
                          visible     : abilitazioniCustom.menuDocumentazione,
                          disabled    : true,
                          children    : [
                              {
                              posizione   : 1,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Manuale Operativo',
                              descrizione : '',
                              icon        : 'far fa-file-pdf',
                              routerLink  : 'assets/manuali/MVSPM%20-%20Wind%20-%20Manuale%20Operativo.pdf',
                              nomeFile    : 'MVSPM - Wind - Manuale Operativo.pdf',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 2,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Logistica Manuale Operativo',
                              descrizione : '',
                              icon        : 'far fa-file-pdf',
                              routerLink  : 'assets/manuali/Manuale%20Tool%20Logistica%20-%20Rev.%201.0.pdf',
                              nomeFile    : 'Manuale Tool Logistica - Rev. 1.0.pdf',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 3,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Modulo di Carico/Scarico',
                              descrizione : '',
                              icon        : 'far fa-file-excel',
                              routerLink  : 'assets/manuali/Modulo_di_Carico_Scarico.xls',
                              nomeFile    : 'Modulo_di_Carico_Scarico.xls',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 4,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Dettagli Report Pyramide',
                              descrizione : '',
                              icon        : 'far fa-file-pdf',
                              routerLink  : 'assets/manuali/Dettagli%20Report%20Pyramide.pdf',
                              nomeFile    : 'Dettagli Report Pyramide.pdf',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 5,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Norma Gestione scorte guaste',
                              descrizione : '',
                              icon        : 'far fa-file-pdf',
                              routerLink  : 'assets/manuali/Miglioramenti%20MVSPM%20-%20Imballaggio%20-%20Wind.pdf',
                              nomeFile    : 'Miglioramenti MVSPM - Imballaggio - Wind.pdf',                              
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 6,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'FAQ Pyramide',
                              descrizione : '',
                              icon        : 'far fa-file-pdf',
                              routerLink  : 'assets/manuali/FAQ%20Pyramide.pdf',
                              nomeFile    : 'FAQ Pyramide.pdf',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 7,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Documentazione Tecnica Scorte e Logistica (Link Teams)',
                              descrizione : '',
                              icon        : 'fas fa-link',
                              routerLink  : 'https://teams.microsoft.com/l/team/19%3ae6260647664e4e6191f36a03ffb8438f%40thread.tacv2/conversations?groupId=8bdf79f3-f0f3-492b-9a8b-9054afa1e9a6&tenantId=86fb8e1f-02bd-4434-96ae-aa3ef467ada8',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              },
                              {
                              posizione   : 8,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'PROCEDURA RICHIESTA SCORTE MTNE HUAWEI',
                              descrizione : '',
                              icon        : 'far fa-file-powerpoint',
                              routerLink  : 'assets/manuali/Procedura%20richiesta%20scorta%20MNTE-GPON%20Huawei.pptx',
                              nomeFile    : 'Procedura richiesta scorta MNTE-GPON Huawei.pptx',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              isDownload  : true,
                              },
                              {
                              posizione   : 9,
                              isOpen      : false,
                              isParent    : false,
                              colorTile   : this.colorTileFoglia,
                              livello     : 'documentazione',
                              titolo      : 'Sedi GLS',
                              descrizione : '',
                              icon        : 'fas fa-link',
                              routerLink  : 'https://www.gls-italy.com/it/azienda/sedi',
                              visible     : abilitazioniCustom.menuDocumentazione,
                              disabled    : true,
                              children    : [],
                              isDocument  : true,
                              },
                            ]
                          },
                          {
                          posizione   : 6,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorContattiSpoc.livello,
                          colorTile   : this.colorContattiSpoc.tile, 
                          livello     : 'principale',
                          titolo      : 'Contatti SPOC',
                          descrizione : '',
                          icon        : 'fas fa-address-book',
                          routerLink  : '/contattiSpoc',
                          visible     : true,
                          disabled    : true,
                          children    : []
                          },
                          {
                          posizione   : 7,
                          isOpen      : false,
                          isParent    : true,
                          color       : this.colorEmergencyEscalation.livello,
                          colorTile   : this.colorEmergencyEscalation.tile, 
                          livello     : 'principale',
                          titolo      : 'Emergency path & Escalation',
                          descrizione : '',
                          icon        : 'fas fa-question-circle',
                          routerLink  : '/emergencyEscalation',
                          visible     : true,
                          disabled    : true,
                          children    : []
                          },

                        
                          
      ]; 
    }
  }
 

  
};  
