import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingRichiestaMaterialiConsumoModule } from './app-routing-richiesta-materiali-consumo.module';
import { RichiestaMaterialiConsumoComponent } from './richiesta-materiali-consumo.component';
import { CommonComponentModule } from '../admin-lte/common-component/common-component.module';
import { Page404RichiestaMaterialiConsumoComponent } from './page404-richiesta-materiali-consumo/page404-richiesta-materiali-consumo.component';

@NgModule({
  declarations: [RichiestaMaterialiConsumoComponent, Page404RichiestaMaterialiConsumoComponent],
  imports: [
    CommonModule,
    AppRoutingRichiestaMaterialiConsumoModule,
    CommonComponentModule
  ],

})

export class RichiestaMaterialiConsumoModule { }
