export interface iFooterDataModule {
    annoCopyright   : string;
    linkTitle       : string;
    ragioneSociale  : string;
    version         : string;
}
