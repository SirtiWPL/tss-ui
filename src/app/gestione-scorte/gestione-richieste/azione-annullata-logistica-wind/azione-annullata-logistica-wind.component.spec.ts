import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AzioneAnnullataLogisticaWindComponent } from './azione-annullata-logistica-wind.component';

describe('AzioneAnnullataLogisticaWindComponent', () => {
  let component: AzioneAnnullataLogisticaWindComponent;
  let fixture: ComponentFixture<AzioneAnnullataLogisticaWindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AzioneAnnullataLogisticaWindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AzioneAnnullataLogisticaWindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
