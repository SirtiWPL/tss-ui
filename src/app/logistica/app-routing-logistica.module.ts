import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LogisticaComponent } from './logistica.component';
import { PaginaProvaComponent } from '../admin-lte/common-component/pagina-prova/pagina-prova.component';
import { Page404LogisticaComponent } from './page404-logistica/page404-logistica.component';
import { AuthGuard } from './../auth.guard';
import { ExternalIframeComponent } from '../admin-lte/common-component/external-iframe/external-iframe.component';

/* cfg delle rotte nel figlio */
const appRoutes: Routes = [
  {
    path: '', component: LogisticaComponent,
    data : {livello : 'logistica',icon : 'fas fa-warehouse',titolo:'Logistica'},
    children: [
      { path: 'prelieviDaMagazzini', component: ExternalIframeComponent,
        canActivate: [AuthGuard],
        data : {livello : 'prelieviDaMagazzini',icon : 'fas fa-dolly',titolo:'Prelievi da Magazzino', urlCfg:'WIND_V2/#/ingaggioWind/nuovaRichiestaFornituraApparati'}
      },
      /*
      { path: 'altreAttivita', component: PaginaProvaComponent,
        canActivate: [AuthGuard],
        data : {livello : 'altreAttivita',icon : 'fas fa-list',titolo:'Altre attivita\''}
      },
      */
      { path: 'vincoloSvincoloMateriali', component: ExternalIframeComponent,
        canActivate: [AuthGuard],
        data : {livello : 'vincoloSvincoloMateriali',icon : 'fas fa-unlock-alt',titolo:'Vincolo/Svincolo da Piattaforma',urlCfg:'WIND_V2/#/ingaggioWind/vincoloMateriali'}
      },
    ]
  },
  { path: '404', component: Page404LogisticaComponent },
  
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
  ],
  exports: [RouterModule]
})
export class AppRoutingLogisticaModule { }
